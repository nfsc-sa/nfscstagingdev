<?php
/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Nfsc_survey
 * @subpackage Nfsc_survey/includes
 * @author     Sachin Nawale <sachin.nawale@clariontech.com>
 */
class Nfsc_survey_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
