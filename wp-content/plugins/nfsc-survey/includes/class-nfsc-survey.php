<?php

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       https://profiles.wordpress.org/yogeshdalavi/
 * @since      1.0.0
 *
 * @package    Nfsc_survey
 * @subpackage Nfsc_survey/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    Nfsc_survey
 * @subpackage Nfsc_survey/includes
 * @author     Sachin Nawale <sachin.nawale@clariontech.com>
 */
class Nfsc_survey {

    public function __construct() {

        add_shortcode('survey_shortcode', array($this, 'survey_input_output_ui'));
        add_shortcode('welcome_shortcode', array($this, 'payment_schedule_input_output_ui'));
        add_shortcode('thankyou_shortcode', array($this, 'thankyou_input_output_ui'));
        
        add_action( 'wp_enqueue_scripts', array($this, 'survey_enqueue') );
       
        add_action("wp_ajax_validateSurvey", array($this,"validateSurvey"));
        add_action("wp_ajax_nopriv_validateSurvey", array($this,"validateSurvey"));
        
        add_action( 'wp_ajax_nopriv_get_url_param',array($this, 'get_url_param'));
        add_action( 'wp_ajax_get_url_param',array($this, 'get_url_param'));
        
        add_action( 'wp_ajax_nopriv_get_ticket_details',array($this,  'get_ticket_details'));
        add_action( 'wp_ajax_get_ticket_details',array($this, 'get_ticket_details'));

        add_action( 'wp_ajax_nopriv_get_single_ticket_detail',array($this, 'get_single_ticket_detail'));
        add_action( 'wp_ajax_get_single_ticket_detail',array($this, 'get_single_ticket_detail'));

        add_action( 'wp_ajax_nopriv_post_ticket_details',array($this, 'post_ticket_details'));
        add_action( 'wp_ajax_show_post_ticket_details',array($this, 'post_ticket_details'));
    }
    public function survey_enqueue() {
        $language = pll_current_language();
        wp_enqueue_script( 'ajax-script', plugin_dir_url( dirname( __FILE__ ) ) . 'template/assets/js/nfsc-survey.js', array('jquery') );
    }
    public function survey_input_output_ui(){
        require_once plugin_dir_path( __FILE__ ) . '../template/nfsc-survey-html.php';
    }
    public function payment_schedule_input_output_ui(){
        require_once plugin_dir_path( __FILE__ ) . '../template/nfsc-welcome-html.php';
    }
    public function thankyou_input_output_ui(){
        require_once plugin_dir_path( __FILE__ ) . '../template/thankyou-html.php';
    }

    /** This is for get url and parameter from the URL
     * 
     */
    public function get_url_param(){
        $customerName = $_GET['TicketReferenceNo'];
        $surveyno = $_GET['SurveyId'];
        return array($customerName,  $surveyno);
     }
	public function post_ticket_details() {
        session_start();
		$CustomerName =  $_POST['CustomerName'];
		$SurveyID =  $_POST['SurveyID'];
        $AccountNumber =   $_POST['AccountNumber'];
		$TicketID =  $_POST['TicketID'];
        $answerdetails =  $_POST['answerdetails'];
		$param = array(
			'CustomerName' => $CustomerName,
			'SurveyID' => $SurveyID,
			'AccountNumber' => $AccountNumber,
			"TicketID"=> $TicketID,
            "QuestionsAnswers"=> $answerdetails			
		);
        $this->nfsc_log($param);
        	$url = "https://lmswebapi.clariontechnologies.co.in/api/CRMQuestionnaires";
			$data = $this->processPostHttpService($url, $param);
            $this->nfsc_log($data);
			if($data->StatusCode){
				$response = array('status' => true);
				$logData = array('body' => 'Updated Data Successfully');
			} else {
				$response = array('status' => false, 'msg' => "data not submitted");
				$logData = array('body' => 'API server error');
			}
		
		$this->nfsc_log($logData);
		echo json_encode($response);
		session_destroy();
		die;
	}

    /** This is for get ticket details
     * 
     */
    public function get_ticket_details($TicketId, $surveyId){
            $param = array (
                'TicketReferenceNo' => $TicketId,
                'SurveyId' => $surveyId
            );
        $url = 'https://lmswebapi.clariontechnologies.co.in/api/CRMQuestionnaires';
        return $this->processGetHttpService($url, $param);  
        // $jsonfile = file_get_contents( plugin_dir_path( __FILE__ ) . '../includes/jsonfile.json');
        // return json_decode($jsonfile);      
    }
    /**
	 * This is HTTP GET Method of the WordPress.
	 *
	 * @since 1.0.1
	 *
	 * @return json
	 */
	public function processGetHttpService( $url, $param ){
		$param_string      = http_build_query($param);
		$final_url         = $url . '?' . $param_string;
		$getData           = wp_remote_get($final_url);
		try {
			return json_decode($getData['body']);
		} catch (Exception $e) {
			return $e->getMessage();
		}
	}
    
    /**
	 * This is HTTP POST Method of the WordPress.
	 *
	 * @since 1.0.1
	 *
	 * @return json
	 */
	public function processPostHttpService( $url, $param ){   
		$final_url         = $url;
		try {
			$responseData = wp_remote_post($final_url, array(
				'body'        =>  $param
				));
			return json_decode($responseData['body']);
		} catch (Exception $e) {
			return $e->getMessage();
			}
	}
    /**
	 * This is log file function
	 *
	 * @since 1.0.1
	 *
	 * @return json
	 */
    public function nfsc_log($logdata){
		$log_filename = plugin_dir_path( dirname( __FILE__ ) ) . '/logs';
		if (!file_exists($log_filename)) 
		{
			// create directory/folder uploads.
			mkdir($log_filename, 0777, true);
		}
		$log  =  "User: ".$_SERVER['REMOTE_ADDR'].' - '.date("F j, Y, g:i a").PHP_EOL.
		json_encode($logdata).PHP_EOL.
            "-------------------------".PHP_EOL;
		file_put_contents($log_filename . '/log_'.date("j.n.Y").'.log', $log, FILE_APPEND);
	}
}