<?php

/**
 * Fired during plugin activation
 *
 * @link       https://profiles.wordpress.org/yogeshdalavi/
 * @since      1.0.0
 *
 * @package    Nfsc_survey
 * @subpackage Nfsc_survey/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Nfsc_survey
 * @subpackage Nfsc_survey/includes
 * @author     Sachin Nawale <sachin.nawale@clariontech.com>
 */
class Nfsc_survey_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {
	}

}
