<?php
$language = pll_current_language();
$lblWelcome = ($language === 'ar') ? 'مرحبا بكم' : 'Welcome';
$lblSubmit = ($language === 'ar') ? 'بدء المسح' : 'Start the Survey';
$lblSurveyTicket = ($language === 'ar') ? 'تذكرة استطلاع العملاء' : 'Customer Survey Ticket';
$lblPrevious = ($language === 'ar') ? 'السابق' : 'Prev';
$lblNext = ($language === 'ar') ? 'التالي' : 'Next';
$lblalredySubmitted = ($language === 'ar') ? 'تم تقديم نموذج المسح بالفعل' : 'Survey form is Already Submitted';
$lblThankyou = ($language === 'ar') ? 'شكرا لك على ملاحظاتك القيمة' : 'Thank you for your valuable feedback';
$lblTicketNoExist = ($language === 'ar') ? 'معرف التذكرة غير موجود!' : 'Ticket ID does not exist!';
$validationText = ($language === 'ar') ? 'يجب أن يكون النص من 10 إلى 100 حرف' : 'The text should be 10 to 100 characters';
$validationRequiredQuestion = ($language === 'ar') ? 'السؤال مطلوب' : 'The Question is required';
?>