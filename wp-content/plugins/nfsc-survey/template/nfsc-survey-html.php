<?php
session_start();
$plugin_aseets_path = plugin_dir_url( dirname( __FILE__ ) ) ;
require_once plugin_dir_path( __FILE__ ) .'lang/lang.php';
$objAdmin = new Nfsc_survey('nfsc-survey', '1.0.1');
date_default_timezone_set("Asia/Riyadh"); 

$TicketId = $_SESSION['TicketId'];
$surveyId = $_SESSION['surveyId'];
$QuestionDatas = $objAdmin->get_ticket_details($TicketId, $surveyId);
$QuestionData = $QuestionDatas->Questionnairelist;
$customerName = $QuestionData[0]->CustomerFullName;
$accountNumber = $QuestionData[0]->MortgageLoanAccountNumber;
$ticketID = $QuestionData[0]->TicketID;
$surveyID = $QuestionData[0]->SurveyID;
?>
<link rel="stylesheet" href="<?php echo $plugin_aseets_path.'template/assets/css/bootstrapValidator.min.css';?>">
<link rel="stylesheet" href="<?php echo $plugin_aseets_path.'template/assets/css/nfsc-survey.css';?>">
<script type="text/javascript" >
      var  ajaxurl = "<?php echo admin_url('admin-ajax.php'); ?>";
      var validationText = "<?php echo $validationText; ?>";
      var validationRequiredQuestion = "<?php echo $validationRequiredQuestion; ?>";
</script>
<head>
<title>Survey Dashboard</title>
<script type="text/javascript" src="<?php echo $plugin_aseets_path.'template/assets/js/jquery-3.3.1.min.js';?> ">
</script>
<script type="text/javascript">
      	$(document).ready(function(){
			$("#myForm").multiStepForm(
			{
				// defaultStep:0,
				callback : function(){
					console.log("save");
				}
			}
			).navigateTo(0);
		});
	</script>
</head>

<?php if(pll_current_language() === 'ar'){ ?>
   <?php $thankyou_url = home_url().'/thank-you-ar'; ?>
    <link rel="stylesheet" href="https://cdn.rtlcss.com/bootstrap/v4.5.3/css/bootstrap.min.css">
<?php }else{ ?>
    <?php $thankyou_url = home_url().'/en/thank-you-en'; ?>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css">
<?php  } ?>

<div class="container">
    <?php if($QuestionData != null){ ?>
    <div class="row justify-content-center invoice_input_container">
        <div class="col-md-8 mt-6 border-radius" style="background-color: #fff;">
            <form class="mx-auto survey-questions-form" id="myForm">
                <div class="" style="padding:10px; margin-top:8px;">
                          <?php 
                           foreach($QuestionData as $singleQuestion) { 
                               $QuestionNo = $singleQuestion->Seq;
                               $QuestionID = $singleQuestion->QuestionID;
                               $Question = $singleQuestion->Question;
                               $QuestionTypeID = $singleQuestion->QuestionTypeID;
                               $IsMandatory = $singleQuestion->IsMandatory;
                               $IsMandatory  = var_export($IsMandatory, true);
                                if($QuestionTypeID == 1){
                                    $inputType = "textbox";
                                } else if($QuestionTypeID == 2){
                                    $inputType = "ratings";
                                }
                                else if($QuestionTypeID == 3){
                                    $inputType = "radio";
                                }
                                else{
                                    $inputType = "checkbox";
                                }?>
                              
                              <div class="form-group tab">
                                   <label class="question-id" style="font-size:17px;" questionId="<?php echo $QuestionID;?>" questiontypeid="<?php echo $QuestionTypeID;?>" ismandatory="<?php echo $IsMandatory;?>"><?php echo 'Question  ' . $QuestionNo; ?></label><hr style="max-width:initial; margin-top: 0em;border-top: 1px solid;">
                                   <div class="question-row">
                                   <?php if($IsMandatory == "true") { ?>
                                    <label class="question" style="font-size:17px; margin-left: 10px;"><?php echo $Question;?><span class="star-icon"> *</span></label>
                                    <?php $QType = 'required_Id'; ?>
                                    <?php }else{ ?>
                                        <label class="question" style="font-size:17px; margin-left: 10px;"><?php echo $Question;?></label>
                                        <?php $QType = 'optional_Id'; ?>
                                    <?php } ?>
                                    </div>
                                    <div class="answer-row">
                                    <?php $answerData = $QuestionData[$QuestionNo-1]->AnswerOptions; ?>
                                                                  
                                    <?php foreach($answerData as $singleAnswer) { ?>
                                        <?php if($inputType != "ratings") { 
                                            if($QuestionTypeID != 1){  ?>
                                                <p class="mb-1"></p>
                                                    <input type="<?php echo $inputType; ?>" style="margin-left:20px; transform:scale(1.2)" class="answerId" id="<?php echo  $singleAnswer->ID; ?>" qtype="<?php echo $QType; ?>" name="<?php echo $QuestionID;?>" value="<?php echo  $singleAnswer->Option; ?>">
                                                    <label for="<?php echo  $singleAnswer->Option; ?>" style="font-size:17px;"><?php echo  $singleAnswer->Option; ?></label>  
                                                                                                
                                            <?php }else{ ?>
                                                  <textarea cols="60" rows="3" class="answerId textArea" id="<?php echo  $singleAnswer->ID; ?>" qtype="<?php echo $QType; ?>" name="answerId"></textarea>
                                                  <div class="answerId textarea-error" style="display:none; text-align:center;color: red; font-size: 0.8em;"><?php echo $validationText; ?></div>
                                            <?php } ?>
                                      
                                        <?php }else{ ?>
                                            <p class="mb-1"></p>
                                            <fieldset class="rating">
                                           
                                            <?php for($i = 0; $i < $singleAnswer->Option; $i++ ){ 
                                                 
                                                $QuestionNos = $QuestionID . "a". $i; 
                                                $ratingId = 'rating_'.$QuestionNos;
                                                $ratingValue = $i+1;
                                                ?>
                                                <input type="radio" style="margin-left:20px; transform:scale(1.2)" class="answerId" id="<?php echo $ratingId; ?>" name="<?php echo $singleAnswer->ID;?>" ratingid="<?php echo  $singleAnswer->ID; ?>" value="<?php echo $ratingValue; ?>">
                                                <label for="<?php echo $ratingId; ?>" style="font-size:17px;"><?php echo "test"; ?></label>
                                            <?php } ?>   
                                                <div class="stars">
                                           <?php for($j = 0; $j < $singleAnswer->Option; $j++ ){
                                                $QuestionSeq = $QuestionID . "a" . $j;
                                                $ratingId = 'rating_'.$QuestionSeq;
                                                $ratingValue = $j+1;
                                                 ?>
                                                <label for="<?php echo $ratingId; ?>" style="font-size:17px;" aria-label="<?php echo $ratingValue .' stars' ; ?>" title="<?php echo $ratingValue .' stars' ; ?>"></label>
                                             <?php } ?>
                                            </div>
                                            </fieldset>
                                        <?php } ?>
                                    <?php } ?>
                                    </div>
                                    <?php if($QType == 'required_Id'){ ?>
                                    <div class="question-validation" style="display:none; text-align:center;color: red; font-size: 0.8em;"><?php echo $validationRequiredQuestion; ?></div>
                                          <?php } ?>
                                </div>
                        <?php }  

                          ?>

                </div>
                <div class="form-output-details" style="display:none;">
                </div>
                <input type="hidden" name="qty" class="qty" maxlength="12" value="" class="input-text qty" />
                <input type="hidden" value="<?php echo $thankyou_url; ?>" name="thankyou_url" class="thankyou_url">
                <input type="hidden" value="<?php echo $customerName; ?>" name="customerName" class="customerName">
                <input type="hidden" value="<?php echo $accountNumber; ?>" name="accountNumber" class="accountNumber">
                <input type="hidden" value="<?php echo $ticketID; ?>" name="ticketID" class="ticketID">
                <input type="hidden" value="<?php echo $surveyID; ?>" name="surveyID" class="surveyID">


                <div class="form-group prev-next-btn-group">
                <button type="button" class="btn btn-gray mt-2 survey-prev previous"><?php echo $lblPrevious; ?></button>
                <button type="button" class="btn bg-gradient-info mt-2 survey-next next" style="right:0"><?php echo $lblNext; ?></button>
                <button type="button" class="btn bg-gradient-info mt-2 survey-submit submit" style="display:none"><?php echo "Submit"; ?></button>
                </div>
            </form>
        </div>
    </div>
    <?php }else{ ?>
        <div class="form-group" style="box-shadow: 0px 0px 10px; padding: 10px;">
            <?php echo $lblalredySubmitted; ?>
        </div>
        <?php } ?>
</div>
<script type="text/javascript" src="<?php echo $plugin_aseets_path.'template/assets/js/bootstrapValidator.min.js'; ?>"></script>
<script type="text/javascript" src="<?php echo $plugin_aseets_path.'template/assets/js/nfsc-survey.js'; ?>"></script> 
