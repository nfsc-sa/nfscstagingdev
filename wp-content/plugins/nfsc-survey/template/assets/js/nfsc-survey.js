var answerArr = [];
var checkboxArr = [];
function onlyNumber (obj) {
    var $this = jQuery(obj);
        $this.val($this.val().replace(/\D/g, ''));
}
var initializeGlobelCalls = function () {
    var start_survey = function(){
        if($('#payment-schedule-form').length){
            $('.btn-submit').click(function(e){
                e.preventDefault();
               $('.btn-submit').html('<span class="spinner-border spinner-border-sm  m-1" role="status" aria-hidden="true"></span>');
                window.location.href = $('.dashboard_url').val();
            });
        }
    };
    var geturlparam = function(){
        $( document ).ready(function() {
            const getUrlParameter = function getUrlParameter(sParam) {
                var sPageURL = window.location.search.substring(1),
                    sURLVariables = sPageURL.split('&'),
                    sParameterName,
                    i;
                for (i = 0; i < sURLVariables.length; i++) {
                    sParameterName = sURLVariables[i].split('=');
                    if (sParameterName[0] === sParam) {
                        return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
                    }
                }
                return false;
            };
            const TicketReferenceNo = getUrlParameter('TicketReferenceNo');
             // var currentURLs = window.location.href;
            $.ajax({
                    url: ajaxurl,
                    data: {
                        action: 'get_url_param',
                         TicketReferenceNo: TicketReferenceNo
                    },
                    type: "POST",
                    success: function(response) {
                        try {
                            const result =  jQuery.parseJSON(response);
                          } catch (err) {
                            // 👇️ This runs
                            console.log('Error: ', err.message);
                          }
                    }
                });
         });
    };
    var getticketdetails = function(){
        $( document ).ready(function() {
            // console.log(validationText);
        });
            $('.survey-next').click(function(){
                 output = $('input[name=interest]:checked', '#SurveryAnswer').val();
                // var questionIds = '';
                 let location = $(this).parent().parent().find('div.form-group.tab.current');
                 var questionIds = location.find('.question-id').attr('questionid');
                 let questionTypeId = location.find('.question-id').attr('questionTypeId');
                 var answerId = '';
                 var answerText = '';
                 if(questionTypeId == 1){
                    answerId = location.find('.answerId').attr('id');
                    answerText = location.find('.answerId').val();
                }
                else if(questionTypeId == 2){
                    answerId = location.find('.answerId:checked').attr('ratingid');
                    answerText = location.find('.answerId:checked').val();
                    // inputtype = 'radio';
                }
                else if(questionTypeId == 3){
                    answerId = location.find('.answerId:checked').attr('id');
                    answerText = location.find('.answerId:checked').val();
                    // answerText = "";
                    // inputtype = 'radio';
                }
                else if(questionTypeId == 4){
                    // inputtype = 'checkbox';
                    var questionId = '';
                    var answerIdCheck = '';
                    var answerTextCheck = '';
                    questionId = location.find('.question-id').attr('questionId');
                    answerArr = answerArr.filter(x => x.QuestionsID != questionId);
                     location.find('.answerId:checked').each(function(i, j){
                        answerIdCheck = $(this).attr('id');
                        answerTextCheck = $(this).val();
                        if(questionTypeId == 4){
                                answerId = answerIdCheck;
                                answerText = answerTextCheck;
                                const answerData = {
                                        "QuestionsID": questionId,
                                        "AnswersID": answerId,
                                        "AnswersInText": answerText
                                    };
                                    answerArr.push(answerData);
                        }
                    });
                }
                else{
                   answerId = location.find('.answerId').attr('id');
                    answerText = location.find('.answerId').val();
                }
                if(questionTypeId != 4){
                     if(answerArr.length>0){
                       let found = false;
                        answerArr.forEach(function(obj, j) {
                            x = obj;
                             if (x.QuestionsID == questionIds) {
                                found = true;
                                x.AnswersID = answerId;
                                x.AnswersInText = answerText;
                            }
                        });
                        if(!found){
                            const answerData = {
                                "QuestionsID": questionIds,
                                "AnswersID": answerId,
                                "AnswersInText": answerText
                            };
                            answerArr.push(answerData);
                            answerArr = answerArr.filter(x => x.AnswersID != undefined);
                            answerArr = answerArr.filter(x => x.AnswersInText != '');
                        }
                    }else{
                        const answerData = {
                            "QuestionsID": questionIds,
                            "AnswersID": answerId,
                            "AnswersInText": answerText
                        };
                        answerArr.push(answerData);
                        answerArr = answerArr.filter(x => x.AnswersID != undefined);
                        answerArr = answerArr.filter(x => x.AnswersInText != '');
                    }
                }
            });
            // On clicking submit do following
        $("input[type=submit]").click(function() {
            let atLeastOneChecked = false;
            $("input[type=radio]").each(function() {
                // If radio button not checked
                // display alert message
                if ($(this).attr("checked") !== "checked") {
                    // Alert message by displaying
                    // error message
                    $("#msg").html(
        "<span class='alert alert-danger' id='error'>"
        + "Please Choose atleast one</span>");
                }
            });
        });
    };
    var submitDetails = (function ( $ ) {
        $.fn.multiStepForm = function(args) {
            if(args === null || typeof args !== 'object' || $.isArray(args))
              throw  " : Called with Invalid argument";
              let form = this;
              let tabs = form.find('.tab');
              let steps = form.find('.step');
            steps.each(function(i, e){
              $(e).on('click', function(ev){
                form.navigateTo(i);
              });
            });
            form.navigateTo = function (i) {/*index*/
              /*Mark the current section with the class 'current'*/
              tabs.removeClass('current').eq(i).addClass('current');
              // Show only the navigation buttons that make sense for the current section:
              form.find('.previous').toggle(i > 0);
              atTheEnd = i >= tabs.length - 1;
              form.find('.next').toggle(!atTheEnd);
              // console.log('atTheEnd='+atTheEnd);
              form.find('.submit').toggle(atTheEnd);
              fixStepIndicator(curIndex());
              return form;
            }
            function curIndex() {
              /*Return the current index by looking at which section has the class 'current'*/
              return tabs.index(tabs.filter('.current'));
            }
            function fixStepIndicator(n) {
              steps.each(function(i, e){
                i == n ? $(e).addClass('active') : $(e).removeClass('active');
              });
            }
            /* Previous button is easy, just go back */
            form.find('.previous').click(function() {
                let location = $(this).parent().parent().find('div.form-group.tab.current');
                let isMandatory = location.find('.question-id').attr('ismandatory');
                let questionIds = location.find('.question-id').attr('questionid');
                    if(isMandatory == "true"){
                        const filter = answerArr.filter(x => x.QuestionsID == questionIds);
                        if( filter.length == 0 || filter[0].AnswersID == ''){
                            $('.question-validation').css('display','none');
                        }
                    }
                    $('.question-validation').css('display','none');

              form.navigateTo(curIndex() - 1);
            });
            /* Next button goes forward iff current block validates */
            form.find('.next').click(function() {
              if('validations' in args && typeof args.validations === 'object' && !$.isArray(args.validations)){
                if(!('noValidate' in args) || (typeof args.noValidate === 'boolean' && !args.noValidate)){
                  form.validate(args.validations);
                  if(form.valid() == true){
                    form.navigateTo(curIndex() + 1);
                    return true;
                  }
                 return false;
                }
              }
              let location = $(this).parent().parent().find('div.form-group.tab.current');
              let isMandatory = location.find('.question-id').attr('ismandatory');
              let questionIds = location.find('.question-id').attr('questionid');
              let qtypeid = location.find('.question-id').attr('questiontypeid');
                var filter = answerArr.filter(x => x.QuestionsID == questionIds);
                if(qtypeid == 1 && filter != ''){
                    const textlength = filter[0].AnswersInText;
                    if( textlength != '' && textlength.length < 10 || textlength.length > 100){
                        $('.textarea-error').css('display','block');
                        $('.question-validation').css('display','none');
                        return;
                    }
                    $('.textarea-error').css('display','none');
                }
                if(isMandatory == "true"){
                    const filter = answerArr.filter(x => x.QuestionsID == questionIds);
                    if( filter.length == 0 || filter[0].AnswersID == '' || filter[0].AnswersInText == ''){
                        console.log(filter.length);
                        $('.question-validation').css('display','block');
                        return;
                    }
                }
                $('.question-validation').css('display','none');
              form.navigateTo(curIndex() + 1);
            });
            form.find('.submit').on('click', function(e){
                var result = window.sessionStorage.getItem('result');
                let CustomerName = $('.customerName').val();
                let SurveyID = $('.surveyID').val();
                let AccountNumber = $('.accountNumber').val();
                let TicketID = $('.ticketID').val();
                //var questionIdss = '';
                let locations = $(this).parent().parent().find('div.form-group.tab.current');
                let isMandatorys = locations.find('.question-id').attr('ismandatory');
                var questionIdss = locations.find('.question-id').attr('questionid');
                let questionTypeIds = locations.find('.question-id').attr('questionTypeId');
                 var answerIds = '';
                 var answerTexts = '';
                 if(questionTypeIds == 1){
                    answerIds = locations.find('.answerId').attr('id');
                    answerTexts = locations.find('.answerId').val();
                }
                else if(questionTypeIds == 2){
                    answerIds = locations.find('.answerId:checked').attr('ratingid');
                    answerTexts = locations.find('.answerId:checked').val();
                    // inputtype = 'radio';
                }
                else if(questionTypeIds == 3){
                    answerIds = locations.find('.answerId:checked').attr('id');
                    answerTexts = locations.find('.answerId:checked').val();
                }
                else if(questionTypeIds == 4){
                    // inputtype = 'checkbox';
                    var questionId = '';
                    var answerIdCheck = '';
                    var answerTextCheck = '';
                    questionId = locations.find('.question-id').attr('questionId');
                    answerArr = answerArr.filter(x => x.QuestionsID != questionId);
                    locations.find('.answerId:checked').each(function(i, j){
                        answerIdCheck = $(this).attr('id');
                        answerTextCheck = $(this).val();
                        if(questionTypeIds == 4){
                                answerIds = answerIdCheck;
                                answerTexts = answerTextCheck;
                                var answerData = {
                                        "QuestionsID": questionId,
                                        "AnswersID": answerIds,
                                        "AnswersInText": answerTexts
                                    };
                                answerArr.push(answerData);
                        }
                    });
                }
                else{
                   answerIds = locations.find('.answerId').attr('id');
                    answerTexts = locations.find('.answerId').val();
                }
                if(questionTypeIds != 4){
                    answerArr = answerArr.filter(x => x.AnswersID != answerIds);
                    if(answerArr.length>0){
                        let found = false;
                        answerArr.forEach(function(obj, j) {
                            x = obj;
                            //  console.log(obj)
                            if (x.QuestionsID == questionIdss) {
                                found = true;
                                x.AnswersID = answerIds;
                                x.AnswersInText = answerTexts;
                            }
                        });
                        if(!found){
                            var answerData = {
                                "QuestionsID": questionIdss,
                                "AnswersID": answerIds,
                                "AnswersInText": answerTexts
                            };
                            answerArr.push(answerData);
                            answerArr = answerArr.filter(x => x.AnswersID != undefined);
                            answerArr = answerArr.filter(x => x.AnswersInText != '');
                        }
                    }else{
                        var answerData = {
                            "QuestionsID": questionIdss,
                            "AnswersID": answerIds,
                            "AnswersInText": answerTexts
                        };
                        answerArr.push(answerData);
                        answerArr = answerArr.filter(x => x.AnswersID != undefined);
                        answerArr = answerArr.filter(x => x.AnswersInText != '');
                    }
                }
                const answerdetails = answerArr;
                var filter = answerArr.filter(x => x.QuestionsID == questionIdss);
                if(questionTypeIds == 1 && filter != ''){
                    const textlength = filter[0].AnswersInText;
                    if( textlength != '' && textlength.length < 10 || textlength.length > 100){
                        $('.textarea-error').css('display','block');
                        $('.question-validation').css('display','none');
                        return;
                    }
                    $('.textarea-error').css('display','none');
                }
                if(isMandatorys == "true"){
                  var filter = answerArr.filter(x => x.QuestionsID == questionIdss);
                  if( filter.length == 0 || filter[0].AnswersID == '' || filter[0].AnswersInText == ''){
                      $('.question-validation').css('display','block');
                    //   $("input").prop('required',true);
                      return;
                  }
                }
                $.ajax({
                    url: ajaxurl,
                    data: {
                        action: 'post_ticket_details',
                        CustomerName: CustomerName,
                        SurveyID: SurveyID,
                        AccountNumber: AccountNumber,
                        TicketID: TicketID,
                        answerdetails: answerdetails
                    },
                    type: "POST",
                    success: function(response) {
                        try {
                            const result = jQuery.parseJSON(response);
                            if(result.status == true){
                            window.location.href = $('.thankyou_url').val();
                            }
                        } catch (err) {
                            // 👇️ This runs
                            console.log('Error: ', err.message);
                        }
                    }
                });
              });
            /*By default navigate to the tab 0, if it is being set using defaultStep property*/
            typeof args.defaultStep === 'number' ? form.navigateTo(args.defaultStep) : null;
            form.noValidate = function() {
            };
            return form;
        };
      }( jQuery ));
    return {
        init: function () {
            start_survey();
            geturlparam();
            getticketdetails();
            // postticketdetails();
            // submitDetails();
        }
    };
}();
