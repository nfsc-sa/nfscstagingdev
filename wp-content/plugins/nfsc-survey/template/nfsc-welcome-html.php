<?php
session_start();
$plugin_aseets_path = plugin_dir_url( dirname( __FILE__ ) ) ;
require_once plugin_dir_path( __FILE__ ) .'lang/lang.php';
$objAdmin = new Nfsc_survey('nfsc-survey', '1.0.1');
$userData = $objAdmin->get_url_param();
$_SESSION['TicketId'] = $userData[0];
$_SESSION['surveyId'] = $userData[1];
$TicketId = $_SESSION['TicketId'];
$surveyId = $_SESSION['surveyId'];
$QuestionData = $objAdmin->get_ticket_details($TicketId, $surveyId);
$statusCode = $QuestionData->StatusCode;
?>

<link rel="stylesheet" href="<?php echo $plugin_aseets_path.'template/assets/css/bootstrap-datepicker.min.css';?>">
<link rel="stylesheet" href="<?php echo $plugin_aseets_path.'template/assets/css/bootstrapValidator.min.css';?>">
<link rel="stylesheet" href="<?php echo $plugin_aseets_path.'template/assets/css/nfsc-survey.css';?>">
<script type="text/javascript" >
      var  ajaxurl = "<?php echo admin_url('admin-ajax.php'); ?>";
</script>
<?php if(pll_current_language() === 'ar'){ 
    $dashboard_url = home_url().'/survey-dashboard-ar'; ?>
    <link rel="stylesheet" href="https://cdn.rtlcss.com/bootstrap/v4.5.3/css/bootstrap.min.css">
<?php }else{ 
    $dashboard_url = home_url().'/en/survey-dashboard-en/'; ?>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css">
<?php  } ?>

<div class="container">
<?php if($statusCode == "200"){ ?>
    <div class="row justify-content-center invoice_input_container">
        <div class="col-md-4 mt-6 shadow-lg p-4 border-radius" style="background-color: #fff;">
            <form class="mx-auto" id="payment-schedule-form">
                <div class="form-group">
                    <label for="nationalIDInput"><?php echo $lblWelcome; ?></label>
                    <?php foreach($QuestionData as $CustomerName){
                        $CustomerName = $CustomerName[0]->CustomerFullName;
                    } ?>
                    <label for="nationalIDInput"><Strong><?php echo $CustomerName; ?></strong></label><br>
                    <label for="nationalIDInput"><?php echo $lblSurveyTicket . ' :'; ?></label>
                    <label for="nationalIDInput"><Strong><?php echo $userData[0]; ?></strong></label>
               </div>
                <button type="submit" class="btn btn-new btn-submit w-100 mt-3"><?php echo $lblSubmit; ?></button>
                <input type="hidden" value="<?php echo $dashboard_url; ?>" name="dashboard_url" class="dashboard_url">
            </form>
        </div>
    </div>
    <div class="invoice-details-container" style="display:none;">
        <div class="row justify-content-center">
            <div class="col-md-8 mt-6">
                <div class="row">
                       <div class="col-md-12 text-center mb-5">
                           <div class="invoice-section padding-invoice">
                                <h4 class="outpaymentScheduleDate pt-2"></h4>
                                <div class="p-3">
                                    <a class="btn btn-new download-link" target="_blank" href="#"><?php echo $lblDownloadPDF; ?></a>
                                <button type="button" class="btn btn-gray ml-3 backInvoiceInput"><?php echo $lblBack; ?></button>
                            </div>
                          </div>  
                          
                      </div>
                </div>
            </div>
        </div>
        <div class="row justify-content-center">
            <div>
                
            </div>
        </div>
    </div> 
<?php } else if($statusCode == "208"){ ?>
        <div class="form-group" style="box-shadow: 0px 0px 10px; padding: 10px;">
            <?php echo $lblalredySubmitted; ?>
        </div>
        <?php }else{
           ?>
           <div class="form-group" style="box-shadow: 0px 0px 10px; padding: 10px;">
               <?php echo $lblTicketNoExist; ?>
           </div>
           <?php 
        } ?>    
</div>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flatpickr/4.2.3/flatpickr.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/flatpickr/4.2.3/flatpickr.js">
<script src="<?php echo $plugin_aseets_path.'template/assets/js/bootstrap-datepicker.min.js';?>" ></script>
<script type="text/javascript" src="<?php echo $plugin_aseets_path.'template/assets/js/bootstrapValidator.min.js'; ?>"></script>
 <script type="text/javascript" src="<?php echo $plugin_aseets_path.'template/assets/js/nfsc-survey.js'; ?>"></script> 