<?php
session_start();
$plugin_aseets_path = plugin_dir_url( dirname( __FILE__ ) ) ;
require_once plugin_dir_path( __FILE__ ) .'lang/lang.php';
$objAdmin = new Nfsc_survey('nfsc-survey', '1.0.1');
$userData = $objAdmin->get_url_param();
$_SESSION['TicketId'] = $userData[0];
$_SESSION['surveyId'] = $userData[1];
$TicketId = $_SESSION['TicketId'];
$surveyId = $_SESSION['surveyId'];
$QuestionData = $objAdmin->get_ticket_details($TicketId,$surveyId);
?>

<link rel="stylesheet" href="<?php echo $plugin_aseets_path.'template/assets/css/bootstrap-datepicker.min.css';?>">
<link rel="stylesheet" href="<?php echo $plugin_aseets_path.'template/assets/css/bootstrapValidator.min.css';?>">
<link rel="stylesheet" href="<?php echo $plugin_aseets_path.'template/assets/css/nfsc-survey.css';?>">
<script type="text/javascript" >
      var  ajaxurl = "<?php echo admin_url('admin-ajax.php'); ?>";
</script>
<?php if(pll_current_language() === 'ar'){ 
    $dashboard_url = home_url().'/survey-dashboard-ar'; ?>
    <link rel="stylesheet" href="https://cdn.rtlcss.com/bootstrap/v4.5.3/css/bootstrap.min.css">
<?php }else{ 
    $dashboard_url = home_url().'/en/survey-dashboard-en/'; ?>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css">
<?php  } ?>

<div class="container">
    <div class="row justify-content-center invoice_input_container">
        <div class="col-md-4 mt-6 shadow-lg p-4 border-radius" style="background-color: #fff;">
            <form class="mx-auto" id="payment-schedule-form">
                <div class="form-group" style="text-align:center; margin-bottom:0;">
                    <label for="feedbackInput"><strong><?php echo $lblThankyou; ?></stong></label>
               </div>
            </form>
        </div>
    </div>     
</div>
<script>
var home_url = '<?php echo home_url(); ?>'
setTimeout(function () {
   //Redirect with JavaScript
   window.location.href= home_url;
}, 3000);
</script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flatpickr/4.2.3/flatpickr.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/flatpickr/4.2.3/flatpickr.js">
<script src="<?php echo $plugin_aseets_path.'template/assets/js/bootstrap-datepicker.min.js';?>" ></script>
<script type="text/javascript" src="<?php echo $plugin_aseets_path.'template/assets/js/bootstrapValidator.min.js'; ?>"></script>
 <script type="text/javascript" src="<?php echo $plugin_aseets_path.'template/assets/js/nfsc-survey.js'; ?>"></script> 