<?php
$language = pll_current_language();
$lblInvoiceDate = ($language === 'ar') ? 'تاريخ الفاتورة' : 'Invoice Date';
$lblInvoiceRefNumber = ($language === 'ar') ? 'رقم الفاتورة' : 'Invoice Referance Number';
$lblSubmit = ($language === 'ar') ? 'إرسال' : 'Submit';
$lblClientName = ($language === 'ar') ? 'العميل' : 'Client Name';
$lblInvoiceSubject = ($language === 'ar') ? 'موضوع الفاتورة' : 'Invoice Subject';
$textInvoiceSubject = ($language === 'ar') ? 'خدمات إدارة المحافظ العقارية' : 'Real Estate Portfolio Management Services';
$lblVatNumber = ($language === 'ar') ? 'الرقم الضريبي للعميل' : 'Customer VAT Number';
$lblSRC = ($language === 'ar') ? 'الشركة السعودية لإعادة التمويل العقاري' : 'Saudi Real Estate Refinance Company';
$lblNFSC =  ($language === 'ar') ? 'الشركة الوطنية لخدمات التمويل' : 'National Financing Services Company';
$lblPriceBeforeVat = ($language === 'ar') ? 'السعر قبل الضريبة' : 'Price before VAT';
$lblPriceVat = ($language === 'ar') ? 'السعر شامل الضريبة' : 'VAT Amount';
$lblGrandTotal = ($language === 'ar') ? 'المجموع الكلي' : 'Grand Total';
$lblBillerVat = ($language === 'ar') ? 'الرقم الضريبي لمقدم الخدمة' : 'Service Provider VAT';
$lblBiller = ($language === 'ar') ? 'مقدم الخدمة' : 'Service Provider';
$lblInvoiceDetails = ($language === 'ar') ? 'تفاصيل الفاتورة' : 'Invoice Details';
$errorInvoiceNotFound = ($language === 'ar') ? 'الفاتورة غير متاحة ' : 'Invoice not found'; 
$lblNationalID = ($language === 'ar') ? 'الهوية الوطنية' : 'National ID';
$lblRefernceNumber = ($language === 'ar') ? 'الرقم المرجعي' : 'Reference Number';
$errorPaymentScheduleNotFound = ($language === 'ar') ? 'جدول السداد غير متاح' : 'Payment schedule not found';
$lblBack = ($language === 'ar') ? 'الخلف' : 'Back';
$lblDownloadPDF =  ($language === 'ar') ? 'تحميل' : 'Download PDF';
$lblServicePriod =  ($language === 'ar') ? 'ف? تقدPم الخدمة' : 'Service Period';

$nodSubmit = ($language === 'ar') ? 'إرسال' : 'Submit';
$lblnodDate = ($language === 'ar') ? 'تاريخ الفاتورة' : 'nod Date';
$lblnodRefNumber = ($language === 'ar') ? 'رقم الفاتورة' : 'nod Referance Number';
$lblnodDetails = ($language === 'ar') ? 'تفاصيل الفاتورة' : 'nod Details';
$errornodNotFound = ($language === 'ar') ? 'الفاتورة غير متاحة ' : 'nod not found'; 
$errorNoDNotFound = ($language === 'ar') ? 'لم يتم العثور على إشعار التأخير' : 'Notice of Delay not found';
?>