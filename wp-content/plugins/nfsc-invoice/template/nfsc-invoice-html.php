<?php
$plugin_aseets_path = plugin_dir_url( dirname( __FILE__ ) ) ;
require_once plugin_dir_path( __FILE__ ) .'lang/lang.php';
date_default_timezone_set("Asia/Riyadh"); 
?>
<link rel="stylesheet" href="<?php echo $plugin_aseets_path.'template/assets/css/bootstrap-datepicker.min.css';?>">
<link rel="stylesheet" href="<?php echo $plugin_aseets_path.'template/assets/css/bootstrapValidator.min.css';?>">
<link rel="stylesheet" href="<?php echo $plugin_aseets_path.'template/assets/css/nfsc-invoice.css';?>">
<?php if(pll_current_language() === 'ar'){ ?>
    <link rel="stylesheet" href="https://cdn.rtlcss.com/bootstrap/v4.5.3/css/bootstrap.min.css">
<?php }else{ ?>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css">
<?php  } ?>

<div class="container">
    <div class="row justify-content-center invoice_input_container">
        <div class="col-md-4 mt-6 shadow-lg p-4 border-radius" style="background-color: #fff;">
            <form class="mx-auto" id="invoiceForm">
                <div class="form-group">
                    <label for="invoiceDateInput"><?php echo $lblInvoiceDate; ?></label>
                    <input type="text" class="form-control" id="invoiceDateInput" data-input  name="invoiceDateInput" aria-describedby="emailHelp" placeholder="dd/mm/yyyy">
                </div>
                <div class="form-group">
                    <label for="invoiceReferanceInput"><?php echo $lblInvoiceRefNumber; ?></label>
                    <input type="text" class="form-control" id="invoiceReferanceInput" maxlength="8" name="invoiceReferanceInput"  onkeyup="onlyNumber(this)">
                </div>
                <div class="alert alert-warning error-invoice" role="alert" style="display:none;">
                   <?php echo $errorInvoiceNotFound; ?>
                </div>
                <button type="submit" class="btn btn-new btn-submit w-100 mt-3"><?php echo $lblSubmit; ?></button>
                
            </form>
        </div>
    </div>
    <div class="invoice-details-container" style="display:none;">
        <div class="row justify-content-center">
            <div class="col-md-8 mt-6">
                <div class="row">
                       <div class="col-md-12 text-center mb-5">
                          <h4><?php echo $lblInvoiceDetails; ?></h4>
                      </div>
                      <div class="col-md-6 text-center">
                          <div class="invoice-section">
                                <p><?php echo $lblInvoiceRefNumber; ?></p>
                                <h4><span class="outInvoiceRefNumber"></span></h4>
                          </div>    
                      </div>  
                      <div class="col-md-6 text-center">
                            <div class="invoice-section">
                                <p><?php echo $lblInvoiceDate; ?></p>
                                <h4><span class="outInvoiceDate"></span> 7:30 AM</h4>
                            </div>
                      </div>
                      <div class="col-md-6 text-center">
                            <div class="invoice-section">
                                <p><?php echo $lblClientName; ?></p>
                                <h4><?php echo $lblSRC; ?></h4>
                            </div>
                      </div>  
                      <div class="col-md-6 text-center">
                            <div class="invoice-section">
                                <p><?php echo $lblBiller; ?></p>
                                <h4><?php echo $lblNFSC; ?></h4>
                            </div>
                      </div>
                      <div class="col-md-6 text-center">
                            <div class="invoice-section">
                                <p><?php echo $lblVatNumber; ?></p>
                                <h4>302008745900003</h4>
                            </div>
                      </div>
                      <div class="col-md-6 text-center">
                            <div class="invoice-section">
                                <p><?php echo $lblBillerVat; ?></p>
                                <h4>310533118800003</h4>
                            </div>
                      </div>
                      <div class="col-md-12 text-center">
                        <div class="invoice-section">
                            <p><?php echo $lblInvoiceSubject; ?></p>
                            <h4><?php echo $textInvoiceSubject; ?></h4>
                        </div>
                      </div>
                      <div class="col-md-12 text-center">
                        <div class="invoice-section">
                            <p><?php echo $lblServicePriod; ?></p>
                            <h4> <span class="invoice_service_period"></span></h4>
                        </div>
                      </div>
                      
                      <div class="col-md-6 text-center">
                        <div class="invoice-section">
                            <p><?php echo $lblPriceBeforeVat; ?></p>
                            <h4>300,000.00</h4>
                        </div>
                     </div>
                     <div class="col-md-6 text-center">
                            <div class="invoice-section">
                                <p><?php echo $lblPriceVat; ?></p>
                                <h4>45,000.00</h4>
                            </div>
                      </div>
                      <div class="col-md-12 text-center">
                            <div class="invoice-section">
                                <p><?php echo $lblGrandTotal; ?></p>
                                <h4>345,000.00</h4>
                            </div>
                      </div>
                </div>
            </div>
           
        </div>
        <div class="row justify-content-center">
                <div>
                    <button type="button" class="btn btn-gray mt-2 backInvoiceInput"><?php echo $lblBack; ?></button>
                </div>
            </div>
    </div>
    
</div>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flatpickr/4.2.3/flatpickr.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/flatpickr/4.2.3/flatpickr.js">
<script src="<?php echo $plugin_aseets_path.'template/assets/js/bootstrap-datepicker.min.js';?>" ></script>
<script type="text/javascript" src="<?php echo $plugin_aseets_path.'/template/assets/js/bootstrapValidator.min.js'; ?>"></script>