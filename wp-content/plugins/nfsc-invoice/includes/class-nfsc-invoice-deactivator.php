<?php

/**
 * Fired during plugin activation
 *
 * @link       https://profiles.wordpress.org/yogeshdalavi/
 * @since      1.0.0
 *
 * @package    Nfsc_Invoice
 * @subpackage Nfsc_Invoice/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Nfsc_Invoice
 * @subpackage Nfsc_Invoice/includes
 * @author     Yogesh Dalavi <yogesh.dalavi@clariontechnologies.co.in>
 */
class Nfsc_invoice_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
