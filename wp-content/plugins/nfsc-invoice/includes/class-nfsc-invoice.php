<?php

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       https://profiles.wordpress.org/yogeshdalavi/
 * @since      1.0.0
 *
 * @package    Nfsc_invoice
 * @subpackage Nfsc_invoice/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    Nfsc_invoice
 * @subpackage Nfsc_invoice/includes
 * @author     Yogesh Dalavi <yogesh.dalavi@clariontechnologies.co.in>
 */
class Nfsc_invoice {

    public function __construct() {
        add_shortcode('nod_shortcode', array($this, 'nod_input_output_ui'));
        add_shortcode('invoice_shortcode', array($this, 'invoice_input_output_ui'));
        add_shortcode('payment_schedule_shortcode', array($this, 'payment_schedule_input_output_ui'));
        add_action( 'wp_enqueue_scripts', array($this, 'invoice_enqueue') );
        add_action("wp_ajax_validateInvoice", array($this,"validateInvoice"));
        add_action("wp_ajax_nopriv_validateInvoice", array($this,"validateInvoice"));
        add_action("wp_ajax_valid_payment_schedule_details", array($this,"valid_payment_schedule_details"));
        add_action("wp_ajax_nopriv_valid_payment_schedule_details", array($this,"valid_payment_schedule_details"));

        add_action("wp_ajax_nod_details", array($this,"nod_details"));
        add_action("wp_ajax_nopriv_nod_details", array($this,"nod_details"));

    }
    public function invoice_enqueue() {
        $language = pll_current_language();
        $errorSelectInvoiceDate = ($language === 'ar') ? 'الرجاء اختيار تاريخ الفاتورة' : 'Please select Date';
        $errorAddInvoiceRefNumber = ($language === 'ar') ? 'الرجاء إدخال الرقم المرجعي للفاتورة' : 'Please add referance number';
        $errorDigitInvoiceRefNumber = ($language === 'ar') ? 'الرجاء إدخال الرقم المرجعي للفاتورة' : 'Please enter 6 digit invoice reference number';
        $errorNationalIDValid = ($language === 'ar') ? 'الرجاء إدخال رقم هوية وطنية صحيح' : 'Please enter valid NationalId';
        $errorNationalIDReq = ($language === 'ar') ? 'الرجاء إدخال رقم الهوية الوطنية' : 'National ID is Required';
        $errorNationalIDTenDigit = ($language === 'ar') ? 'يجب أن يتكون رقم الهوية من 10 أرقام على الأقل' : 'National Id must be 10 digits long';
        $pamentScheduleDateMsg = ($language === 'ar') ? 'تم إصدار جدول السداد في تاريخ {date}' : 'Payment Schedule Generated on {date}';
        $nodDateMsg = ($language === 'ar') ? 'تم إنشاء إشعار التأخير في {date}' : 'Notice of Delay Generated on {date}';
        wp_enqueue_script( 'ajax-script', plugin_dir_url( dirname( __FILE__ ) ) . 'template/assets/js/nfsc-invoice.js', array('jquery') );
        wp_localize_script( 'ajax-script', 'my_ajax_object', 
                array( 'ajax_url' => admin_url( 'admin-ajax.php' ),
                'errorSelectInvoiceDate' => $errorSelectInvoiceDate, 
                'errorAddInvoiceRefNumber' => $errorAddInvoiceRefNumber, 
                'errorDigitInvoiceRefNumber' => $errorDigitInvoiceRefNumber,
                'errorNationalIDValid' => $errorNationalIDValid,
                'errorNationalIDReq' => $errorNationalIDReq,
                'errorNationalIDTenDigit' => $errorNationalIDTenDigit,
                'pamentScheduleDateMsg' => $pamentScheduleDateMsg,
                'nodDateMsg' => $nodDateMsg
            ) );

    }
    public function invoice_input_output_ui(){
        require_once plugin_dir_path( __FILE__ ) . '../template/nfsc-invoice-html.php';
    }
    public function payment_schedule_input_output_ui(){
        require_once plugin_dir_path( __FILE__ ) . '../template/nfsc-payment-schedule-html.php';
    }
    public function validateInvoice(){
        global $wpdb;
        
        $data = $_POST['invoiceData'];
        $language = $_POST['language'];
		parse_str($data, $invoiceData);
        $invoiceDate = $invoiceData['invoiceDateInput'];
        $invoiceRefNumber = $invoiceData['invoiceReferanceInput'];
        if($invoiceDate && $invoiceRefNumber){
            $format_date = str_replace('/', '-', $invoiceDate);
            $inputDate = strtotime($format_date);
            $month = date('F',$inputDate);
            $year = date('Y',$inputDate);
            $resMonth = Nfsc_invoice::get_month_for_arabic_english($month, $language);
            $invoiceDateformated = date('Y-m-d', strtotime($invoiceDate));
            $result = $wpdb->get_row( "SELECT * FROM `fS6_invoice` WHERE invoice_date = '".$invoiceDateformated."' AND invoice_referance_number = '".$invoiceRefNumber."'" );
            if($result) {
                $response = array('status'=> 'true','invoice_date' => $invoiceDate, 'invoice_ref_number' => $invoiceRefNumber, 'month' => $resMonth, 'year' => $year );
            }else{
                $response = array('status'=> 'false', 'msg' => 'Invoice not found');
            }

        }
        echo json_encode($response);
		die;
    }
    public function get_month_for_arabic_english($month, $language){
        $lblJan = ($language === 'ar') ? 'يناير' : 'January';
        $lblFeb = ($language === 'ar') ? 'فبراير' : 'February';
        $lblMarch = ($language === 'ar') ? 'مارس': 'March';
        $lblApr = ($language === 'ar') ? 'ابريل': 'April';
        $lblMay = ($language === 'ar') ? 'مايو': 'May';
        $lblJun = ($language === 'ar') ? 'يونيو': 'June';
        $lblJul = ($language === 'ar') ? 'يوليو': 'July';
        $lblAug = ($language === 'ar') ? 'أغسطس': 'August';
        $lblSep = ($language === 'ar') ? 'سبتمبر': 'September';
        $lblOct = ($language === 'ar') ? 'أكتوبر': 'October';
        $lblNov = ($language === 'ar') ? 'نوفمبر': 'November';
        $lblDec = ($language === 'ar') ? 'ديسمبر': 'December';
        if ($month == 'January'){
           return $lblJan;
        } elseif ($month == 'February') {
           return $lblFeb;
        } elseif ($month == 'March') {
            return $lblMarch;
        } elseif ($month == 'April') {
            return $lblApr;
        } elseif ($month == 'May') {
            return $lblMay;
        } elseif ($month == 'June') {
            return $lblJun;
        } elseif ($month == 'July') {
            return $lblJul;
        } elseif ($month == 'August') {
            return $lblAug;
        } elseif ($month == 'September') {
            return $lblSep;
        } elseif ($month == 'October') {
            return $lblOct;
        } elseif ($month == 'November') {
            return $lblNov;
        } else {
            return $lblDec;
        }
    }
    function valid_payment_schedule_details(){
        $data = $_POST['paymentData'];
		parse_str($data, $paymentData);
        $nationalIDInput = $paymentData['nationalIDInput'];
        $invoiceRefNumber = $paymentData['invoiceReferanceInput'];
        if($nationalIDInput && $invoiceRefNumber){
            $param = array (
				'NationalID' => $nationalIDInput,
                'DocumentNumber' => $invoiceRefNumber
			);
            $url = 'https://lmswebapi.clariontechnologies.co.in/api/PaymentSchedule';
			$data = $this->processGetHttpService($url, $param);
            if($data->downloadlink){
                $response = array('status' => 'true', 'link' => $data->downloadlink, 'DateTime' => $data->DateTime);
            }else{

                $response = array('status' => 'false', 'msg' => 'Payment schedule not found');
            }
            
        }
        echo json_encode($response);
		die;
    }

    public function nod_input_output_ui(){
        require_once plugin_dir_path( __FILE__ ) . '../template/nfsc-nod-html.php';
    }
    function nod_details(){
        $data = $_POST['nodData'];
		parse_str($data, $nodData);
        $nationalIDInput = $nodData['nationalIDInput'];
        $nodRefNumber = $nodData['nodInput'];
        if($nationalIDInput && $nodRefNumber){
            $param = array (
				'NationalID' => $nationalIDInput,
                'DocumentNumber' => $nodRefNumber
			);
            $url = 'https://lmswebapi.clariontechnologies.co.in/api/NoticeOfDelay';
			$data = $this->processGetHttpService($url, $param);
            if($data->downloadlink){
                $response = array('status' => 'true', 'link' => $data->downloadlink, 'DateTime' => $data->DateTime);
            }else{

                $response = array('status' => 'false', 'msg' => 'Notice of Delay not found');
            }
            
        }
        echo json_encode($response);
		die;
    }
    	/**
	 * This is HTTP GET Method of the WordPress.
	 *
	 * @since 1.0.1
	 *
	 * @return json
	 */
	public function processGetHttpService( $url, $param ){
		$param_string      = http_build_query($param);
		$final_url         = $url . '?' . $param_string;
		$getData           = wp_remote_get($final_url);
		try {
			return json_decode($getData['body']);
		} catch (Exception $e) {
			return $e->getMessage();
		}
	}
}