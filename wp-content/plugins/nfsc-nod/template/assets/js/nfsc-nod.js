/** Custom JS Scripts Here */
jQuery(document).ready(function() { 
    window.$ = jQuery;
    initializeGlobelCalls.init(); 
    
}); 
function onlyNumber (obj) {
    var $this = jQuery(obj);
        $this.val($this.val().replace(/\D/g, ''));
}
var initializeGlobelCalls = function () {

    var nod_general = function() {
        if($('#nodDateInput').length){
            $("#nodDateInput").flatpickr({
                dateFormat: "d-m-Y",
                maxDate: "today",
            });
        }
        
        $('.backnodInput').click(function(){
            if( $("#nodForm").length){
                $("#nodForm")[0].reset();
            }else{
                $("#nod-details-form")[0].reset();
            }
            $('.nod-details-container').hide();
            $('.nod_input_container').show();
        });
    }

    var nod_details_download = function(){
        if($('#nod-details-form').length){
            $('#nod-details-form').bootstrapValidator({
                message: 'This value is not valid',
                fields: {
                    nationalIDInput: {
                        validators: {
                            callback: {
                                message: my_ajax_object.errorNationalIDValid,
                                callback: function (value, validator, $field) {
                                    if (value.length == '') {
                                        return {
                                            valid: false,
                                            message: my_ajax_object.errorNationalIDReq
                                        };
                                    }
                                    if (value.length < 10) {
                                        return {
                                            valid: false,
                                            message: my_ajax_object.errorNationalIDTenDigit
                                        };
                                    }
                                    if(value.length == 10){
                                        var isValid = ValidateNICNumber($('#nationalIDInput').val());
                                        if(isValid == false){
                                            return {
                                                valid: false,
                                                message: my_ajax_object.errorNationalIDValid
                                            };
                                        }
                                        
                                    }
                                    return true;
                                }
                            }
                        }
                    },
                    nodInput : {
                        validators: {
                            notEmpty: {
                                message: my_ajax_object.errorAddnodRefNumber
                            }
                        }
                    }

                    
                }
            }).on('success.form.bv', function (e) {
                e.preventDefault();
                var btn = $('.btn-submit').text();
                // $('.btn-submit').html('<span class="spinner-border spinner-border-sm  m-1" role="status" aria-hidden="true"></span>');
                var formdata = $('#nod-details-form').serialize();
                $.ajax({
                    url: my_ajax_object.ajax_url,
                    data: {
                        action: 'nod_details',
                        paymentData: formdata,
                    },
                    type: "POST",
                    success: function(response) {
                        $('.btn-submit').text(btn)
                        var data = jQuery.parseJSON(response);
                        if(data.status == "false") {
                            $('.error-nod').show();
                            setTimeout(function(){ $('.error-nod').hide(); }, 4000);
                        }else{
                            $('.nod_input_container').hide();
                            $('.nod-details-container').show();
                            $('.download-link').attr('href', data.link);
                            $('.nodDate').html(my_ajax_object.pamentScheduleDateMsg.replace("{date}", data.DateTime));
                             
                        }
                        
                    }
                });
            });
        }
    }

    var ValidateNICNumber = function (number) {
        if (number.length != 10) {
            return false;
        } else {
            var nSum = 0;
            var lastDigit = "";
            var nDigit = 0;
            for (i = 0; i < 9; i++) {
                nDigit = parseInt(number[i]);
                if (i % 2 == 0) // If Odd position digit double it
                {
                    if ((nDigit * 2) > 9) // If two digits number then add each digit
                        nSum = parseInt(nSum) + parseInt((nDigit * 2).toString().substring(0, 1)) + parseInt((nDigit * 2).toString().substring(1, 2));
                    else
                        nSum = parseInt(nSum) + (parseInt(nDigit) * 2);
                } else
                    nSum += nDigit;
            }
            var lastDigit = (nSum % 10).toString();
            if (lastDigit != "0") {
                lastDigit = (10 - (lastDigit)).toString();
            }
    
            if (lastDigit == number.substring(10, 9)){
                return true;
            } else {
                return false;
            }
        }
    }

    return {
        init: function () {
            nod_general();
            nod_details_download();
        }
    }
}();