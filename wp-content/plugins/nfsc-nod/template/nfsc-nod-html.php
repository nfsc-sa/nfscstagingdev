<?php
$plugin_aseets_path = plugin_dir_url( dirname( __FILE__ ) ) ;
require_once plugin_dir_path( __FILE__ ) .'lang/lang.php';
?>
<link rel="stylesheet" href="<?php echo $plugin_aseets_path.'template/assets/css/bootstrap-datepicker.min.css';?>">
<link rel="stylesheet" href="<?php echo $plugin_aseets_path.'template/assets/css/bootstrapValidator.min.css';?>">
<link rel="stylesheet" href="<?php echo $plugin_aseets_path.'template/assets/css/nfsc-nod.css';?>">
<?php if(pll_current_language() === 'ar'){ ?>
    <link rel="stylesheet" href="https://cdn.rtlcss.com/bootstrap/v4.5.3/css/bootstrap.min.css">
<?php }else{ ?>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css">
<?php  } ?>


<div class="container">
    <div class="row justify-content-center nod_input_container">
        <div class="col-md-4 mt-6 shadow-lg p-4 border-radius" style="background-color: #fff;">
            <form class="mx-auto" id="nod-details-form">
                <div class="form-group">
                    <label for="nationalIDInput"><?php echo $lblNationalID; ?></label>
                    <input type="text" class="form-control" id="nationalIDInput" name="nationalIDInput" maxlength="10" onkeyup="onlyNumber(this)" aria-describedby="nationalid">
                </div>
                <div class="form-group">
                    <label for="nodInput"><?php echo $lblRefernceNumber; ?></label>
                    <input type="text" class="form-control" id="nodInput" name="nodInput" maxlength="10">
                </div>
                <div class="alert alert-warning error-nod" role="alert" style="display:none;">
                   <?php echo $errorNoDNotFound; ?>
                </div>
                <button type="submit" class="btn btn-new btn-submit w-100 mt-3"><?php echo $nodSubmit; ?></button>
                
            </form>
        </div>
    </div>
    <div class="nod-details-container" style="display:none;">
        <div class="row justify-content-center">
            <div class="col-md-8 mt-6">
                <div class="row">
                       <div class="col-md-12 text-center mb-5">
                           <div class="nod-section padding-nod">
                                <h4 class="nodDate pt-2"></h4>
                                <div class="p-3">
                                    <a class="btn btn-new download-link" target="_blank" href="#"><?php echo $lblDownloadPDF; ?></a>
                                <button type="button" class="btn btn-gray ml-3 backnodInput"><?php echo $lblBack; ?></button>
                            </div>
                          </div>  
                          
                      </div>
                </div>
            </div>
        </div>
        <div class="row justify-content-center">
            <div>
                
            </div>
        </div>
    </div>     
</div>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flatpickr/4.2.3/flatpickr.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/flatpickr/4.2.3/flatpickr.js">
<script src="<?php echo $plugin_aseets_path.'template/assets/js/bootstrap-datepicker.min.js';?>" ></script>
<script type="text/javascript" src="<?php echo $plugin_aseets_path.'/template/assets/js/bootstrapValidator.min.js'; ?>"></script>