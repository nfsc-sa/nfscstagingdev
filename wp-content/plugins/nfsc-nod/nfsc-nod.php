<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://profiles.wordpress.org/sachinnawale/
 * @since             1.0.0
 * @package           Nfsc_nod
 *
 * @wordpress-plugin
 * Plugin Name:       NFSC NoD
 * Plugin URI:        https://wordpress.org/plugins/nfsc-nod
 * Description:       This plugin enables the front -end dashboard functionality as well as the other custom functions
 * Version:           1.0.0
 * Author:            Sachin Nawale
 * Author URI:        https://profiles.wordpress.org/sachinnawale/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       nfsc-clarion
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'NFSC_NOD_VERSION', '1.0.0' );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-nfsc-clarion-activator.php
 */
function activate_nfsc_nod() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-nfsc-nod-activator.php';
	Nfsc_nod_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-nfsc-clarion-deactivator.php
 */
function deactivate_nfsc_nod() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-nfsc-nod-deactivator.php';
	Nfsc_nod_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_nfsc_nod' );
register_deactivation_hook( __FILE__, 'deactivate_nfsc_nod' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-nfsc-nod.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_Nfsc_nod() {

	$plugin = new Nfsc_nod();

}
run_Nfsc_nod();
