<?php

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       https://profiles.wordpress.org/sachinnawale/
 * @since      1.0.0
 *
 * @package    Nfsc_NoD
 * @subpackage Nfsc_NoD/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    Nfsc_NoD
 * @subpackage Nfsc_NoD/includes
 * @author     Sachin Nawale <sachin.nawale@clariontech.com>
 */
class Nfsc_NoD {

    public function __construct() {

        add_shortcode('nod_shortcode', array($this, 'payment_schedule_input_output_ui'));
        add_action( 'wp_enqueue_scripts', array($this, 'nod_enqueue') );
        add_action("wp_ajax_nod_details", array($this,"nod_details"));
        add_action("wp_ajax_nopriv_nod_details", array($this,"nod_details"));
    }
    public function nod_enqueue() {
        $language = pll_current_language();
        $errorSelectnodDate = ($language === 'ar') ? 'الرجاء اختيار تاريخ الفاتورة' : 'Please select Date';
        $errorAddnodRefNumber = ($language === 'ar') ? 'الرجاء إدخال الرقم المرجعي للفاتورة' : 'Please add referance number';
        $errorDigitnodRefNumber = ($language === 'ar') ? 'الرجاء إدخال الرقم المرجعي للفاتورة' : 'Please enter 6 digit nod reference number';
        $errorNationalIDValid = ($language === 'ar') ? 'الرجاء إدخال رقم هوية وطنية صحيح' : 'Please enter valid NationalId';
        $errorNationalIDReq = ($language === 'ar') ? 'الرجاء إدخال رقم الهوية الوطنية' : 'National ID is Required';
        $errorNationalIDTenDigit = ($language === 'ar') ? 'يجب أن يتكون رقم الهوية من 10 أرقام على الأقل' : 'National Id must be 10 digits long';
        $pamentScheduleDateMsg = ($language === 'ar') ? 'تم إنشاء إشعار التأخير في {date}' : 'Notice of Delay Generated on {date}';
        wp_enqueue_script( 'ajax-scripts', plugin_dir_url( dirname( __FILE__ ) ) . 'template/assets/js/nfsc-nod.js', array(), false, true );
        wp_localize_script( 'ajax-scripts', 'my_ajax_object', 
                array( 'ajax_url' => admin_url( 'admin-ajax.php' ),
                'errorSelectnodDate' => $errorSelectnodDate, 
                'errorAddnodRefNumber' => $errorAddnodRefNumber, 
                'errorDigitnodRefNumber' => $errorDigitnodRefNumber,
                'errorNationalIDValid' => $errorNationalIDValid,
                'errorNationalIDReq' => $errorNationalIDReq,
                'errorNationalIDTenDigit' => $errorNationalIDTenDigit,
                'pamentScheduleDateMsg' => $pamentScheduleDateMsg
            ) );

    }
    public function payment_schedule_input_output_ui(){
        require_once plugin_dir_path( __FILE__ ) . '../template/nfsc-nod-html.php';
    }
    function nod_details(){
        $data = $_POST['paymentData'];
		parse_str($data, $paymentData);
        $nationalIDInput = $paymentData['nationalIDInput'];
        $nodRefNumber = $paymentData['nodInput'];
        if($nationalIDInput && $nodRefNumber){
            $param = array (
				'NationalID' => $nationalIDInput,
                'DocumentNumber' => $nodRefNumber
			);
            $url = 'https://lmswebapi.clariontechnologies.co.in/api/NoticeOfDelay';
			$data = $this->processGetHttpService($url, $param);
            if($data->downloadlink){
                $response = array('status' => 'true', 'link' => $data->downloadlink, 'DateTime' => $data->DateTime);
            }else{

                $response = array('status' => 'false', 'msg' => 'Payment schedule not found');
            }
            
        }
        echo json_encode($response);
		die;
    }

    	/**
	 * This is HTTP GET Method of the WordPress.
	 *
	 * @since 1.0.1
	 *
	 * @return json
	 */
	public function processGetHttpService( $url, $param ){
		$param_string      = http_build_query($param);
		$final_url         = $url . '?' . $param_string;
		$getData           = wp_remote_get($final_url);
		try {
			return json_decode($getData['body']);
		} catch (Exception $e) {
			return $e->getMessage();
		}
	}
}