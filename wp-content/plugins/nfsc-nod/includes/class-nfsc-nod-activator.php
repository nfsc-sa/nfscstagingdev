<?php

/**
 * Fired during plugin activation
 *
 * @link       https://profiles.wordpress.org/sachinnawale/
 * @since      1.0.0
 *
 * @package    Nfsc_nod
 * @subpackage Nfsc_nod/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Nfsc_nod
 * @subpackage Nfsc_nod/includes
 * @author     Sachin Nawale <sachin.nawale@clariontech.com>
 */
class Nfsc_nod_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
