<?php

/**
 * Provide a public-facing view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       https://profiles.wordpress.org/yogeshdalavi/
 * @since      1.0.0
 *
 * @package    Nfsc_Clarion
 * @subpackage Nfsc_Clarion/public/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
