<?php
$api_url = (get_option('api_setting')) ? get_option('api_setting') : 'https://lmswebapi.clariontechnologies.co.in/api/' ;
define('API_BASE_URL', $api_url);
define('API_USER_AUTH', 'UserAuth');
define('API_VALIDATE_OTP', 'ValidateOTP');
define('API_BASIC_INFORMATION', 'BasicInformation');
define('API_CONTRACTS_BASIC_INFORMATION', 'BasicInformation/GetContractsInfoFromNID');
define('API_TICKET_LIST', 'TicketList');
define('API_TICKET_CATEGORY', 'TicketSelectionDetails');
define('API_TICKET_CREATE', 'CreateNewTicket');
define('API_PAYMENT_SCHEDULE', 'PaymentSchedules');
define('API_PAYMENT_DETAILS', 'PaymentDetails');
define('API_SMS_PAYMENT_SCHEDULE', 'PaymentDetails/SendPaymentScheduleSMS');

define('API_KYC_USER_AUTH', 'CheckValidUser');
define('API_KYC_VALIDATE_ACCESS_OTP', 'ValidateAccessOTP');
define('API_KYC_GET_DETAILS', 'GetKYCDetails');
define('API_KYC_POST_DETAILS', 'PostKYCDetails');
define('API_KYC_GET_DROPDOWN_DATA', 'GetDropdowndata');

define('API_USER_LOGOUT', 'UserAuth/Logout');

define('API_USER_ACTIVITY_LOG', 'UserAuth/TraceActivity');

define('API_REFRESH_TOKEN', 'ValidateOTP/RefreshToken');

define('NATIONAL_ID', 'nationalID');
define('SUCCESS', 'success');
define('ERROR', 'error');


