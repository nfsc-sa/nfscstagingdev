<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       https://profiles.wordpress.org/yogeshdalavi/
 * @since      1.0.0
 *
 * @package    Nfsc_Clarion
 * @subpackage Nfsc_Clarion/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Nfsc_Clarion
 * @subpackage Nfsc_Clarion/admin
 * @author     Yogesh Dalavi <yogesh.dalavi@clariontechnologies.co.in>
 */
class Nfsc_Clarion_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */

	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;
		$this->resource_table = 'f6S_lang_resource';
		// user_logs
		$this->user_logs_table = 'f6s_user_logs';
		
		/**
		 * The file responsible for the all website constants including API and varibales
		 * core plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/constant/constant.php';
		

	}
	public function is_polylang_configure()
        {
            if (!function_exists('pll_current_language')) {
                wp_die('Please configure languages before moving to site');
                exit();
            }
        }

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Nfsc_Clarion_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Nfsc_Clarion_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/nfsc-clarion-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Nfsc_Clarion_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Nfsc_Clarion_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/nfsc-clarion-admin.js', array( 'jquery' ), $this->version, false );

	}
	/**
	 * create menu to customize the dashboard setting.
	 *
	 * @since    1.0.0
	 */
	public function dashboard_general_setting() {
		add_menu_page(
			'Front-end Dashboard Setting', // page <title>Title</title>
			'Dashboard Settings', // menu link text
			'manage_options', // capability to access the page
			'dashboard-setting', // page URL slug
			array($this,'dashboard_page_content'), // callback function /w content
			'dashicons-admin-generic', // menu icon
			4 // priority
		);
		add_submenu_page( 'dashboard-setting', 'Resources', 'Resources','manage_options', 'resources', array($this,'resources_sub_content'),);
	}
	
	public function dashboard_page_content(){
		echo '<div class="wrap">
			<h1>NFSC Dashboard setting</h1>
			<form method="post" action="options.php">';
				settings_fields( 'general_option' ); // settings group name
				do_settings_sections( 'dashboard-setting' ); // just a page slug
				submit_button();

			echo '</form></div>';
	}
	/**
	 * create field to customize the dashboard setting.
	 *
	 * @since    1.0.0
	 */
	public function general_setting () {
		add_settings_section(  
			'maintenance_settings_section', // Section ID 
			'Disable dashboard', // Section Title
			array($this,'general_setting_callback'), // Callback
			'dashboard-setting' // What Page?  This makes the section show up on the General Settings Page
		);
	
		add_settings_field( // Option 1
			'disable_setting', // Option ID
			'Disable Dashboard', // Label
			array($this,'disable_textbox_callback'), // !important - This is where the args go!
			'dashboard-setting', // Page it will be displayed (General Settings)
			'maintenance_settings_section', // Name of our section
			array( // The $args
				'disable_setting' // Should match Option ID
			)  
		); 
        
		add_settings_section(  
			'api_settings_section', // Section ID 
			'API Setting', // Section Title
			'', // Callback
			'dashboard-setting' // What Page?  This makes the section show up on the General Settings Page
		);
	
		add_settings_field( // Option 1
			'api_setting', // Option ID
			'API Base URL', // Label
			array($this,'api_textbox_callback'), // !important - This is where the args go!
			'dashboard-setting', // Page it will be displayed (General Settings)
			'api_settings_section', // Name of our section
			array( // The $args
				'api_setting' // Should match Option ID
			)  
		); 

		add_settings_section(  
			'dashboard_settings_section', // Section ID 
			'Session Setting', // Section Title
			'', // Callback
			'dashboard-setting' // What Page?  This makes the section show up on the General Settings Page
		);
	
		add_settings_field( // Option 1
			'session_warning', // Option ID
			'Session Warning Poup after Time', // Label
			array($this,'dashboard_textbox_callback'), // !important - This is where the args go!
			'dashboard-setting', // Page it will be displayed (General Settings)
			'dashboard_settings_section', // Name of our section
			array( // The $args
				'session_warning' // Should match Option ID
			)  
		); 
		add_settings_field( // Option 1
			'session_redirect', // Option ID
			'Session Redirect Time', // Label
			array($this,'dashboard_textbox_callback'), // !important - This is where the args go!
			'dashboard-setting', // Page it will be displayed (General Settings)
			'dashboard_settings_section', // Name of our section
			array( // The $args
				'session_redirect' // Should match Option ID
			)  
		); 
		add_settings_section(  
			'ticket_settings_section', // Section ID 
			'Ticket Setting', // Section Title
			'', // Callback
			'dashboard-setting' // What Page?  This makes the section show up on the General Settings Page
		);
		add_settings_field( // Option 1
			'ticket_description', // Option ID
			'Ticket Description character length', // Label
			array($this,'ticket_length_textbox_callback'), // !important - This is where the args go!
			'dashboard-setting', // Page it will be displayed (General Settings)
			'ticket_settings_section', // Name of our section
			array( // The $args
				'ticket_description' // Should match Option ID
			)  
		); 
		register_setting('general_option','ticket_description', 'esc_attr');
		register_setting('general_option','disable_setting', 'esc_attr');
		register_setting('general_option','api_setting', 'esc_attr');
		register_setting('general_option','session_warning', 'esc_attr');
		register_setting('general_option','session_redirect', 'esc_attr');
		
		
	}

	public function general_setting_callback() { // Section Callback
		echo '<p>All the Front-end NFSC Dashboard setting</p>';  
	}
	
	function disable_textbox_callback($args) {  // Textbox Callback
		$option = get_option($args[0]);
		$checked = ($option == 'on') ? 'checked' : '';
		echo '<label class="switch-disable-dashboard"><input type="checkbox" id="'. $args[0] .'" name="'. $args[0] .'" '.$checked.'><span class="slider round"></span></label>';
	}

	function dashboard_textbox_callback($args) {  // Textbox Callback
		$option = get_option($args[0]);
		echo '<input type="number"  min="1" id="'. $args[0] .'" name="'. $args[0] .'" value="' . $option . '" /> (Minute)';
	}
	function ticket_length_textbox_callback($args) {  // Textbox Callback
		$option = get_option($args[0]);
		echo '<input type="number"  min="20" id="'. $args[0] .'" name="'. $args[0] .'" value="' . $option . '" /> (Max)';
	}
	function api_textbox_callback($args) {  // Textbox Callback
		$option = get_option($args[0]);
		echo '<input type="url" id="'. $args[0] .'" name="'. $args[0] .'" value="' . $option . '" />';
	}
	
	/**
	 * Active/Disable online Service.
	 *
	 * @since    1.0.0
	 */
	function maintenace_mode(){
		$maintenace = get_option( 'disable_setting' );
		if ( !is_user_logged_in() && ($maintenace == 'on')) {
			session_destroy();
			require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/template/online-service-unavailable.php';
			exit();
		}
	}
	/**
	 * Action to hide the Online service on maintenace active.
	 *
	 * @since    1.0.0
	 */
	public function hide_online_service_menu() { 
		$maintenace = get_option( 'disable_setting' );
		if ( !is_user_logged_in() && ($maintenace == 'on')) { ?>
			<style>
				.menu-online-service{
					display: none !important;
				}
			</style>
		<?php }
	}
	
	/**
	 * Resource Management
	 *
	 * @since    1.0.0
	 */
	public function resources_sub_content(){
		/**
		 * The file responsible for the all website constants including API and varibales
		 * core plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/partials/nfsc-clarion-resource-display.php';
	}
	/**
	 * validate the National ID and Originsation Date
	 *
	 * @since    1.0.0
	 */

	public function validateNationalID(){

		$data = $_POST['NICdata'];
		parse_str($data, $formdata);
		$nationalID = (int) filter_var($formdata['nationalID'], FILTER_SANITIZE_NUMBER_INT);
		$param = array (
			'NationalID' => $nationalID,
			'ChannelType' => 'Web'
		);

		$url = API_BASE_URL . API_USER_AUTH;
		$data = $this->processPostHttpService($url, $param);
		if($data->StatusCode){
			if($data->StatusCode != 200){
				$response = array('status' => "false", 'msg' => $this->get_resource('errorNationalIDValid'));
				$logData = array('status' => 'Invalid Data', 'National ID' => substr_replace($nationalID,'XXXXXX', 0, 6));
			}else {
				$response = array('status' => "true", 'account_number' => $data->AccountNumber, 'nationalID' => $nationalID, 'originationDate' => $originationDate);
				$logData = array('status' => 'Validated National ID successfully', 'Account Number' => substr_replace($data->AccountNumber,'XXXXXXXX', 0, 8), 'National ID' => substr_replace($nationalID,'XXXXXX', 0, 6));
			}
			
		} else {
			$response = array('status' => "false", 'msg' => $this->get_resource('errorServerAPI'));
			$logData = array('status' => 'API server Error', 'National ID' => substr_replace($nationalID,'XXXXXX', 0, 6));

		}
		
		$this->nfsc_log($logData);
		echo json_encode($response);
		die;
	}

	public function validateKYCNationalID(){

		$data = $_POST['NICdata'];
		parse_str($data, $formdata);
		$nationalID = (int) filter_var($formdata['nationalID'], FILTER_SANITIZE_NUMBER_INT);
		$MobileNumber = (int) filter_var($formdata['MobileNumber'], FILTER_SANITIZE_NUMBER_INT);
		
			$param = array (
				'NationalID' => $nationalID,
				'MobileNumber' => $MobileNumber
				// 'ChannelType' => 'Web'
			);

		$url = API_BASE_URL . API_KYC_USER_AUTH;
		$data = $this->processKYCGetHttpService($url, $param);
		if($data->status){
			if($data->status == 'error'){
				// $response = array('status' => "false", 'msg' => $this->get_resource('errorKYCNationalIDValid'));
				$response = array('status' => "false", 'msg' => 'Please Enter Valid National ID Or Mobile Number');
				$logData = array('status' => 'Invalid Data', 'National ID' => substr_replace($nationalID,'XXXXXX', 0, 6));
			}else {
				$response = array('status' => "true", 'account_number' => $data->AccountNumber, 'nationalID' => $nationalID, 'MobileNumber' => $MobileNumber);
				$logData = array('status' => 'Validated National ID successfully', 'Account Number' => substr_replace($data->AccountNumber,'XXXXXXXX', 0, 8), 'National ID' => substr_replace($nationalID,'XXXXXX', 0, 6));
			}
			
		} else {
			$response = array('status' => "false", 'msg' => $this->get_resource('errorServerAPI'));
			$logData = array('status' => 'API server Error', 'National ID' => substr_replace($nationalID,'XXXXXX', 0, 6));

		}
		
		$this->nfsc_log($logData);
		echo json_encode($response);
		die;
	}
	/**
	 * validate the OTP
	 *
	 * @since    1.0.0
	 */

	public function verifyOTP(){
		session_start();
		$data = $_POST['OTPdata'];
		parse_str($data, $formdata);
		$otp = implode("",$formdata['OTP']);
		$account_number = (int) filter_var($formdata['acc_no'], FILTER_SANITIZE_NUMBER_INT);
		$hidenationalId = (int) filter_var($formdata['hidenationalId'], FILTER_SANITIZE_NUMBER_INT);
		$param = array (
			'otp' => $otp,
			'AccountNumber' => $account_number,
			'ChannelType' => 'Web'
		);	
		$url = API_BASE_URL . API_VALIDATE_OTP;
		$data = $this->processPostHttpService($url, $param);
		$responses = $data->response;	
		$this->nfsc_log($data);
		if($data){	
			if($data->StatusCode == '200'){
				$_SESSION['AccountNumber'] = $responses->MortgageLoanAccountNumber; //$data->AccountNumber add one OTP start
				$_SESSION['NationalId'] = $hidenationalId;
				$_SESSION['LogId'] = $responses->LogID;
				$_SESSION['Token'] = $responses->Token;
				$response = array('status' => "true",'logid' => $responses->LogID, 'token' => $responses->Token, 'account_number' => $responses->MortgageLoanAccountNumber); //$data->AccountNumber  add one OTP start
				$logData = array('status' => 'OTP Validated','logid' =>$responses->LogID, 'token' => $responses->Token, 'Account Number' => substr_replace($responses->MortgageLoanAccountNumber,'XXXXXXXX', 0, 8));
			}				
			elseif($data->StatusCode == '408' ){
				$response = array('status' => "false", 'msg' => $this->get_resource('errorOTPExpire'));
				$logData = array('status' => 'OTP Error', 'msg' => $this->get_resource('errorOTPExpire'));	
			}elseif ($data->StatusCode == '400') {
				$response = array('status' => "false", 'msg' => $this->get_resource('errorOTPValid'));
				$logData = array('status' => 'OTP Error', 'msg' => $this->get_resource('errorOTPValid'));	
			}else{
				$response = array('status' => "false", 'msg' => $this->get_resource('errorServerAPI'));
				$logData = array('status' => 'OTP Error', 'msg' => $this->get_resource('errorServerAPI'));	
			}		

		}else{
			$response = array('status' => "false", 'logid' =>$responses->LogId, 'msg' => $this->get_resource('errorServerAPI'));
			$logData = array('status' => 'API server error','logid' =>$responses->LogId, 'Account Number' => substr_replace($account_number,'XXXXXXXX', 0, 8));
		}
		$this->nfsc_log($logData);
		echo json_encode($response);
		die;		
	}

	/**
	 * validate the KYC OTP
	 *
	 * @since    1.0.0
	 */

	public function KYCverifyOTP(){
		session_start();
		$data = $_POST['KYCOTPdata'];
		parse_str($data, $formdata);
		$otp = implode("",$formdata['KYCOTP']);
		// $account_number = (int) filter_var($formdata['acc_no'], FILTER_SANITIZE_NUMBER_INT);
		// $hidenationalId = (int) filter_var($formdata['hidenationalId'], FILTER_SANITIZE_NUMBER_INT);
		$nationalID = (int) filter_var($formdata['hidenationalId'], FILTER_SANITIZE_NUMBER_INT);
		$param = array (
			'otp' => $otp,
			'NationalID' => $nationalID,
			'ChannelType' => 'Web'
		);
		
		$url = API_BASE_URL . API_KYC_VALIDATE_ACCESS_OTP;
		
		$data = $this->processKYCGetHttpService($url, $param);
		if($data){

			if($data->status == 'success'){
				// $_SESSION['AccountNumber'] = $data->AccountNumber; //$data->AccountNumber add one OTP start
				$_SESSION['NationalId'] = $nationalID;
				$response = array('status' => "true", 'national_id' => $data->NationalID); //$data->AccountNumber  add one OTP start
				$logData = array('status' => 'OTP Validated', 'National ID' => substr_replace($data->NationalID,'XXXXXX', 0, 6));
			
			}				
			elseif($data->status == 'expired' ){
					$response = array('status' => "false", 'msg' => $this->get_resource('errorOTPExpire'));
				}
			elseif ($data->status == 'error') {
					$response = array('status' => "false", 'msg' => $this->get_resource('errorOTPValid'));
				}
			else{
					$response = array('status' => "false", 'msg' => $this->get_resource('errorServerAPI'));
				}
				$logData = array('status' => 'OTP Error', 'National ID' => substr_replace($nationalID,'XXXXXX', 0, 6));
				
		}else{
			$response = array('status' => "false", 'msg' => $this->get_resource('errorServerAPI'));
			$logData = array('status' => 'API server error', 'National ID' => substr_replace($nationalID,'XXXXXXXX', 0, 8));
		}
		$this->nfsc_log($logData);
		echo json_encode($response);
		die;
		
	}

	/**
	 * resend the OTP
	 *
	 * @since    1.0.0
	 */
	public function resendOTP(){

		$nationalID = (int) filter_var($_POST['NationalID'], FILTER_SANITIZE_NUMBER_INT);
				$param = array (
				'NationalID' => $nationalID,
				'ChannelType' => 'Web'
			);
			
			$url = API_BASE_URL . API_USER_AUTH;
			$data = $this->processPostHttpService($url, $param);
			
			if($data->StatusCode) {
				
				if($data->StatusCode != 200){
					$response = array('status' => "false", 'msg' => $this->get_resource('errorServerAPI'));
					$logData = array('status' => 'API server error', 'National ID' => substr_replace($nationalID,'XXXXXX', 0, 6));
				} else {
					$response = array('status' => "true");
					$logData = array('status' => 'Send OTP Successfully', 'National ID'  => substr_replace($nationalID,'XXXXXX', 0, 6));
				}
			}else{
				$response = array('status' => "false", 'msg' => $this->get_resource('errorServerAPI'));
				$logData = array('status' => 'API server error', 'National ID'  => substr_replace($nationalID,'XXXXXX', 0, 6));
			}
		$this->nfsc_log($logData);
		echo json_encode($response);
		die;
	}
	
	/**
	 * resend the KYCOTP
	 *
	 * @since    1.0.0
	 */
	public function resendKYCOTP(){
		$nationalID = (int) filter_var($_POST['NationalID'], FILTER_SANITIZE_NUMBER_INT);
		$MobileNumber = (int) filter_var($_POST['MobileNumber'], FILTER_SANITIZE_NUMBER_INT);
		
			$param = array (
				'NationalID' => $nationalID,
				'MobileNumber' => $MobileNumber
				// 'ChannelType' => 'Web'
			);

		$url = API_BASE_URL . API_KYC_USER_AUTH;
		$data = $this->processKYCGetHttpService($url, $param);
		if($data->status){
			if($data->status != 'success'){
				$response = array('status' => "false", 'msg' => $this->get_resource('errorNationalIDValid'));
				$logData = array('status' => 'Invalid Data', 'National ID' => substr_replace($nationalID,'XXXXXX', 0, 6));
			}else {
				$response = array('status' => "true", 'account_number' => $data->AccountNumber, 'nationalID' => $nationalID, 'MobileNumber' => $MobileNumber);
				$logData = array('status' => 'Validated National ID successfully', 'Account Number' => substr_replace($data->AccountNumber,'XXXXXXXX', 0, 8), 'National ID' => substr_replace($nationalID,'XXXXXX', 0, 6));
			}
			
		} else {
			$response = array('status' => "false", 'msg' => $this->get_resource('errorServerAPI'));
			$logData = array('status' => 'API server Error', 'National ID' => substr_replace($nationalID,'XXXXXX', 0, 6));

		}
		
		$this->nfsc_log($logData);
		echo json_encode($response);
		die;
	}
	/**
	 * get the user account information
	 *
	 * @since    1.0.0
	 */
	public function user_account_info ( $account_number ) {
		if($account_number){
			$param = array (
				'AccountNumber' => $account_number
			);
			$url = API_BASE_URL . API_BASIC_INFORMATION;
			return $this->processGetHttpService($url, $param);
		}
	}
	/**
	 * get the Contract account information
	 *
	 * @since    1.0.0
	 */
	public function contracts_account_info ( $nationalId ) {
		if($nationalId){
			$param = array (
				'NationalID' => $nationalId
			);
			$url = API_BASE_URL . API_CONTRACTS_BASIC_INFORMATION;
			return $this->processGetHttpService($url, $param);
		}
	}
	/**
	 * get the KYC user account information
	 *
	 * @since    1.0.0
	 */
	public function kyc_user_account_info ( $NationalID ) {
		if($NationalID){
			$param = array (
				'NationalID' => $NationalID
			);
			$url = API_BASE_URL . API_KYC_GET_DETAILS;
			return $this->processKYCGetHttpService($url, $param);
		}
	}
	
	/**
	 * POST the KYC user account information
	 *
	 * @since    1.0.0
	 */
	public function kyc_post_user_account_info() {
		session_start();
		$data = $_POST['KYCPostdata'];
		$accNo = $_POST['accNumber'];
		parse_str($data, $formdata);
		$nationalID = (int) filter_var($formdata['NationalID'], FILTER_SANITIZE_NUMBER_INT);
		$MobileNumber = (int) filter_var($formdata['MobileNumber'], FILTER_SANITIZE_NUMBER_INT);
        $Email =  filter_var($formdata['lblEmail'], FILTER_SANITIZE_EMAIL);
		$BuildingNo = filter_var($formdata['lblBuildingNo'], FILTER_SANITIZE_STRING);
		$StreetName = filter_var($formdata['lblStreetName'], FILTER_SANITIZE_STRING) ;
		$DistrictName = filter_var($formdata['lbleDistrictName'], FILTER_SANITIZE_STRING);
		$CityID =  filter_var($formdata['lblCityId'],FILTER_SANITIZE_NUMBER_INT);
		$ZipCode =  filter_var($formdata['lblZipCode'], FILTER_SANITIZE_STRING);
		$AdditionalNumber =  filter_var($formdata['lblAdditionalNumber'], FILTER_SANITIZE_NUMBER_INT);
		$UnitNo =  filter_var($formdata['lblUnitNo'], FILTER_SANITIZE_NUMBER_INT);
		$NIDExpiryDate =  filter_var($formdata['lblNIDExDate'], FILTER_SANITIZE_STRING);
		$Occupation =  filter_var($formdata['lblOccupation'], FILTER_SANITIZE_NUMBER_INT);
		$Position = filter_var($formdata['lblPosition'], FILTER_SANITIZE_NUMBER_INT);
		$MonthlyIncome = filter_var($formdata['lblMonthlyIncome'], FILTER_SANITIZE_STRING);
		$MonthlyIncome = intval(preg_replace('/[^\d.]/', '', $MonthlyIncome));
		$NetWorth = filter_var($formdata['lblNetWorth'], FILTER_SANITIZE_STRING);
		$NetWorth = intval(preg_replace('/[^\d.]/', '', $NetWorth));
		$FundSource = filter_var($formdata['lblFundSource'], FILTER_SANITIZE_NUMBER_INT);
		$IsPEPSYes = filter_var($formdata['pepsyes'], FILTER_SANITIZE_STRING);
		$IsPEPSNo = filter_var($formdata['pepsno'], FILTER_SANITIZE_STRING);
		if($IsPEPSYes == "Yes"){
			$ISPEPSS = "yes";
		}
		if($IsPEPSNo == "No"){
			$ISPEPSS = "no";
		}
		if($accNo != ""){
			$FundSource = 1;
			$NetWorth = 1;
			$DistrictName = 1;
			$UnitNo = 1;
			$Position = 1;
			$ISPEPSS = 'yes';
		}
		$param = array(
			'NationalID' => $nationalID,
			'MobileNumber' => $MobileNumber,
			'Email' => $Email,
			"BuildingNo"=> $BuildingNo,
			"StreetName"=> $StreetName,
			"DistrictName"=> $DistrictName,
			"CityID"=> $CityID,
			"ZipCode"=> $ZipCode,
			"AdditionalNumber"=> $AdditionalNumber,
			"UnitNo"=> $UnitNo,
			"NIDExpiryDate"=>$NIDExpiryDate,
			"Occupation"=>$Occupation,
			"Position"=> $Position,
			"MonthlyIncome"=>$MonthlyIncome,
			"NetWorth"=> $NetWorth,
			"FundSource"=> $FundSource,
			'IsPEPS'=> $ISPEPSS
			
		);
			$url = API_BASE_URL . API_KYC_POST_DETAILS;
			$data = $this->processKycPostHttpService($url, $param);
			$this->nfsc_log($param);
			$this->nfsc_log($data);
			if($data) {
			if($data){
				$response = array('body' => "true");
				$logData = array('body' => 'Updated Data Successfully', 'National ID'  => substr_replace($nationalID,'XXXXXX', 0, 6));
			} else {
				$response = array('body' => "false", 'msg' => "Status showing an error");
				$logData = array('body' => 'API server error', 'National ID' => substr_replace($nationalID,'XXXXXX', 0, 6));
			}
		}else{
			$response = array('body' => "false", 'msg' => $this->get_resource('errorServerAPI'));
			$logData = array('body' => 'API server error', 'National ID'  => substr_replace($nationalID,'XXXXXX', 0, 6));
		}
		$this->nfsc_log($logData);
		echo json_encode($response);
		die;
	}
	/**
	 * get the drowpdown KYC Details
	 *
	 * @since    1.0.0
	 */
	public function get_dropdown_details() {
			$url = API_BASE_URL . API_KYC_GET_DROPDOWN_DATA;
			// $this->nfsc_log($url);
			return $this->processGetDropdownData($url);
	}

	/**
	 * get the user ticket information
	 *
	 * @since    1.0.0
	 */
	public function get_ticket_list ( $account_number ) {
		if($account_number){
			$param = array (
				'AccountNumber' => $account_number
			);
			$url = API_BASE_URL . API_TICKET_LIST;
			return $this->processGetHttpService($url, $param);
		}
	}

	/**
	 * get the ticket Type
	 *
	 * @since    1.0.0
	 */
	public function get_ticket_type ( $account_number ) {
		if($account_number){
			$param = array (
				'AccountNumber' => $account_number
			);
			$url = API_BASE_URL . API_TICKET_CATEGORY;
			return  $this->processGetHttpService($url, $param);
		}
	}

	/**
	 * get the payment details. 
	 *
	 * @since    1.0.0
	 */
	public function user_payment_detail ( $account_number ) {
		if($account_number){
			$param = array (
				'AccountNumber' => $account_number
			);
			$url = API_BASE_URL . API_PAYMENT_DETAILS;
			return $this->processGetHttpService($url, $param);
		}
	}

	/**
	 * Get the Payment Schedule.
	 *
	 * @since    1.0.0
	 */
	public function user_payment_schedule ( $account_number ) {
		if($account_number){
			$param = array (
				'AccountNumber' => $account_number
			);
			$url = API_BASE_URL . API_PAYMENT_SCHEDULE;
			return $this->processGetHttpService($url, $param);
		}
	}
	/**
	 * Post the SMS Payment Schedule.
	 *
	 * @since    1.0.0
	 */
	public function sms_payment_schedule() {
		session_start();
		$account_number = $_POST['accountNo'];
		if($account_number){
			$param = array (
				'AccountNumber' => $account_number
			);
			$url = API_BASE_URL . API_SMS_PAYMENT_SCHEDULE;
			$data = $this->processPostHttpService($url, $param);
			$this->nfsc_log($url);
			$this->nfsc_log($data);
			if($data){
				$response = array('status' => $data->StatusCode);
				$logData = array('status' => $data->StatusCode);
			}		
		$this->nfsc_log($logData);
		echo json_encode($response);
		die;

		}
	}
	/**
	 * get the ticket category by ticket type
	 *
	 * @since    1.0.0
	 */
	public function get_ticket_category () {
		session_start();
		global $wpdb;
		$type_id = (int) filter_var($_POST['type_id'], FILTER_SANITIZE_NUMBER_INT);
		$currentLang = $_POST['lang'];
		if($type_id){
			
			$list = array();
			$url = API_BASE_URL . API_TICKET_CATEGORY;
			$data = $this->processGetHttpService($url, $param);
			$data = $data->objTicketModel;
			$TicketCategory = $data->TicketCategory;
			
			$list[] = '<option> '.$this->get_resource('lblSelectCategory').'</option>';
			foreach($TicketCategory as $category) {
				if($category->TicketTypeID == $type_id){ 
					$categoryLang = ($currentLang == 'en') ? $category->TicketCategoryNameInEn : $category->TicketCategoryNameInAr;
					$list[] = '<option value="'.$category->ID.'"  data-id="'.$category->ID.'">'.$categoryLang.'</option>';
				}
			}
			echo json_encode( $list );
			die;
		}
	}
	/**
	 * get the ticket sub category by ticket category
	 *
	 * @since    1.0.0
	 */
	public function get_ticket_subcategory ( ) {
		session_start();
		$cat_id = (int) filter_var($_POST['cat_id'], FILTER_SANITIZE_NUMBER_INT);
		if($cat_id){
			$currentLang = $_POST['lang'];
			$list = array();
			$url = API_BASE_URL . API_TICKET_CATEGORY;
			$data = $this->processGetHttpService($url, $param);
			$data = $data->objTicketModel;
			$TicketSubCategory = $data->TicketSubCategory;
			$list[] = '<option> '.$this->get_resource('lblSelectSubCategory').'</option>';
			$count = 0;
			foreach($TicketSubCategory as $subcategory) {
				if($subcategory->TicketCategoryID == $cat_id){
					$subcategoryLang = ($currentLang == 'en') ? $subcategory->TicketSubCategoryNameInEn : $subcategory->TicketSubCategoryNameInAr;
					$list[] = '<option value="'.$subcategory->ID.'">'.$subcategoryLang.'</option>';
					$count++;
				}
			}
			if($count < 1){
				echo json_encode(array('status' => 'error'));
				die;
			}
			echo json_encode( $list );
			die;
		}
	}

	public function createTicket(){
		session_start();
		$data = $_POST['data'];
		$ticketType = $_POST['ticketType'];
		$ticket_category = $_POST['ticket_category'];
		$ticket_subcategory = $_POST['ticket_subcategory'];
		$ticket_description = $_POST['ticket_description'];
		$file = $_FILES["file"];
		$filesName = $_FILES["file"]['name'];
	
		$account_number = $_SESSION['AccountNumber'];

		if($account_number){
			$param = array (
				'AccountNumber' => $account_number,
				'TicketType' => $ticketType ,
				'TicketCategory' => $ticket_category,
				'TicketSubCategory' => $ticket_subcategory,
				'TicketDescription' => $ticket_description,
				'fileName' => $filesName,
				'file' => $file,
				'ChannelType' => 'Web'
			);
			
			$url = API_BASE_URL . API_TICKET_CREATE;
			$data = $this->createTicketPostHttpService($url, $param);
			
			if($data->NewTicketNumber){
				$response = array('status' => "true", 'msg' => $this->get_resource('lblTicketsubmitted'));
				$logData = array('status' => 'Ticket Created', 'Account Number' => substr_replace($account_number,'XXXXXXXX', 0, 8));
			} else{
				$response = array('status' => "false", 'msg' => $this->get_resource('errorTicketSubmit'));
				$logData = array('status' => 'Ticket not created', 'Account Number' => substr_replace($account_number,'XXXXXXXX', 0, 8));
			}
		}else{
			$response = array('status' => "false", 'msg' => 'Session has been expire. Please login again');
			$logData = array('status' => 'Session Expired while creating ticket', 'Account Number' => substr_replace($account_number,'XXXXXXXX', 0, 8));
		}
		$this->nfsc_log($logData);
		echo json_encode($response);
		die;
		
	}
	/**
	 * Put account number insession redirect user to dashboard
	 *
	 * @since    1.0.0
	 */
	public function viewDetailsAccountNumber() {
		session_start();
		$account_number = $_POST['accno'];
		$_SESSION['AccountNumber']  = $account_number;
		$lang = $_POST['lang'];
		if($lang == 'ar'){
			$response = home_url().'/dashboard-ar';
		} else {
			$response = home_url().'/en/dashboard';
		}
		echo $response;
		die;
	}
	/**
	 * Logout user
	 *
	 * @since    1.0.0
	 */
	public function logout () {
		session_start();
		session_destroy();
		$lang = $_POST['lang'];
		if($lang == 'ar'){
			$response = home_url().'/login-ar';
		} else {
			$response = home_url().'/en/login';
		}
		echo $response;
		die;
	}
	/**
	 * This function add user logs in the database.
	 *
	 * @since 1.0.1
	 *
	 * @return json
	 */

	public function add_user_logs(){
		session_start();
		$isSystemLogout = $_POST['SystemLogout'];
		$LogId = $_POST['logid'];
			$param = array (
				'logId' => $LogId,
				'isSystemLogout' =>$isSystemLogout
			);

		$url = API_BASE_URL . API_USER_LOGOUT;
		$data = $this->processPostHttpService($url, $param);
			if($data->status == 'success'){
				$response = array('status' => "true", 'msg' => 'Noramal Logout');
				$logData = array('status' => 'Valid Normal Logout');
			}		
		//$this->nfsc_log($logData);
		echo json_encode($response);
		die;
	}

	/**
	 * This function for user's activity track.
	 *
	 * @since 1.0.1
	 *
	 * @return json
	 */

	public function users_activity_log(){
		session_start();
		$LogId = $_POST['logid'];
		$accountno = $_POST['accountno'];
		$tabname = $_POST['tabname'];
			$param = array (
				'LogId' => $LogId,
				'CustomerId' => $accountno,
				'ActivityPage' => $tabname
			);

		$url = API_BASE_URL . API_USER_ACTIVITY_LOG;
		$data = $this->processActivityPostHttpService($url, $param);
			if($data->status == 'success'){
				$response = array('status' => "true", 'msg' => 'Activity Traced');
				$logData = array('status' => 'Activity Traced');
			}		
		echo json_encode($response);
		die;
	}
	/**
	 * This function for refresh Function
	 *
	 * @since 1.0.1
	 *
	 * @return json
	 */
	public function jwt_refresh_token(){
		session_start();
		$token =  $_SESSION['Token'];
		$refresh_token = $_SESSION['RefreshToken'];
			$param = array (
				'Token' => $token,
				'RefreshToken' =>$refresh_token
			);

		$url = API_BASE_URL . API_REFRESH_TOKEN;

		$data = $this->processKycPostHttpService($url, $param);
			if($data->status == 'success'){
				$_SESSION['Token'] = $data->Token ;
				$_SESSION['RefreshToken'] =  $data->RefreshToken;
				$response = array('status' => "true", 'token' => $data->Token, 'RefreshToken' => $data->RefreshToken);
				$logData = array('status' => "true", 'token' => $data->Token, 'RefreshToken' => $data->RefreshToken);
			}		
		//$this->nfsc_log($logData);
		echo json_encode($response);
		die;
	}
	/**
	 * This is HTTP POST Method of the WordPress.
	 *
	 * @since 1.0.1
	 *
	 * @return json
	 */
	public function processPostHttpService( $url, $param ){   
		$param_string      = http_build_query($param);
		$final_url         = $url . '?' . $param_string;
		$token =  $_SESSION['Token'];
		$args = array(
			'headers' => array(
				'Authorization' => 'Bearer '. $token, 
			),
			'body' => 'grant_type=client_credentials'
		);
		try {
			$responseData = wp_remote_post($final_url, $args);
			return json_decode($responseData['body']);
		} catch (Exception $e) {
			return $e->getMessage();
			}
		
	}

	/**
	 * This is KYC HTTP POST Method of the WordPress.
	 *
	 * @since 1.0.1
	 *
	 * @return json
	 */
	public function processKycPostHttpService( $url, $param ){   
		$param_string      = http_build_query($param);
		$final_url         = $url;
		try {
			$responseData = wp_remote_post($final_url, array(
				'method'      => 'POST',
				'timeout'     => 45,
				'redirection' => 5,
				'httpversion' => '1.0',
				'blocking'    => true,
				'headers'     => array(),
				'body'        =>  $param,
				'cookies'     => array()
				));
			return json_decode($responseData['body']);
		} catch (Exception $e) {
			return $e->getMessage();
			}
	}
	/**
	 * This is Activity HTTP POST Method of the WordPress.
	 *
	 * @since 1.0.1
	 *
	 * @return json
	 */
	public function processActivityPostHttpService( $url, $param ){   
		$param_string      = http_build_query($param);
		$final_url         = $url . '?' . $param_string;
		$token =  $_SESSION['Token'];
		$args = array(
			'headers' => array(
				'Authorization' => 'Bearer '. $token, 
			),
			'body' => $param
		);
		$responseData = wp_remote_post($final_url, $args);
		try {
			
			return json_decode($responseData['body']);
		} catch (Exception $e) {
			return $e->getMessage();
			}	
			
	}
	/**
	 * This is create Ticket HTTP POST Method of the WordPress.
	 *
	 * @since 1.0.1
	 *
	 * @return json
	 */
	public function createTicketPostHttpService( $url, $param ){   
		$token =  $_SESSION['Token'];	
		$file = $_FILES["file"];
		$filesName = $_FILES["file"]['name'];
		$fileTemp = $_FILES['file']['tmp_name'];

		$dirPath = plugin_dir_path( dirname( __FILE__ ) );
		$target = $dirPath.'uploads'.'/'. basename($filesName) ;
		move_uploaded_file($_FILES['file']['tmp_name'],$target);
		$responses = 0;
	    if(move_uploaded_file($_FILES['file']['tmp_name'],$target)){
			  $responses = $target;
		   }
		$local_file = $target ; //path to a local file on your server
		
		if($param['TicketSubCategory'] == 'null'){
			$param['TicketSubCategory'] = 0;
		}

		$post_fields = array(
			'AccountNumber' => $param['AccountNumber'],
			'TicketType' => $param['TicketType'],
			'TicketCategory' => $param['TicketCategory'],
			'TicketSubCategory' => $param['TicketSubCategory'],
			'TicketDescription' => $param['TicketDescription'],
			'ChannelType' => 'Web',
			'fileName' => $filesName
		);
		$boundary = wp_generate_password( 24 );
		$headers  = array(
			'Authorization' => 'Bearer '. $token,
			'content-type' => 'multipart/form-data; boundary=' . $boundary,
		);
		$payload = '';
		// First, add the standard POST fields:
		foreach ( $post_fields as $name => $value ) {
			$payload .= '--' . $boundary;
			$payload .= "\r\n";
			$payload .= 'Content-Disposition: form-data; name="' . $name .
				'"' . "\r\n\r\n";
			$payload .= $value;
			$payload .= "\r\n";
		}
		// Upload the file
		if ( $local_file ) {
			$payload .= '--' . $boundary;
			$payload .= "\r\n";
			$payload .= 'Content-Disposition: form-data; name="' . 'file' .
			'"; filename="' . $filesName . '"' . "\r\n";
			//        $payload .= 'Content-Type: image/jpeg' . "\r\n";
			$payload .= "\r\n";
			$payload .= file_get_contents( $local_file );
			$payload .= "\r\n";
		}
		$payload .= '--' . $boundary . '--';
		$responseData = wp_remote_post( $url,
			array(
				'headers'    => $headers,
				'body'       => $payload,
			)
		);

		try {
			
			return json_decode($responseData['body']);
		} catch (Exception $e) {
			return $e->getMessage();
			}	
			
	}
	/**
	 * This is HTTP GET Method of the WordPress.
	 *
	 * @since 1.0.1
	 *
	 * @return json
	 */
	public function processGetHttpService( $url, $param ){
		$param_string      = http_build_query($param);
		$final_url         = $url . '?' . $param_string;
		$token =  $_SESSION['Token'];
		$args = array(
			'headers' => array(
				'Authorization' => 'Bearer '. $token
			)
		);
		$getData           = wp_remote_get($final_url, $args);
		try {
			return json_decode($getData['body']);
		} catch (Exception $e) {
			return $e->getMessage();
		}
	}

	/**
	 * This is HTTP GET Method of the WordPress.
	 *
	 * @since 1.0.1
	 *
	 * @return json
	 */
	public function processKYCGetHttpService( $url, $param ){
		$param_string      = http_build_query($param);
		$final_url         = $url . '?' . $param_string;
		$getData           = wp_remote_get($final_url);
		try {
			return json_decode($getData['body']);
		} catch (Exception $e) {
			return $e->getMessage();
		}
	}

	/**
	 * This is HTTP GET Method of the WordPress.
	 *
	 * @since 1.0.1
	 *
	 * @return json
	 */
	public function processGetDropdownData( $url ){
		// $param_string      = http_build_query($param);
		$final_url         = $url;
		$getData           = wp_remote_get($final_url);
		try {
			return json_decode($getData['body']);
		} catch (Exception $e) {
			return $e->getMessage();
		}
	}

	/**
	 * This function get the resource from the database for the arabic and English.
	 *
	 * @since 1.0.1
	 *
	 * @return json
	 */
	public function get_resource($key){
		global $wpdb;
		if (pll_current_language() == 'en') {
			$sql = "SELECT `ResourceEnglishName` FROM `$this->resource_table` WHERE `ResourceKey` = '$key'";
		} else {
		
			$sql = "SELECT `ResourceArabicName` FROM `$this->resource_table` WHERE `ResourceKey` = '$key'";
		}

		 return $wpdb->get_var($sql, 0);
	}

	/**
	 * This function get the all resource from the database for the arabic and English.
	 *
	 * @since 1.0.1
	 *
	 * @return json
	 */
	public function get_resource_all(){
		global $wpdb;
		$sql = "SELECT * FROM `$this->resource_table`";
		return $wpdb->get_results($sql);
	}
	/**
	 * This function add resource in the database for the arabic and English.
	 *
	 * @since 1.0.1
	 *
	 * @return json
	 */
	public function add_resource(){
		$data = $_POST['resourceData'];
		parse_str($data, $resourceData);
		global $wpdb;
		$resourceExists = $wpdb->get_row( "SELECT * FROM $this->resource_table WHERE ResourceKey = '".$resourceData['resource_key']."'" );
		
		if($resourceExists){
			$response = array('status' => "false", 'msg' => 'Resource Exists. Please use another key');
		} else {
			$insertResponse = $wpdb->insert( 
				$this->resource_table, 
				array( 
					'ResourceKey' => $resourceData['resource_key'], 
					'ResourceEnglishName' => $resourceData['resource_english'], 
					'ResourceArabicName' => $resourceData['resource_arabic']
				), 
				array( 
					'%s', 
					'%s', 
					'%s',					) 
			);
			if($insertResponse){
				$response = array('status' => "true", 'msg' => 'Resource added successfully');
			} else {
				$response = array('status' => "false", 'msg' => 'Facing issue while adding resource');
			}
			
		}
		echo json_encode($response);
		die;
	}

	/**
	 * This function edit resource in the database for the arabic and English.
	 *
	 * @since 1.0.1
	 *
	 * @return json
	 */
	public function edit_resource(){
		$data = $_POST['resourceData'];
		parse_str($data, $resourceData);
		global $wpdb;
		return $wpdb->update( 
			'f6S_lang_resource', 
			array( 
				'ResourceEnglishName' => $resourceData['resource_english'], 
				'ResourceArabicName' => $resourceData['resource_arabic']
			), 
			array( 'id' => (int)$resourceData['resource_id']), 
			array( 
				'%s',   // value1
				'%s'    // value2
			), 
			array( '%d' ) 
		);
	}

	public function nfsc_log($logdata){
		$log_filename = plugin_dir_path( dirname( __FILE__ ) ) . '/logs';
		if (!file_exists($log_filename)) 
		{
			// create directory/folder uploads.
			mkdir($log_filename, 0777, true);
		}
		$log  =  "User: ".$_SERVER['REMOTE_ADDR'].' - '.date("F j, Y, g:i a").PHP_EOL.
		json_encode($logdata).PHP_EOL.
            "-------------------------".PHP_EOL;
		file_put_contents($log_filename . '/log_'.date("j.n.Y").'.log', $log, FILE_APPEND);
	}
}
