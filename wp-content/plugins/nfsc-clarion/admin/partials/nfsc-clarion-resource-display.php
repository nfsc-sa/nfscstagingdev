<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       https://profiles.wordpress.org/yogeshdalavi/
 * @since      1.0.0
 *
 * @package    Nfsc_Clarion
 * @subpackage Nfsc_Clarion/admin/partials
 */
?>
<link href="https://cdn.datatables.net/1.10.25/css/jquery.dataTables.min.css" rel="stylesheet" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css" />  
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js"></script>
<script src="<?php echo plugin_dir_url( dirname( __FILE__ ) ).'../includes/template/assets/js/nfsc-dashboard.js'; ?> "></script>
<script type="text/javascript" src="<?php echo plugin_dir_url( dirname( __FILE__ ) ).'../includes/template/assets/dist/js/bootstrapValidator.min.js'; ?>"></script>
<!-- This file should primarily consist of HTML with a little bit of PHP. -->
<?php
$objAdmin = new Nfsc_Clarion_Admin('nfsc-clarion', '1.0.1');
$all_resource = $objAdmin->get_resource_all();
?>
<div class="container" style="margin-top:35px;">
    <button type="button" class="btn btn-primary btnAddResource" style=" position: absolute; z-index: 1; ">Add New Resource</button>
    <table class="wp-list-table widefat fixed striped table-view-list pages table align-items-center table-striped table-nowrap mb-0" id="resourceList" aria-describedby="ticketlist" aria-hidden="true">
        <thead>
            <tr>
                <th class="text-nowrap text-center" scope="row">Sr no</th>
                <th class="text-nowrap text-center" scope="row">Resource Key</th>
                <th class="text-nowrap text-center" scope="row">Resource - English</th>
                <th class="text-nowrap text-center" scope="row">Resource - Arabic</th>
                <th class="text-nowrap text-center" scope="row">Edit</th>
            </tr>
        </thead>
        <tbody>
         <?php if($all_resource) { ?>
            <?php foreach($all_resource as $resource) { ?>
            <tr>
                <td class="classID"><?php echo $resource->id; ?></td>
                <td><?php echo $resource->ResourceKey; ?></td>
                <td><?php echo $resource->ResourceEnglishName; ?></td>
                <td><?php echo $resource->ResourceArabicName; ?></td>
                <td><span class="dashicons dashicons-edit resource-edit" style="cursor:pointer;"></span></td>
            </tr>
            <?php } ?>
         <?php } else { ?>
            <tr>No record</tr>   
         <?php } ?>   
        </tbody>
    </table>
</div>
<!--Add Resource Modal -->
<div class="modal fade" id="addResourceModal" tabindex="-1" role="dialog" aria-labelledby="AddModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>

         <h3 class="modal-title" id="AddModalLabel">Add Resource</h3>
        
      </div>
      <form id="addResourceForm" method="POST" action="" role="form" aria-labelledby="mainnavheading">
        <div class="modal-body">
            <div class="form-group">
                <label for="addresourceKeyInput">Resource Key</label>
                <input type="text" class="form-control" id="addresourceKeyInput" name="resource_key">
            </div>
            <div class="form-group">
                <label for="addresourceEnglishInput">Resource in English</label>
                <input type="text" class="form-control" id="addresourceEnglishInput" name="resource_english">
            </div>
            <div class="form-group">
                <label for="addresourceArabicInput">Resource in Arabic</label>
                <input type="text" class="form-control" id="addresourceArabicInput" name="resource_arabic">
            </div>
        </div>
        <div class="modal-footer">
            <div class="addresourceresponse"></div>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary addResource">Save</button>
        </div>
      </form>
    </div>
  </div>
</div>
<!--Edit Resource Modal -->
<div class="modal fade" id="editResourceModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>

         <h3 class="modal-title" id="exampleModalLabel">Edit Resource</h3>
        
      </div>
      <form id="editResourceForm" method="POST" action="" role="form" aria-labelledby="mainnavheading">
        <div class="modal-body">
        
                <div class="form-group">
                    <label for="resourceKeyInput">Resource Key</label>
                    <input type="text" class="form-control" id="resourceKeyInput" name="resource_key" readonly>
                </div>
                <div class="form-group">
                    <label for="resourceEnglishInput">Resource in English</label>
                    <input type="text" class="form-control" id="resourceEnglishInput" name="resource_english">
                </div>
                <div class="form-group">
                    <label for="resourceArabicInput">Resource in Arabic</label>
                    <input type="text" class="form-control" id="resourceArabicInput" name="resource_arabic">
                </div>
            
        </div>
        <div class="modal-footer">
            <div class="editresponse"></div>
            <input type="hidden"id="resourceIDInput" name="resource_id">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary btnsaveResource">Save changes</button>
        </div>
      </form>
    </div>
  </div>
</div>