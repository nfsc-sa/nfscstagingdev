<?php

/**
 * Fired during plugin deactivation
 *
 * @link       https://profiles.wordpress.org/yogeshdalavi/
 * @since      1.0.0
 *
 * @package    Nfsc_Clarion
 * @subpackage Nfsc_Clarion/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Nfsc_Clarion
 * @subpackage Nfsc_Clarion/includes
 * @author     Yogesh Dalavi <yogesh.dalavi@clariontechnologies.co.in>
 */
class Nfsc_Clarion_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
