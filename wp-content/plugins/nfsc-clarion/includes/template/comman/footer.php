 <?php
 $plugin_aseets_path = plugin_dir_url( dirname( __FILE__ ) ) ;
 ?>
 <!-- End Navbar -->
    <div class="container-fluid py-4">
        <footer class="footer py-5">
            <div class="container">
            
            <div class="row">
                <div class="col-8 mx-auto text-center mt-1">
                <p class="mb-0 text-secondary">
                    <?php echo $lblCopyright; ?>
                </p>
                </div>
            </div>
            </div>
        </footer>
    </div>
</main>
<script>
    var lblSessionExpire = '<?php echo $lblSessionExpire; ?>';
    var btnStayConnected = '<?php echo $btnStayConnected; ?>';
    var lblRedirectTime = '<?php echo $lblRedirectTime; ?>';
    var btnLogOut = '<?php echo $btnLogOut; ?>';
    var lblSelectSubCategory = '<?php echo $lblSelectSubCategory; ?>';
</script>  
<script src="<?php echo $plugin_aseets_path.'assets/js/core/popper.min.js';?>" ></script>
<script src="<?php echo $plugin_aseets_path.'assets/js/core/bootstrap.min.js';?> " ></script>
<script src="<?php echo $plugin_aseets_path.'assets/js/plugins/perfect-scrollbar.min.js';?>" ></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/flatpickr/4.6.9/flatpickr.min.js" ></script>
<script src="<?php echo $plugin_aseets_path.'assets/js/plugins/smooth-scrollbar.min.js'; ?> "></script>
<script src="<?php echo $plugin_aseets_path.'assets/js/plugins/chartjs.min.js'; ?> "></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?php echo $plugin_aseets_path.'/assets/dist/js/bootstrapValidator.min.js'; ?>"></script>
<script src="<?php echo $plugin_aseets_path.'assets/js/bootstrap-session-timeout.js'; ?> "></script>
<script src="https://cdn.jsdelivr.net/npm/apexcharts"></script>
<script src="<?php echo $plugin_aseets_path.'assets/js/nfsc-dashboard.js'; ?> "></script>
