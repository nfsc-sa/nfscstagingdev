<?php
$plugin_aseets_path = plugin_dir_url( dirname( __FILE__ ) ) ;
$translations = pll_the_languages(array('raw'=>1));
if (pll_current_language() == 'ar') {
  $bodyClass = "ar-container";
  $html_lang = 'lang="ar" dir="rtl"';
  $LangText = $translations['en']['name'];
  $LangLink = $translations['en']['url'];
  $dashboard_url = home_url().'/dashboard-ar';
  $accoount_url = home_url().'/account-ar';
}else{
  $bodyClass = "en-container";
  $html_lang = 'lang="en"';
  $LangText = $translations['ar']['name'];
  $LangLink = $translations['ar']['url'];
  $dashboard_url = home_url().'/en/dashboard';
  $accoount_url = home_url().'/en/account';
}  
?>
<!DOCTYPE html>
<html <?php echo $html_lang; ?>>
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    <title>
      <?php wp_title(''); ?> | <?php echo get_bloginfo( 'name' ); ?>
      
    </title>
    <!--     Fonts and icons     -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <link href="<?php echo $plugin_aseets_path.'/assets/css/nucleo-svg.css'; ?>" rel="stylesheet" />
    <!-- Font Awesome Icons -->
    <script src="https://kit.fontawesome.com/42d5adcbca.js" crossorigin="anonymous"></script>
    <script> var ajaxurl = "<?php echo admin_url('admin-ajax.php'); ?>";</script>
    <link href="https://cdn.datatables.net/1.10.25/css/jquery.dataTables.min.css" rel="stylesheet" />
    <!-- CSS Files -->
    <link id="pagestyle" href="<?php echo $plugin_aseets_path.'/assets/css/soft-ui-dashboard.css?v=1.0.3';?>" rel="stylesheet" />
    <link id="nucleo" href="<?php echo $plugin_aseets_path.'/assets/css/nucleo-icons.css?v=1.0.3';?>" rel="stylesheet" />
    <link id="adminstyle" href="<?php echo $plugin_aseets_path.'../../admin/css/nfsc-clarion-admin.css?v=1.0.3';?>" rel="stylesheet" />
    <?php
    $session_warining  = (get_option( 'session_warning' )) ? (get_option( 'session_warning' ) * 60000) : '120000';
    $session_end  = (get_option( 'session_redirect' )) ? (get_option( 'session_redirect' ) * 60000) : '180000';
    $ticket_description_length = (get_option( 'ticket_description' )) ? get_option( 'ticket_description' ) : '500';
    ?>
    <script>
      var session_timeout_warining_time = '<?php echo $session_warining; ?>';
      var session_timeout_redirect_time = '<?php echo $session_end ?>';
      var ticket_description_length = '<?php echo $ticket_description_length ?>';
      var home_page_url = '<?php echo $dashboard_url; ?>';
      var logoutLang = '<?php echo pll_current_language(); ?>';
    </script>
  </head>
  <body class="g-sidenav-show bg-gray-100 <?php echo $bodyClass; ?>" <?php echo $html_lang; ?>>
    
    <main class="main-content position-relative max-height-vh-100 h-100 mt-1 border-radius-lg">
      <!-- Navbar -->
      <nav class="navbar navbar-main navbar-expand-lg px-0 navbar-light bg-white" id="navbarBlur" navbar-scroll="true" aria-label="true">
        <div class="container-fluid py-1 px-3 container">
          <nav aria-label="navbar-brand m-0">
            <a href="<?php echo home_url(); ?>"><img src="<?php echo esc_url( wp_get_attachment_url( get_theme_mod( 'custom_logo' ) ) );?>" class="navbar-brand-img h-100" alt="main_logo"></a>
          </nav>
          <div class="collapse navbar-collapse mt-sm-0 mt-2 me-md-0 me-sm-4 ms-3" id="navbar">
           
            <div class="ms-md-auto pe-md-3 d-flex align-items-center">
            <a href="<?php echo $accoount_url; ?>"><img src="<?php echo plugin_dir_url( dirname( __FILE__ ) ) . 'assets/img/Accounts.png'; ?>" class="account-img" alt="account"></a>
            </div>
            
            <div class="dropdown">
              <a href="#" class="dropdown-toggle " data-bs-toggle="dropdown" id="navbarDropdownMenuLink2">
                  <?php echo $lblWelome;?>, 
                  <?php //echo ($userData->FullName) ? $userData->FullName : '';  
                     echo ($FullName) ? $FullName : ''; 
                  ?>         
              </a>
              <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink2">
                <?php pll_the_languages(array( 'hide_current'  => 1));?>
                <li>
                    <a class="dropdown-item btnLogout" href="#">
                      <span class="d-sm-inline "><?php echo $btnLogOut; ?></span>
                    </a>
                </li>
                
               </ul>
           </div>
           
             
        </div>
      </nav>
    
   