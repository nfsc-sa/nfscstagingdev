<?php 
$plugin_aseets_path = plugin_dir_url( dirname( __FILE__ ) ) ;
require_once plugin_dir_path( dirname( __FILE__ ) ) . '../admin/constant/lang.php'; 
?>
<!DOCTYPE html>
<html <?php echo $html_lang; ?> >

<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  
  <title>
    <?php wp_title(''); ?> | <?php echo get_bloginfo( 'name' ); ?>
    
  </title>
  <!--     Fonts and icons     -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" />
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
  <link href="<?php echo $plugin_aseets_path.'/template/assets/css/nucleo-svg.css'; ?>" rel="stylesheet" />
  <!-- Font Awesome Icons -->
  <script src="https://kit.fontawesome.com/42d5adcbca.js" crossorigin="anonymous"></script>
  <link href="<?php echo $plugin_aseets_path.'/template/assets/css/nucleo-svg.css';?>" rel="stylesheet" />
  <!-- CSS Files -->
  <link rel="stylesheet" href="<?php echo $plugin_aseets_path.'/template/assets/dist/css/bootstrapValidator.css'; ?>" />
  <script src="https://www.google.com/recaptcha/api.js?onload=loginCaptcha&render=explicit&hl=<?php echo pll_current_language(); ?>" async defer></script>
  <link id="pagestyle" href="<?php echo $plugin_aseets_path.'/template/assets/css/soft-ui-dashboard.css?v=1.0.3';?>" rel="stylesheet" />
  <link id="adminstyle" href="<?php echo $plugin_aseets_path.'../admin/css/nfsc-clarion-admin.css?v=1.0.3';?>" rel="stylesheet" />
  
</head>

<body class="login-page login-background">
  
<main class="main-content mt-0">
   <section>
      <div class="page-header min-vh-75">
         <div class="container">
            <div class="row">
               <div class="col-xl-4 col-lg-5 col-md-6 d-flex flex-column mx-auto">
                  <div class="card card-plain mt-6">
                     <div class="card-header pb-3 text-left bg-transparent">
                        <a class="navbar-brand m-0" href="<?php echo home_url(); ?>">
                        <img src="<?php echo $plugin_aseets_path.'template/assets/img/logo.png';?>" class="navbar-brand-img h-100" alt="main_logo" style="width:100%;">
                        </a>
                     </div>
                     <div class="card-body">
                       <div class="text-center pt-4 pb-3 border-radius-lg bg-warning">
                          <h4><?php echo $lblOnlineServiceUnavailableHeading; ?></h4>
                          <p class="text-dark"><?php echo $lblOnlineServiceUnavailableSubHeading; ?></p>
                      </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </section>
</main>
 
  <!-- -------- START FOOTER 3 w/ COMPANY DESCRIPTION WITH LINKS & SOCIAL ICONS & COPYRIGHT ------- -->
  <footer class="footer py-5">
    <div class="container">
      
      <div class="row">
        <div class="col-8 mx-auto text-center mt-1">
          <p class="mb-0 text-secondary">
            <?php echo $lblCopyright ; ?>
          </p>
        </div>
      </div>
    </div>
  </footer>
</body>

</html>