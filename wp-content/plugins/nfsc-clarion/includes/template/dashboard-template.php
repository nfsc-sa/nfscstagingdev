<?php 
session_start();
if(!$_SESSION['NationalId']){  
  wp_redirect(home_url('/en/login')); 
}
$accountNumber = $_SESSION['AccountNumber'];
// $LogId = $_SESSION['LogId'];
$objAdmin = new Nfsc_Clarion_Admin('nfsc-clarion', '1.0.1');
$language = pll_current_language();
$objAdmin->maintenace_mode();
$userData = $objAdmin->user_account_info($accountNumber);
$userData = $userData->data;
$FullName = $userData->FullName;
$ticketData = $objAdmin->get_ticket_list($accountNumber);
$ticketData = $ticketData->objTicketModel;
$ticketType = $objAdmin->get_ticket_type($accountNumber);
$ticketType = $ticketType->objTicketModel;
$payment_details = $objAdmin->user_payment_detail($accountNumber);
$payment_details = $payment_details->data;
$payment_schedule = $objAdmin->user_payment_schedule($accountNumber);
$payment_schedules = $payment_schedule->data;
require_once plugin_dir_path( dirname( __FILE__ ) ) . '../admin/constant/lang.php'; 
require_once plugin_dir_path( dirname( __FILE__ ) ) . 'template/comman/header.php'; 
$btnLogOut = $objAdmin->get_resource('btnLogOut');
$LMSReferenceNo = $userData->LMSReferenceNo;
$MortgageLoanAccountNumber = $userData->MortgageLoanAccountNumber;
$NationalID = $userData->NationalID;
$NIDExpiryDate = ($userData->NIDExpiryDate) ? date('d-m-Y', strtotime($userData->NIDExpiryDate)) : '';
$OriginationDate = ($userData->OriginationDate) ? date('d-m-Y', strtotime($userData->OriginationDate)) : '';
$OriginatorNameInEn = $userData->OriginatorNameInEn;
$ContractTypeInEn = $userData->ContractTypeInEn;
$ContractTypeInAr = $userData->ContractTypeInAr;
$FullName = $userData->FullName;
$LegalOwner = $userData->LegalOwner;
$MobileNumber = $userData->MobileNumber;
$MaturityDate = ($userData->MaturityDate) ? date('d-m-Y', strtotime($userData->MaturityDate)) : '';
$DelinquentBalance =  number_format($userData->DelinquentBalance, 2);
$NumberOfPaidInstallments = $userData->NumberOfPaidInstallments;
$OutstandingPrincipal = number_format($userData->OutstandingPrincipal, 2);
$DueAmount = number_format($userData->DueAmount, 2);
$DueDate = $userData->DueDate;
$RemainingTerm = $userData->RemainingTerm;
$InterestTypeOrigination = $userData->InterestTypeOrigination;
$InitialMonthlyInstallment = number_format($userData->InitialMonthlyInstallment, 2);
$OriginalAPR = $userData->OriginalAPR;
$OriginalLoanAmount = number_format($userData->OriginalLoanAmount, 2);
$UnpaidPrincipalBalance = number_format($userData->UnpaidPrincipalBalance, 2);
$LastRepricingDate = ($userData->LastRepricingDate) ? date('d-m-Y', strtotime($userData->LastRepricingDate)) : '';
$FinalPayment = $userData->FinalPayment;
$Outstanding = number_format($userData->Outstanding, 2);
$InterestRateMargin = $userData->InterestRateMargin;
$TermCostRate = $userData->TermCostRate;
$CurrentLoanAmount = number_format($userData->CurrentLoanAmount, 2);
$TotalPayableAmount = number_format($userData->TotalPayableAmount, 2);
$InitialTerm = $userData->InitialTerm;
$IsGuarantor = $userData->IsGuarantor;
$IsRestructured = $userData->IsRestructured;
$InitalMaturityDate = date('d-m-Y', strtotime($userData->InitalMaturityDate));
$CurrentMonthlyInstallment = number_format($userData->CurrentMonthlyInstallment, 2);
$InstallmentDay = $userData->InstallmentDay;
$FirstPaymentDate = ($userData->FirstPaymentDate) ? date('d-m-Y', strtotime($userData->FirstPaymentDate)) : '' ;
$PaymentFrequency = $userData->PaymentFrequency;
$DownPayment = number_format($userData->DownPayment, 2);
$TotalPaidAmount = number_format($userData->TotalPaidAmount, 2);
$PaidAmount = !is_null($userData->TotalPaidAmount) ? $userData->TotalPaidAmount : 0;
$OutstandingAmount = !is_null($userData->Outstanding) ? $userData->Outstanding : 0;
$DelinquentAmount = !is_null($userData->DelinquentBalance) ? $userData->DelinquentBalance : 0;
$REDFSubsidized = $payment_schedule->REDFSubsidized;
$IsContractClosed  = $userData->IsContractClosed;
?>
<script>
  var charData = [<?php  echo $PaidAmount; ?>,<?php echo $OutstandingAmount; ?>, <?php echo $DelinquentAmount; ?>];
  var errorPlsSelectTicket = "<?php echo $errorPlsSelectTicket; ?>";
  var errorPlsSelectTicketCat = "<?php echo $errorPlsSelectTicketCat; ?>";
  var errorPlsSelectTicketSubCat = "<?php echo $errorPlsSelectTicketSubCat; ?>";
  var errorPlsaddTicketDesc = "<?php echo $errorPlsaddTicketDesc; ?>";
  var errorTicketDescLength = "<?php echo $errorTicketDescLength; ?>";
  var lblPaid = "<?php echo $lblPaid; ?>";
  var lblUnpaid = "<?php echo $lblUnpaid; ?>";
  var lblDelinquentBalance = "<?php echo $lblDelinquentBalance; ?>";
  var errorAttachmentFileSize = "<?php echo $errorAttachmentFileSize; ?>";
</script>
<div class="container-fluid py-4 container dashboard-conatiner">

<section class="py-3">
  <div class="container">
    <div class="row">
       <div class="col-lg-7 mx-auto">
          <div class="nav-wrapper position-relative end-0">
             <ul class="nav nav-pills nav-fill p-1" role="tablist">
                <li class="nav-item">
                   <a class="nav-link mb-0 px-0 py-1 active" data-bs-toggle="tab" href="#profile-tabs-simple" role="tab" aria-controls="profile" aria-selected="true">
                      <h6 class="m-1 profile-tabs-simple"><?php echo $lblAccountInfo; ?></h6>
                   </a>
                </li>
                <li class="nav-item ticketTab">
                   <a class="nav-link mb-0 px-0 py-1" data-bs-toggle="tab" href="#dashboard-tabs-simple" role="tab" aria-controls="dashboard" aria-selected="false">
                      <h6 class="m-1 dashboard-tabs-simple"><?php echo $lblTicketInfo; ?></h6>
                   </a>
                </li>
                <li class="nav-item paymentScheduleTab">
                   <a class="nav-link mb-0 px-0 py-1" data-bs-toggle="tab" href="#dashboard-tabs-payment-schedule" role="tab" aria-controls="payment-details" aria-selected="false">
                      <h6 class="m-1 dashboard-tabs-payment-schedule"><?php echo $lblPaymentSchedule; ?></h6>
                   </a>
                </li>
                <li class="nav-item paymentDetailsTab">
                   <a class="nav-link mb-0 px-0 py-1" data-bs-toggle="tab" href="#dashboard-tabs-payment-detail" role="tab" aria-controls="payment-details" aria-selected="false">
                      <h6 class="m-1 dashboard-tabs-payment-detail"><?php echo $lblPaymentDetails; ?></h6>
                   </a>
                </li>
              </ul>
          </div>
       </div>
       
    </div>
 </div>
</section>
<div class="tab-content tab-space">
   <!-- Start Tab 1 -->
   <div class="tab-pane active" id="profile-tabs-simple">
    <div class="row mt-4">
      <div class="col-lg-6">
        <div class="card">
          <div class="card-body p-3">
            <div class="d-flex flex-column justify-content-center text-center">
                <h6 class="mb-0 badge text-sm badge-soft-info"><?php echo $lblAccountNo; ?> - <?php echo $MortgageLoanAccountNumber; ?></h6>
                <input type="hidden" value="<?php echo $MortgageLoanAccountNumber; ?>" name="user_acc_no" class="user_acc_no">                 
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-6">
        <div class="card">
          <div class="card-body p-3">
            <div class="d-flex flex-column justify-content-center text-center">
                <h6 class="mb-0 badge text-sm badge-soft-info"><?php echo $lblSadadNumber; ?> - <?php echo $LMSReferenceNo; ?></h6>
              </div>
          </div>
        </div>
      </div>
    </div> 
    <div class="row mt-4">
      <div class="col-lg-6 ms-auto">
          <div class="card">
            <div class="card-body p-3">
                <div class="row">
                  <div class="col-12">
                      <div class="table-responsive">
                        <table class="table align-items-center mb-0"  aria-describedby="account" aria-hidden="true">
                            <tbody>
                              <tr>
                                  <td>
                                    <div class="d-flex px-2 py-0">
                                        <div class="d-flex flex-column justify-content-center">
                                          <h6 class="mb-0"><?php echo $lblFullName; ?></h6>
                                        
                                        </div>
                                    </div>
                                  </td>
                                  <td class="align-middle text-center">
                                    <p class="pb-0 mb-0"><?php echo $FullName; ?></p>
                                    <input type="hidden" value="<?php echo $FullName; ?>" name="fullname" class="fullname">
                                  </td>
                              </tr>
                              <tr>
                                  <td>
                                    <div class="d-flex px-2 py-0">
                                        <div class="d-flex flex-column justify-content-center">
                                          <h6 class="mb-0"><?php echo $lblMobileNumber; ?></h6>
                                        </div>
                                    </div>
                                  </td>
                                  <td class="align-middle text-center">
                                  <p class="pb-0 mb-0"> <?php echo $MobileNumber; ?></p>
                                  </td>
                              </tr>
                              <tr>
                                  <td>
                                    <div class="d-flex px-2 py-0">
                                        <div class="d-flex flex-column justify-content-center">
                                          <h6 class="mb-0"><?php echo $lblproduct; ?></h6>
                                        </div>
                                    </div>
                                  </td>
                                  <td class="align-middle text-center">
                                    <p class="pb-0 mb-0"><?php echo ($language == 'ar' ) ? $ContractTypeInAr : $ContractTypeInEn; ?></p>
                                  </td>
                              </tr>
                              <tr>
                                  <td>
                                    <div class="d-flex px-2 py-0">
                                        <div class="d-flex flex-column justify-content-center">
                                          <h6 class="mb-0 "><?php echo $lblOutstandAmt; ?></h6>
                                        </div>
                                    </div>
                                  </td>
                                  <td class="align-middle text-center">
                                    <p class="pb-0 mb-0"><?php echo $OutstandingPrincipal; ?></p>
                                  </td>
                              </tr>
                              <tr>
                                  <td>
                                    <div class="d-flex px-2 py-0">
                                        <div class="d-flex flex-column justify-content-center">
                                          <h6 class="mb-0"><?php echo $lblCurrentAPR; ?></h6>
                                        </div>
                                    </div>
                                  </td>
                                  <td class="align-middle text-center">
                                      <p class="pb-0 mb-0"><?php echo $OriginalAPR.'%' ;?></p>
                                  </td>
                              </tr>
                              <tr>
                                <td>
                                  <div class="d-flex px-2 py-0">
                                      <div class="d-flex flex-column justify-content-center">
                                        <h6 class="mb-0"><?php echo $lblRemaningTerm; ?></h6>
                                      </div>
                                  </div>
                                </td>
                                <td class="align-middle text-center">
                                  <p class="pb-0 mb-0"><?php echo $RemainingTerm; ?></p>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                  <div class="d-flex px-2 py-0">
                                      <div class="d-flex flex-column justify-content-center">
                                        <h6 class="mb-0"><?php echo $lblMaturityDate; ?></h6>
                                      </div>
                                  </div>
                                </td>
                                <td class="align-middle text-center">
                                  <p class="pb-0 mb-0"> <?php echo $MaturityDate; ?></p>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                      </div>
                  </div>
                </div>
            </div>
          </div>
      </div>
      <div class="col-lg-6 mt-lg-0 mt-4">
        <div class="card h-100">
          <div class="card-body p-3">
                  <div class="row">
                      <div class="col-md-12">
                         <div id="chart"></div>
                      </div>

                      <div class="text-center text-muted">
                          <div class="row">
                              <div class="col-md-4 col-sm-12">
                                  <div class="mt-4">
                                      <p class="mb-2 text-truncate"><?php echo $lblPaid; ?></p>
                                      <h5 class="align-middle text-center"><?php echo $TotalPaidAmount; ?></h5>
                                  </div>
                              </div>
                              <div class="col-md-4 col-sm-12">
                                  <div class="mt-4">
                                      <p class="mb-2 text-truncate"><?php echo $lblUnpaid; ?></p>
                                      <h5 class="align-middle text-center"><?php echo $Outstanding; ?></h5>
                                  </div>
                              </div>
                              <div class="col-md-4 col-sm-12">
                                  <div class="mt-4">
                                      <p class="mb-2 text-truncate"><?php echo $lblDelinquentBalance; ?></p>
                                      <h5 class="align-middle text-center"><?php echo $DelinquentBalance; ?></h5>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
             </div>
        </div>
      </div>
    </div>
    <div class="row">
        <div class="col-md-12 mt-4">
          <div class="card">
              <div class="card-header pb-0 px-3">
                <h6 class="mb-0"><?php echo $lblBasicInfo; ?></h6>
              </div>
              <div class="card-body pt-4 p-3">
                <div class="row">
                  <div class="col-md-6">
                    <div class="table-responsive">
                      <table class="table align-items-center  mb-0" aria-describedby="basicinfo" aria-hidden="true">
                          <tbody>
                            <tr>
                                <th id="HeaderCustomerID" class="text-nowrap" scope="row"><span key="t-CustomerID"><?php echo $lblCustomerID; ?></span></th>
                                <td class="align-middle text-center"><?php echo $NationalID; ?></td>
                            </tr>
                            <tr>
                                <th id="HeaderNICExpiryDate" class="text-nowrap" scope="row"><span key="t-NICExpiryDate"><?php echo $lblNIDExDate; ?>
                                  </span>
                                </th>
                                <td class="align-middle text-center"><?php echo $NIDExpiryDate; ?></td>
                            </tr>
                            <tr>
                                <th id="HeaderOriginationDate" class="text-nowrap" scope="row"><span key="t-OrginationDate"><?php echo $lblOriginationDate; ?></span></th>
                                <td class="align-middle text-center"><?php echo $OriginationDate; ?></td>
                            </tr>
                          </tbody>
                      </table>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="table-responsive">
                      <table class="table align-items-center mb-0" aria-describedby="basicinfo" aria-hidden="true">
                          <tbody>
                            <tr>
                                <th id="HeaderOriginatorName" class="text-nowrap" scope="row"><span key="t-OrginatorName"><?php echo $lblOroginationName; ?></span></th>
                                <td class="align-middle text-center"><?php echo $OriginatorNameInEn; ?></td>
                            </tr>
                            <tr>
                                <th class="text-nowrap" scope="row"><span key="t-LegalOwner"><?php echo $lblLegalOwner; ?></span></th>
                                <td class="align-middle text-center"><?php echo $LegalOwner; ?></td>
                            </tr>
                           
                            <tr <?php echo ($DelinquentBalance > 0) ? 'class="text-danger"' : ''; ?> >
                                <th class="text-nowrap" scope="row"><span key="t-Delinquentbalance"><?php echo $lblDelinquentBalance; ?></span></th>
                                <td class="align-middle text-center"><?php echo $DelinquentBalance; ?></td>
                            </tr>
                         </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
          </div>
        </div>
    </div>
    <div class="row">
      <div class="col-md-12 mt-4">
            <div class="card">
                <div class="card-header pb-0 px-3">
                  <div class="row">
                      <div class="col-md-6">
                        <h6 class="mb-0"><?php echo $lblMortgageInfo; ?></h6>
                      </div>
                  </div>
                </div>
                <div class="card-body pt-4 p-3">
                  <div class="row">
                    <div class="col-md-6">
                      <div class="table-responsive">
                      <table class="table align-items-center mb-0" aria-describedby="mydesc" aria-hidden="true">
                          <tbody>
                            <tr>
                                <th class="text-nowrap" scope="row"><span key="t-NumberofPaidInstalments"><?php echo $lblNumberPaidInst; ?></span></th>
                                <td class="align-middle text-center"><?php echo $NumberOfPaidInstallments; ?></td>
                            </tr>
                            <tr>
                                <th class="text-nowrap" scope="row"><span key="t-RemainingTerm"><?php echo $lblRemaningTerm; ?></span></th>
                                <td class="align-middle text-center"><?php echo $RemainingTerm; ?></td>
                            </tr>
                            
                            <tr>
                                <th class="text-nowrap" scope="row"><span key="t-PaymentFrequency"><?php echo $lblPaymentFreq; ?></span></th>
                                <td class="align-middle text-center"><?php echo $PaymentFrequency; ?></td>
                            </tr>
                            <tr>
                                <th class="text-nowrap" scope="row"><span key="t-InstalmentDay"><?php echo $lblInstalDay; ?></span></th>
                                <td class="align-middle text-center"><?php echo $InstallmentDay; ?></td>
                            </tr>
                            <?php $productName = ($language == 'ar' ) ? $ContractTypeInAr : $ContractTypeInEn; ?>
                            <?php if($productName == "Ijarah" || $productName == "إيجارة") : ?>
                            <?php if($LastRepricingDate) : ?>
                              <tr>
                                  <th class="text-nowrap" scope="row"><span key="t-LastRepricingDate"><?php echo $lblLastRepDate; ?></span></th>
                                  <td class="align-middle text-center"><?php echo $LastRepricingDate; ?></td>
                              </tr>
                            <?php endif; ?>
                            <?php endif; ?>
                          </tbody>
                        </table>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="table-responsive">
                        <table class="table align-items-center mb-0" aria-describedby="mydesc" aria-hidden="true">
                            <tbody>
                              <tr>
                                  <th class="text-nowrap" scope="row"><span key="t-TotalPayableAmount"><?php echo $lblPayableAmt; ?></span></th>
                                  <td class="align-middle text-center"><?php echo $TotalPayableAmount; ?></td>
                              </tr>
                              <tr>
                                  <th class="text-nowrap" scope="row"><span key="t-DownPayment"><?php echo $lblDownPayment; ?></span></th>
                                  <td class="align-middle text-center"><?php echo $DownPayment; ?></td>
                              </tr>
                              <?php if($IsGuarantor !="N" && $IsGuarantor != NULL) : ?>
                                <tr>
                                    <th class="text-nowrap" scope="row"><span key="t-IsGuarantor"><?php echo $lblGuarantor; ?></span></th>
                                    <td class="align-middle text-center"><?php echo ($IsGuarantor !="N") ? 'Yes': ''; ?></td>
                                </tr>
                              <?php endif; ?>
                              <tr>
                                  <th class="text-nowrap" scope="row"><span key="t-CurrentMonthlyInstalment"><?php echo $lblCurrentMonthInsta; ?></span></th>
                                  <td class="align-middle text-center"><?php echo $CurrentMonthlyInstallment; ?></td>
                              </tr>
                              <?php if($FinalPayment) : ?>
                              <tr>
                                  <th class="text-nowrap" scope="row"><span key="t-FinalPayment"><?php echo $lblFinalPay; ?></span></th>
                                  <td class="align-middle text-center"><?php echo $FinalPayment ;?></td>
                              </tr>
                              <?php endif; ?>
                              <tr>
                                  <th class="text-nowrap" scope="row"><span key="t-TermCostRate"><?php echo $lblTermCostRate; ?></span></th>
                                  <td class="align-middle text-center"><?php echo $TermCostRate ;?></td>
                              </tr>
                            </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                  
                </div>
            </div>
          </div>
    </div>
   </div>
   <!-- End Tab 1 -->
   <!-- Start Tab 2 -->
   <div class="tab-pane" id="dashboard-tabs-simple">
      <div class="row">
          <div class="col-md-12">
            <div class="card mb-4">
                <div class="card-header pb-0">
                <div class="row">
                    <div class="col-6 d-flex align-items-center">
                      <!-- <h6 class="mb-0"><?php echo $lblTicketList; ?></h6> -->
                    </div>
                    <div class="col-12 text-end" >
                   <?php if($IsContractClosed == 1){ ?>
                       <button type="button" class="btn bg-gradient-dark mb-0 popupCreateTicket" data-bs-toggle="modal" data-bs-target="#exampleModal" title="<?php echo $isClosedcontract; ?>" style="pointer-events: all;" disabled>
                            <?php echo $lblCreateTicket; ?>             
                   <?php }else{ ?>
                    <button type="button" class="btn bg-gradient-dark mb-0 popupCreateTicket" data-bs-toggle="modal" data-bs-target="#exampleModal">
                            <?php echo $lblCreateTicket; ?> 
                    <?php } ?>
                    </div>
                  </div>
                </div>
                <div class="card-body p-4">
                  <div class="table-responsive">
                      <table class="table align-items-center table-striped table-nowrap mb-0" id="TicktList" aria-describedby="ticketlist" aria-hidden="true">
                        <thead>
                          <tr>
                            <th class="text-nowrap text-center" scope="row"><?php echo $lblTicketID; ?></th>
                            <th class="text-nowrap text-center" scope="row"><?php echo $lblCreatedDate; ?></th>
                            <th class="text-nowrap text-center" scope="row"><?php echo $lblTicketType; ?></th>
                            <th class="text-nowrap text-center" scope="row"><?php echo $lblCategory; ?></th>
                            <th class="text-nowrap text-center" scope="row"><?php echo $lblTicketStatus; ?></th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php 
                          if($ticketData) {  
                            foreach($ticketData as $ticketList) { ?>
                               <tr>
                                <td class="align-middle text-center">
                                  <p class="mb-1 mt-1"><?php echo $ticketList->TicketReferenceNo; ?></p>
                                </td>
                                <td class="align-middle text-center">
                                  <p class="mb-1"><span style="display:none;"><?php echo $ticketList->CreatedOn; ?></span><?php echo date('d-m-Y', strtotime($ticketList->CreatedOn)); ?></p>
                                </td>
                                <td class="align-middle text-center">
                                  <p class="mb-1"><?php echo ($language == 'ar' ) ? $ticketList->TicketTypeNameInAr : $ticketList->TicketTypeName; ?></p>
                                </td>
                                <td class="align-middle text-center">
                                  <p class="mb-1"><?php echo ($language == 'ar' ) ? $ticketList->TicketCategoryNameInAr : $ticketList->TicketCategoryNameInEn; ?></p>
                                </td>
                               
                                <td class="align-middle text-center mb-1">
                                  <?php if($ticketList->TicketStatusName == 'Created'){
                                          $bgColor = 'badge-soft-info';
                                  }elseif($ticketList->TicketStatusName == 'Closed'){
                                          $bgColor = 'badge-soft-success';
                                  }else{
                                          $bgColor = 'badge-soft-warning';
                                  } ?>
                                  <span class="badge badge-sm <?php echo $bgColor; ?>"><?php echo ($language == 'ar' ) ? $ticketList->TicketStatusNameInAr : $ticketList->TicketStatusName; ?></span>
                                </td>
                              </tr>
                          <?php } } ?>
                          
                        </tbody>
                      </table>
                  </div>        
                </div>
            </div>
          </div>
      </div>

      <!-- Modal -->
      <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel"><?php echo $lblCreateTicket; ?></h5>
              <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <form role="form" id="createTicket" method="POST" action="" enctype="multipart/form-data">
              <div class="modal-body">
                  <div class="form-group">
                  <input type="hidden" id="resetValidation" value="0">
                      <label for="ticketType"><?php echo $lblTicketType; ?> <span class="text-danger">*</span></label>
                      <select class="form-control" id="ticketType" name="ticketType">
                        <option value=""><?php echo $lblsSelectTicketType; ?></option>
                        <?php if($ticketType->TicketType){?>
                          <?php foreach($ticketType->TicketType as $list) { 
                            $ticketTypeLang = (pll_current_language() == 'en') ? $list->TicketTypeNameInEn : $list->TicketTypeNameInAr;
                          ?>
                              <option value="<?php echo $list->ID; ?>" data-id="<?php echo $list->ID; ?>"><?php echo $ticketTypeLang; ?></option>
                          <?php } ?>
                        <?php } ?>
                    </select>
                    </div>
                    <div class="form-group">
                      <label for="ticket_category"><?php echo $lblCategory; ?> <span class="text-danger">*</span> <span class="spinner-border spinner-border-sm process-category" style="display:none;" role="status" aria-hidden="true"></span></label>
                      <select class="form-control" id="ticket_category" name="ticket_category">
                        <option value=""><?php echo $lblSelectCategory; ?></option>
                    </select>
                    </div>
                    <div class="form-group toggle-subcategory">
                      <label for="ticket_subcategory"><?php echo $lblSubCategory; ?> <span class="text-danger">*</span> <span class="spinner-border spinner-border-sm process-subcategory" style="display:none;" role="status" aria-hidden="true"></span></label>
                      <select class="form-control" id="ticket_subcategory" name="ticket_subcategory">
                        <option value=""><?php echo $lblSelectSubCategory; ?></option>
                    </select>
                    </div>
                    <div class="form-group">
                      <label for="ticket_description"><?php echo $lblTicketDesciption; ?> <span class="text-danger">*</span></label>
                      <textarea class="form-control" id="ticket_description" rows="3" name="ticket_description"></textarea>
                    </div>
                    <div class="form-group">
                      <label for="ticket_attachments"><?php echo $lblTicketattachments; ?></label>
                      <input type="file" class="form-control" id="ticket_attachments" name="ticket_attachments">
                    </div>
                    <div class="row ticketResponse text-center" style="display:none;">

                    </div>
                
              </div>
              <div class="modal-footer">
                <button type="button" class="btn bg-secondary text-white" data-bs-dismiss="modal"><?php echo $btnClose; ?></button>
                <button type="submit" class="btn bg-gradient-info" id="btnCreateTicket" disabled><?php echo $btnSubmit; ?></button>
              </div>
            </form>
          </div>
        </div>
      </div>
   </div>
   <!-- End Tab 2 -->
   <!-- Start Tab 3 -->
  <div class="tab-pane" id="dashboard-tabs-payment-schedule">
    <div class="row">
        <div class="col-md-12">
          <div class="card mb-4">
              
                <div class="card-header pb-0"> 
                  <div class="col-12 text-end">           
                      <button class="btn bg-gradient-dark mb-0 smspaymentschedule" data-bs-toggle="modal"  style="text-align:right;"><?php echo $lblSmsPaymentSchedule; ?></button>
                  </div>
                </div>
                  <!-- Modal -->
                <div class="modal fade" id="smsModal" tabindex="-1" role="dialog" aria-labelledby="smsModalLabel" aria-hidden="true">
                  <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                      <div style="text-align: right;">
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true" style = 'top: -0.5em; position: relative;'>&times;</span>
                        </button>
                      </div>
                      <div class="modal-body">
                            <div class="form-group" style = "margin-bottom: 0rem; text-align: center; margin-top: -2em;">
                                <label><?php echo $popupSmsPaymentSchedule; ?></label>
                            </div>               
                        </div>
                    </div>
                  </div>
                </div>
                <div class="modal fade" id="sentSmsModal" tabindex="-1" role="dialog" aria-labelledby="smsModalLabel" aria-hidden="true">
                  <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                      <div style="text-align: right;">
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true" style = 'top: -0.5em; position: relative;'>&times;</span>
                        </button>
                      </div>
                      <div class="modal-body">
                            <div class="form-group" style = "margin-bottom: 0rem; text-align: center; margin-top: -2em;">
                                <label><?php echo $popupSentSmsPaymentSchedule;?></label>
                            </div>               
                        </div>
                    </div>
                  </div>
                </div>

              <div class="card-body p-4">
                <div class="table-responsive">
                    <table class="table align-items-center table-striped table-nowrap mb-0" id="PaymentScheduleList" aria-describedby="ticketlist" aria-hidden="true">
                      <thead>
                        <tr>
                          <th class="text-nowrap text-center" scope="row"><?php echo $lblNumberPaidInst; ?></th>
                          <th class="text-nowrap text-center" scope="row"><?php echo $lblInstallDate; ?></th>
                          <th class="text-nowrap text-center" scope="row"><?php echo $lblInstallmentAmount; ?></th>
                          <th class="text-nowrap text-center" scope="row"><?php echo $lblTermCostAmount; ?></th>
                          <th class="text-nowrap text-center" scope="row"><?php echo $lblPrincipalAmount; ?></th>
                          <th class="text-nowrap text-center" scope="row"><?php echo $lblRemainingAmount; ?></th>
                          <th class="text-nowrap text-center" scope="row"><?php echo $lblIsPaid; ?></th>
                         
                          <?php if($REDFSubsidized == true){  ?>
                          <th class="text-nowrap text-center" scope="row"><?php echo $lblSubsidyStatus; ?></th>
                          <th class="text-nowrap text-center" scope="row"><?php echo $lblSubsidyAmount; ?></th>

                          <?php } ?>

                        </tr>
                      </thead>
                      <tbody>
                        <?php 
                        if($payment_schedules) {  
                          foreach($payment_schedules as $payment_schedule) { ?>
                              <tr>
                                <td class="align-middle text-center">
                                  <p class="mb-1 mt-1"><?php echo $payment_schedule->InstallmentNo; ?></p>
                                </td>
                                <td class="align-middle text-center">
                                  <p class="mb-1"><?php echo date('d-m-Y', strtotime($payment_schedule->InstallmentDate)); ?></p>
                                </td>
                                <td class="align-middle text-center">
                                  <p class="mb-1"><?php echo number_format($payment_schedule->InstallmentAmount, 2); ?></p>
                                </td>
                                <td class="align-middle text-center">
                                  <p class="mb-1"><?php echo number_format($payment_schedule->TermCost,2); ?></p>
                                </td>
                                <td class="align-middle text-center">
                                  <p class="mb-1"><?php echo number_format($payment_schedule->PrincipalAmount, 2); ?></p>
                                </td>
                                <td class="align-middle text-center">
                                  <p class="mb-1"><?php echo number_format($payment_schedule->RemainingAmount, 2); ?></p>
                                </td>
                                <td class="align-middle text-center">
                                  <p class="mb-1"> 
                                  <?php if($language == 'ar'){
                                  echo ($payment_schedule->IsPaid) ?  "مدفوع" :  "غير مدفوع" ;
                                  }else{
                                    echo ($payment_schedule->IsPaid) ?  "Paid" :  "Not Paid" ;
                                  }?></p>
                                </td>
                                <?php if($REDFSubsidized == true){?>
                                <td class="align-middle text-center">
                                  <!-- <p class="mb-1"><?php echo $payment_schedule->REDFSubsidyStatus; ?></p> -->
                                  <p class="mb-1"><i class="ni <?php echo ( $payment_schedule->REDFSubsidyStatus == 'success') ? 'ni-check-bold text-success' : 'ni-fat-remove text-warning' ;?>" aria-hidden="true"></i></p>
                                </td>
                                <td class="align-middle text-center">
                                  <p class="mb-1"><?php echo number_format($payment_schedule->REDFSubsidyAmount, 2) ; ?></p>
                                </td>
                                <?php } ?>
                             </tr>
                        <?php } } ?>
                        
                      </tbody>
                    </table>
                </div>        
              </div>
          </div>
        </div>
    </div>
  </div>
  <!-- End Tab 3 -->
   <!-- Start Tab 4 -->
   <div class="tab-pane" id="dashboard-tabs-payment-detail">
      <div class="row">
          <div class="col-md-12">
            <div class="card mb-4">
               <div class="card-body p-4">
                  <div class="table-responsive">
                      <table class="table align-items-center table-striped table-nowrap mb-0" id="PaymentDetailsList" aria-describedby="ticketlist" aria-hidden="true">
                        <thead>
                          <tr>
                            <th class="text-nowrap text-center" scope="row"><?php echo $lblTransactionID; ?></th>
                            <th class="text-nowrap text-center" scope="row"><?php echo $lblTransactionSource; ?></th>
                            <th class="text-nowrap text-center" scope="row"><?php echo $lblTransactionAmount; ?></th>
                            <th class="text-nowrap text-center" scope="row"><?php echo $lblTransactionDate; ?></th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php 
                          if($payment_details) {  
                            foreach($payment_details as $payment_detail) { ?>
                               <tr>
                                <td class="align-middle text-center">
                                  <p class="mb-1 mt-1"><?php echo $payment_detail->TransactionID; ?></p>
                                </td>
                                <td class="align-middle text-center">
                                  <p class="mb-1"><?php echo  $payment_detail->TransactionCategory; ?></p>
                                </td>
                                <td class="align-middle text-center">
                                  <p class="mb-1"><?php echo  number_format($payment_detail->TransactionAmount, 2); ?></p>
                                </td>
                                <td class="align-middle text-center">
                                  <p class="mb-1"><?php echo  date('d-m-Y', strtotime($payment_detail->TransactionDate)); ?></p>
                                </td>
                              </tr>
                          <?php } } ?>
                          
                        </tbody>
                      </table>
                  </div>        
                </div>
            </div>
          </div>
      </div>
  </div>
   <!-- End Tab 4 -->
  
</div>
      
</div>
<?php require_once plugin_dir_path( dirname( __FILE__ ) ) . 'template/comman/footer.php'; ?>

</body>
</html>