/** Custom JS Scripts Here */
jQuery(document).ready(function() { 
    window.$ = jQuery;
    initializeGlobelCalls.init(); 
    
}); 
function onlyNumber (obj) {
    var $this = jQuery(obj);
        $this.val($this.val().replace(/\D/g, ''));
}

var initializeGlobelCalls = function () {

    var general_setting  = function () {
        $('#exampleModal').on('hidden.bs.modal', function () {
            $('#exampleModal form#createTicket')[0].reset();
        });
        var chartWidth = ($('body').attr('lang') == 'en') ? '371' : '350';
        if($('#chart').length) {
            var options = {
                series: charData,
                chart: {
                width: chartWidth,
                type: 'donut',
              },
              dataLabels: {
                enabled: false
              },
              colors:['#60c3b5', '#818497', '#f47575'],
              responsive: [{
                breakpoint: 480,
                options: {
                  chart: {
                    width: 200
                  },
                  legend: {
                    show: false
                  }
                }
              }],
              legend: {
                position: 'right',
                offsetY: 0,
                height: 230,
              },
              labels: [lblPaid, lblUnpaid, lblDelinquentBalance]
              };
      
              var chart = new ApexCharts(document.querySelector("#chart"), options);
              chart.render();
              
            
        }
          
        if ($(window).width() < 768) {
            $('.login-page').removeClass('login-background');
         }
         else {
            $('.login-page').addClass('login-background');
         }
        $('[data-bs-toggle="tooltip"]').tooltip();
        var ticketstatus = getUrlParameter('ticketstatus');
        if(ticketstatus == 'success') {
            $('a[href="#dashboard-tabs-simple"]').tab('show');
        }
        if($(".datepicker").length){
            $(".datepicker").flatpickr({
                dateFormat: "d-m-Y",
            });
        }
        if($(".dashboard-conatiner").length){
            $.sessionTimeout({
                warnAfter: session_timeout_warining_time,
                redirAfter: session_timeout_redirect_time,
                countdownMessage: lblRedirectTime
            });
         }
        if($("#recaptchaLogin").length){
            setTimeout(function(){
                $('#recaptchaLogin div').css('width', '100%');
            }, 500);
        }
        if( $('.digit-group').length){
            $('.digit-group').find('input').each(function() {
                $(this).attr('maxlength', 1);
                $(this).on('keyup', function(e) {
                    var parent = $($(this).parent().parent());
                    
                    if(e.keyCode === 8 || e.keyCode === 37) {
                        var prev = parent.find('input#' + $(this).data('previous'));
                        
                        if(prev.length) {
                            $(prev).select();
                        }
                    } else if((e.keyCode >= 48 && e.keyCode <= 57) || (e.keyCode >= 65 && e.keyCode <= 90) || (e.keyCode >= 96 && e.keyCode <= 105) || e.keyCode === 39) {
                        var next = parent.find('input#' + $(this).data('next'));
                        
                        if(next.length) {
                            $(next).select();
                        } else {
                            $('#btnSignIn').click();
                        }
                    }
                });
            });
            
        }
        

        if($("#TicktList").length){
            $('#TicktList').DataTable({
                "info":     false,
                "searching": false,
                "bLengthChange": false,
                "pageLength": 10,
                "order": [[ 1, "desc" ]],
                oLanguage: {
                    oPaginate: {
                        sNext: '<span class="pagination-fa"><i class="fa fa-chevron-right" ></i></span>',
                        sPrevious: '<span class="pagination-fa"><i class="fa fa-chevron-left" ></i></span>'
                    },
                    "sEmptyTable": "No ticket found"
                }, 
                'aoColumnDefs': [{
                    'bSortable': false,
                    'aTargets': [-1, -2, -3, -5] /* 1st one, start by the right */
                }]
            });
        }

        if($("#PaymentDetailsList").length){
            $('#PaymentDetailsList').DataTable({
                "info":     false,
                "searching": false,
                "bLengthChange": false,
                "pageLength": 10,
                "order": [[ 0, "desc" ]],
                oLanguage: {
                    oPaginate: {
                        sNext: '<span class="pagination-fa"><i class="fa fa-chevron-right" ></i></span>',
                        sPrevious: '<span class="pagination-fa"><i class="fa fa-chevron-left" ></i></span>'
                    },
                    "sEmptyTable": "No payment details found"
                }, 
                'aoColumnDefs': [{
                    'bSortable': false,
                    'aTargets': [-1, -2, -3] /* 1st one, start by the right */
                }]
            });
        }

        if($("#PaymentScheduleList").length){
            $('#PaymentScheduleList').DataTable({
                "info":     false,
                "searching": false,
                "bLengthChange": false,
                "pageLength": 10,
                "order": [[ 0, "asc" ], [ 1, "asc" ]],
                oLanguage: {
                    oPaginate: {
                        sNext: '<span class="pagination-fa"><i class="fa fa-chevron-right" ></i></span>',
                        sPrevious: '<span class="pagination-fa"><i class="fa fa-chevron-left" ></i></span>'
                    },
                    "sEmptyTable": "No payment schedule details found"
                }, 
                'aoColumnDefs': [{
                    'bSortable': false,
                    'aTargets': [-1, -2, -3, -4, -5, -6] /* 1st one, start by the right */
                }]
            });
        }
        
        
    }
    var verifyNationalID  = function () {
        if($('#verifyNICForm').length){
            $('#verifyNICForm').bootstrapValidator({ 
                message: 'This value is not valid',
                fields: {
                    nationalID: {
                        validators: {
                            callback: {
                                message: errorNationalIDValid,
                                callback: function (value, validator, $field) {
                                    if (value.length == '') {
                                        return {
                                            valid: false,
                                            message: errorNationalIDReq
                                        };
                                    }
                                    if (value.length < 10) {
                                        return {
                                            valid: false,
                                            message: errorNationalIDTenDigit
                                        };
                                    }
                                    if(value.length == 10){
                                        var isValid = ValidateNICNumber($('#nationalID').val());
                                        if(isValid == false){
                                            return {
                                                valid: false,
                                                message: errorNationalIDValid
                                            };
                                        }
                                        
                                    }
                                    return true;
                                }
                            }
                        }
                    },
                    originationDate: {
                        validators: {
                            // notEmpty: {
                            //     // message: erroOriginsationDate
                            //     // message: errorMobileNumber
                               
                            // }

                            // callback: {
                               
                                // callback: function () {
                                //     return false;
                                // }
                            // }
                        }
                    }
                }
            }).on('success.form.bv', function (e) {
                e.preventDefault();
                var captcha_response = $('#login_captcha_reponse_key').val();
                if(captcha_response){
                    var btnText = $('#btnVerifyme').text();
                    $('#btnVerifyme').html('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>');
                    // $('#captchError').hide();
                    var formdata = $('#verifyNICForm').serialize();
                    $.ajax({
                        url: ajaxurl,
                        data: {
                            action: 'validateNationalID',
                            NICdata: formdata
                        },
                        type: "POST",
                        success: function(response) {
                            var data = jQuery.parseJSON(response);
                            if(data.status == "false") {
                                grecaptcha.reset();
                                
                                $('#login_captcha_reponse_key').val('');
                               // $('#btnVerifyme').html(btnText);
                                $('.nationalIDVaildError').show();
                                $('.nationalIDVaildError').html('<span class="error">'+data.msg+'</span>');
                                setTimeout(function(){ $('.nationalIDVaildError').hide(); }, 4000);
                            }else{
                                SetCodeVerificationTimer();
                                $('#verifyNICForm').hide();
                                $('#OTPForm').show();
                                $('.hiddenAcc_no').val(data.account_number);
                                $('.hiddenNationalID').val(data.nationalID);
                                $('.hiddenoriginationDate').val(data.originationDate);
                                
                                
                            }
                            
                        }
                    });
                    
                    
                    
                }else{
                    $('#captchError').show();
                }
                
            });
        }

        if($('#verifyKYCForm').length){
            $('#verifyKYCForm').bootstrapValidator({ 
                message: 'This value is not valid',
                fields: {
                    nationalID: {
                        validators: {
                            callback: {
                                message: errorKYCNationalIDValid,
                                callback: function (value, validator, $field) {
                                    if (value.length == '') {
                                        return {
                                            valid: false,
                                            message: errorNationalIDReq
                                        };
                                    }
                                    if (value.length < 10) {
                                        return {
                                            valid: false,
                                            message: errorNationalIDTenDigit
                                        };
                                    }
                                    if(value.length == 10){
                                        var isValid = ValidateNICNumber($('#nationalID').val());
                                        if(isValid == false){
                                            return {
                                                valid: false,
                                                message: errorKYCNationalIDValid
                                            };
                                        }
                                        
                                    }
                                    return true;
                                }
                            }
                        }
                    },
                    MobileNumber: {
                        validators: {
                            // notEmpty: {
                            //     message: errorMobileNumber
                            // }
                            callback: {
                                message: errorKYCNationalIDValid,
                                callback: function (value, validator, $field) {
                                    if (value.length == '') {
                                        return {
                                            valid: false,
                                            message: errorMobileNumber
                                        };
                                    }
                                    if (value.length < 10) {
                                        return {
                                            valid: false,
                                            message: errorMobileNumber
                                        };
                                    }
                                    if(value.length == 10){
                                        var isValid = $('#lblMobileNumber').val();
                                        if(isValid == false){
                                            return {
                                                valid: false,
                                                message: errorMobileNumber
                                            };
                                        }
                                        
                                    }
                                    return true;
                                }
                            }
                        }
                    }
                }
            }).on('success.form.bv', function (e) {
                e.preventDefault();
                var captcha_response = $('#login_captcha_reponse_key').val();
                if(captcha_response){
                    var btnText = $('#kycbtnVerifyme').text();
                    $('#kycbtnVerifyme').html('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>');
                    // $('#captchError').hide();
                    var formdata = $('#verifyKYCForm').serialize();
                    $.ajax({
                        url: ajaxurl,
                        data: {
                            action: 'validateKYCNationalID',
                            NICdata: formdata
                        },
                        type: "POST",
                        success: function(response) {
                            var data = jQuery.parseJSON(response);
                            if(data.status == "false") {
                                grecaptcha.reset();
                                
                                $('#login_captcha_reponse_key').val('');
                                $('#kycbtnVerifyme').html(btnText);
                                $('.nationalIDVaildError').show();
                                $('.nationalIDVaildError').html('<span class="error">'+data.msg+'</span>');
                                setTimeout(function(){ $('.nationalIDVaildError').hide(); }, 4000);
                            }else{
                                SetCodeVerificationTimer();
                                $('#verifyKYCForm').hide();
                                $('#KycOTPForm').show();
                                $('.hiddenAcc_no').val(data.account_number);
                                $('.hiddenNationalID').val(data.nationalID);
                                $('.hiddenMobileNumber').val(data.MobileNumber);
                            }
                        }
                    });
             
                }else{
                    $('#captchError').show();
                }
                
            });
        }
        
    }
    var ValidateNICNumber = function (number) {
        if (number.length != 10) {
            return false;
        } else {
            var nSum = 0;
            var lastDigit = "";
            var nDigit = 0;
            for (i = 0; i < 9; i++) {
                nDigit = parseInt(number[i]);
               // console.log(i);
                if (i % 2 == 0) // If Odd position digit double it
                {
                    if ((nDigit * 2) > 9) // If two digits number then add each digit
                        nSum = parseInt(nSum) + parseInt((nDigit * 2).toString().substring(0, 1)) + parseInt((nDigit * 2).toString().substring(1, 2));                  
                    else
                        nSum = parseInt(nSum) + (parseInt(nDigit) * 2);
                } else
                    nSum += nDigit;
            }
            console.log(nSum);
            var lastDigit = (nSum % 10).toString();
            console.log(lastDigit);
            if (lastDigit != "0") {
                lastDigit = (10 - (lastDigit)).toString();
            }
    
            if (lastDigit == number.substring(10, 9)){
                return true;
            } else {
                return false;
            }
        }
    }
    var verifyOTP = function (){
        $('#btnSignIn').click(function() {
            var eduInput = document.getElementsByName('OTP[]');
            for (i=0; i<eduInput.length; i++){
                if (eduInput[i].value == ""){
                   $('#OTPError').show();
                   $('#OTPError').html('<span class="error">'+errorOTPEnter+'</span>');	 
                    return false;
                }
                
            }
            // for (i=0; i<eduInput.length;i++ ){
            //     console.log("For Loop");
                // if (eduInput[i].value == 0){
                //     console.log("if condition");
                    // $('#btnSignIn').removeAttr('disabled', 'disabled');
                // }
            // }

            var formdata = $('#OTPForm').serialize();
            $('#OTPError').hide();
            var btnText = $('#btnSignIn').attr('data-btnName');
            $('#btnSignIn').html('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>');
            $('#btnSignIn').attr('disabled', 'disabled');
            $.ajax({
                url: ajaxurl,
                data: {
                    action: 'verifyOTP',
                    OTPdata: formdata
                },
                type: "POST",
                success: function(response) {
                    var data = jQuery.parseJSON(response);
                    $('#btnSignIn').html(btnText);
                    if(data.status == "true") {
                        $('#OTPError').hide();
                        localStorage.setItem('logid', data.logid);
                        window.location.href = $('.dashboard_url').val();
                    }else{
                        $('#btnSignIn').removeAttr('disabled', 'disabled');
                        $('#OTPError').show();
                        $('#OTPError').html('<span class="error">'+data.msg+'</span>');
                    } 
                }
            });
            
        });

        $('#kycbtnSignIn').click(function() {
            var eduInput = document.getElementsByName('KYCOTP[]');
            for (i=0; i<eduInput.length; i++){
                if (eduInput[i].value == ""){
                   $('#OTPError').show();
                   $('#OTPError').html('<span class="error">'+errorOTPEnter+'</span>');	 
                    return false;
                }
            }
            var formdata = $('#KycOTPForm').serialize();
            $('#OTPError').hide();
            var btnText = $('#kycbtnSignIn').attr('data-btnName');
            $('#kycbtnSignIn').html('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>');
            $('#kycbtnSignIn').attr('disabled', 'disabled');
            $.ajax({
                url: ajaxurl,
                data: {
                    action: 'KYCverifyOTP',
                    KYCOTPdata: formdata
                },
                type: "POST",
                success: function(response) {
                    var data = jQuery.parseJSON(response);
                    $('#kycbtnSignIn').html(btnText);
                     if(data.status == "true") {
                        $('#OTPError').hide();
                        window.location.href = $('.dashboard_url').val();
                    }else{
                        $('#kycbtnSignIn').removeAttr('disabled', 'disabled');
                        $('#OTPError').show();
                        $('#OTPError').html('<span class="error">'+data.msg+'</span>');
                    } 
                }
            });
            
        });
    }
    var resendOTP = function (){
        $('#btnResendOtp').click(function() {
            $('#OTPError').hide();
            var btnResendText = $('#btnResendOtp').text();
            $('#btnResendOtp').attr('disabled', 'disabled');
            $('#btnResendOtp').html('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>');
            $.ajax({
                url: ajaxurl,
                data: {
                    action: 'resendOTP',
                    NationalID: $('.hiddenNationalID').val(),
                    OriginationDate : $('.hiddenoriginationDate').val()
                },
                type: "POST",
                success: function(response) {
                    var data = jQuery.parseJSON(response);
                    $('#btnResendOtp').html(btnResendText);
                    if(data.status == "true") {
                        SetCodeVerificationTimer();
                        $('#btnSignIn').removeAttr('disabled', 'disabled');
                    }else{
                        $('#OTPError').show();
                        $('#OTPError').html('<span class="error">'+data.msg+'</span>');
                    } 
                }
            });
        });
    }
    var resendKYCOTP = function (){
        $('#btnResendKycOtp').click(function() {
            $('#OTPError').hide();
            var btnResendText = $('#btnResendKycOtp').text();
            $('#btnResendKycOtp').attr('disabled', 'disabled');
            $('#btnResendKycOtp').html('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>');
            $.ajax({
                url: ajaxurl,
                data: {
                    action: 'resendKYCOTP',
                    NationalID: $('.hiddenNationalID').val(),
                    MobileNumber : $('.hiddenMobileNumber').val()
                },
                type: "POST",
                success: function(response) {
                    var data = jQuery.parseJSON(response);
                    $('#btnResendKycOtp').html(btnResendText);
                    if(data.status == "true") {
                        SetCodeVerificationTimer();
                        $('#kycbtnSignIn').removeAttr('disabled', 'disabled');
                    }else{
                        $('#OTPError').show();
                        $('#OTPError').html('<span class="error">'+data.msg+'</span>');
                    } 
                }
            });
        });
    }
    

    var createTicket = function() {
        if($("#ticketType").length){
            $('#ticketType').change(function() {
                $('.process-category').show();
                $("#ticket_subcategory").html('<option>'+lblSelectSubCategory+'</option>');
                var type_id = $(this).find(':selected').data('id');
                $.ajax({
                    url: ajaxurl,
                    data: {
                        action: 'get_ticket_category',
                        type_id: type_id,
                        lang: logoutLang
                    },
                    type: "POST",
                    success: function(response) {
                        $('.process-category').hide();
                        var data = jQuery.parseJSON(response);
                     //   console.log(data);
                        $("#ticket_category").html(data);
                    }  
                }); 
            });
        }
        if($("#ticket_category").length){
            $("#ticket_category").change(function() {
                $('.process-subcategory').show();
                var cat_id = $(this).find(':selected').data('id');
                $.ajax({
                    url: ajaxurl,
                    data: {
                        action: 'get_ticket_subcategory',
                        cat_id: cat_id,
                        lang: logoutLang
                    },
                    type: "POST",
                    success: function(response) {
                        $('.process-subcategory').hide();
                        var data = jQuery.parseJSON(response);
                        $("#ticket_subcategory").html(data);
                        if(data.status == 'error'){
                            $('.toggle-subcategory').hide();
                        }else{
                            $('.toggle-subcategory').show();
                        }
                    }  
                }); 
            });
        }
        if($('#createTicket').length){
            $('#createTicket').bootstrapValidator({ 
                message: 'This value is not valid',
                fields: {
                    ticketType: {
                        validators: {
                            notEmpty: {
                                message: errorPlsSelectTicket
                            }
                        }
                    },
                    ticket_category: { 
                        validators: {
                            notEmpty: {
                                message: errorPlsSelectTicketCat
                            },
                            callback: {
                                message: errorPlsSelectTicketCat,
                                callback: function (value) {
                                    if (value == 'Select category') {
                                        return {
                                            valid: false,
                                            message: errorPlsSelectTicketCat
                                        };
                                    }
                                    return true;
                                }
                            }
                        }
                    },
                    ticket_subcategory: {
                        validators: {
                            notEmpty: {
                                message: errorPlsSelectTicketSubCat
                            },
                            callback: {
                                message: errorPlsSelectTicketSubCat,
                                callback: function (value) {
                                    if (value == 'Select Subcategory') {
                                        return {
                                            valid: false,
                                            message: errorPlsSelectTicketSubCat
                                        };
                                    }
                                    return true;
                                }
                            }
                        }
                    },
                    ticket_description: {
                        validators: {
                            notEmpty: {
                                message: errorPlsaddTicketDesc
                            }
                            // stringLength: {
                            //     min:20,
                            //     max: ticket_description_length,
                            //     message: errorTicketDescLength
                            // }
                        }
                    },
                    ticket_attachments: {
                        validators: {
                            file: {
                                message: errorAttachmentFileSize, 
                                extension: "pdf",
                                maxSize: 20 * 1024 * 1024
                            }
                        }
                    }
                }     
            }).on('success.form.bv', function (e) {
                e.preventDefault();
                var form = new FormData();

                var files = $("#ticket_attachments").get(0).files;
                console.log($("#ticket_category").val());
                form.append("ticketType", $("#ticketType").val());
                form.append("ticket_category", $("#ticket_category").val());
                form.append("ticket_subcategory", $("#ticket_subcategory").val());
                form.append("ticket_description", $("#ticket_description").val());
                form.append("file", $("#ticket_attachments")[0].files[0]);
                form.append("action", 'createTicket');
                            
                var btnTicketText = $('#btnCreateTicket').text();
                $('#btnCreateTicket').html('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>');
                $.ajax({
                    type: 'POST',
                    url: ajaxurl,
                    enctype: 'multipart/form-data',
                    async: false, 
                    cache: false, 
                    contentType: false, 
                    processData: false,
                    data:form,                 
                    // data: {
                    //     action: 'createTicket',
                    //     ticketData: form
                    // },     
                    success: function(response) {
                        $('#btnCreateTicket').html(btnTicketText);
                        var data = jQuery.parseJSON(response);
                        $('.ticketResponse').show();
                        if(data.status == "false") {
                            $('.ticketResponse').html('<div class="alert alert-danger font-weight-bold  text-white ">'+data.msg+'</div>');
                        }else{
                            $('.ticketResponse').html('<div class="alert alert-success font-weight-bold  text-white ">'+data.msg+'</div>');
                            setTimeout(function(){
                                window.location.replace(home_page_url+'?ticketstatus=success');
                            }, 1000);
                        }
                        setTimeout(function(){
                            $('.ticketResponse').hide();
                        }, 1000);
                        
                    }
                });
            });
        }
    }
    
        // Open the modal 
        $('#exampleModal').on('hide.bs.modal', function (e) {
            clearValidationAndReinitialize();
        });
        
        function clearValidationAndReinitialize() { 
            // Clear validation messages 
            $('#createTicket').bootstrapValidator('resetForm', true); 
            $("#btnCreateTicket").attr("disabled", "disabled");
        }
        
    var getUrlParameter = function (sParam) {
        var sPageURL = window.location.search.substring(1),
            sURLVariables = sPageURL.split('&'),
            sParameterName,
            i;
    
        for (i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('=');
    
            if (sParameterName[0] === sParam) {
                return typeof sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
            }
        }
        return false;
    };
    var SetCodeVerificationTimer = function () {
        if($("#m_timer").length){
            $('#m_timer').countdowntimer({
                minutes: 1,
                seconds:59,
                timeUp: CodeVerificationTimeisUP
            });
            $("#m_timer").css("color", "#54a84e");
        }
    }

    var CodeVerificationTimeisUP =  function () {
        $("#m_timer").css("color", "#f98005");
        $("#btnSignIn").attr("disabled", "disabled");
        $("#kycbtnSignIn").attr("disabled", "disabled");
        $("#btnResendOtp").removeAttr("disabled");
        $("#btnResendKycOtp").removeAttr("disabled");
    }
    var viewDetailsbyAccountNo = function (){
        $('.btnViewDetails').click(function(){
            $(this).html('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>');
            $(".btnViewDetails").attr('disabled', 'disabled');
            $.ajax({
                url: ajaxurl,
                data: {
                    action: 'viewDetailsAccountNumber',
                    accno : $(this).data('acc'),
                    lang: logoutLang
                },
                type: "POST",
                success: function(result) {
                    
                    window.location.href = result;
                    
                }
            });    
        });
    }
    var logout = function (){
        $('.btnLogout').click(function(){

            $.ajax({
                url: ajaxurl,
                data: {
                    action: 'logout',
                    lang : $(this).attr('langAttr')
                },
                type: "POST",
                success: function(result) {
                    
                    window.location.href = result;
                    
                }
            });    
        });
    }
    var userlogs = function (){
        $('.btnLogout, #session-timeout-dialog-logout').click(function(){ 
            var logid = localStorage.getItem('logid'); 
            var userdata = false;
                $.ajax({
                        url: ajaxurl,
                        data: {
                            action: 'add_user_logs',
                            SystemLogout: userdata,
                            logid:logid
                        },
                        type: "POST"
                        // success: function(result) {
                        //     alert("Normal Logout");
                        // }
                    });         
            });
        setInterval(function () {
           var logid = localStorage.getItem('logid'); 
           var user_acc_no = $(".user_acc_no").val();
           if(user_acc_no == undefined){
                var userdata = true;
                $.ajax({
                    url: ajaxurl,
                    data: {
                        action: 'add_user_logs',
                        SystemLogout: userdata,
                        logid:logid
                    },
                    type: "POST"
                    // success: function (data) {
                    //     console.log("session Timeout");
                    // }
                });
             }
        }, 40000);
    }
    var smspaymentschedule = function (){
        $('.smspaymentschedule').click(function(){ 
           var accountNo =  $(".user_acc_no").val();
               $.ajax({
                        url: ajaxurl,
                        data: {
                            action: 'sms_payment_schedule',
                            accountNo: accountNo,
                        },
                        type: "POST",
                        success: function(response) {
                            var data = jQuery.parseJSON(response);
                            console.log(data);
                            if(data.status == 200){
                                $("#smsModal").modal("show");
                                $(".smspaymentschedule").css("background", "rgba(80,165,241,.18)");
                                $(".smspaymentschedule").css("color", "#344767");
                            }else{
                                $("#sentSmsModal").modal("show");
                            }
                        }
                    });         
            });
    }
    var usersactivitylog = function (){
        $( document ).ready(function() {
            var logid = localStorage.getItem('logid'); 
            var accountno = $(".user_acc_no").val();
            var tabname ='Account Information';
            if(accountno != undefined){
                $.ajax({
                        url: ajaxurl,
                        data: {
                            action: 'users_activity_log',
                            accountno: accountno,
                            tabname:tabname,
                            logid:logid
                        },
                        type: "POST"
                    }); 
                }
        });
        $('.profile-tabs-simple').click(function(){ 
            var logid = localStorage.getItem('logid'); 
            var accountno = $(".user_acc_no").val();
            var tabname ='Account Information';
                $.ajax({
                        url: ajaxurl,
                        data: {
                            action: 'users_activity_log',
                            accountno: accountno,
                            tabname:tabname,
                            logid:logid
                        },
                        type: "POST"
                    }); 

         }); 
         $('.dashboard-tabs-simple').click(function(){ 

            var logid = localStorage.getItem('logid'); 
            var accountno = $(".user_acc_no").val();
            var tabname = 'Ticket Information';
                $.ajax({
                        url: ajaxurl,
                        data: {
                            action: 'users_activity_log',
                            accountno: accountno,
                            tabname:tabname,
                            logid:logid
                        },
                        type: "POST"
                    }); 
        }); 
        $('.dashboard-tabs-payment-schedule').click(function(){ 
            var logid = localStorage.getItem('logid'); 
            var accountno = $(".user_acc_no").val();
            var tabname = 'Payment Schedule';
                $.ajax({
                        url: ajaxurl,
                        data: {
                            action: 'users_activity_log',
                            accountno: accountno,
                            tabname:tabname,
                            logid:logid
                        },
                        type: "POST"
                    }); 

        }); 
        $('.dashboard-tabs-payment-detail').click(function(){ 
            var logid = localStorage.getItem('logid'); 
            var accountno = $(".user_acc_no").val();
            var tabname = 'Payment Details';
                $.ajax({
                        url: ajaxurl,
                        data: {
                            action: 'users_activity_log',
                            accountno: accountno,
                            tabname:tabname,
                            logid:logid
                        },
                        type: "POST"
                        // success: function(result) {
                        //     alert("Normal Logout");
                        // }
                    }); 

        });       
    }
    var resourceTable = function () {
        if($("#resourceList").length){
            var table = $('#resourceList').DataTable({
                "info":     false,
                "bLengthChange": false,
                "pageLength": 20,
                "order": [[ 0, "desc" ]],
                oLanguage: {
                    oPaginate: {
                        sNext: '<span class="pagination-fa"><i class="fa fa-chevron-right" ></i></span>',
                        sPrevious: '<span class="pagination-fa"><i class="fa fa-chevron-left" ></i></span>'
                    }
                }, 
                'aoColumnDefs': [{
                    'bSortable': false,
                    'aTargets': [-1] /* 1st one, start by the right */
                },
                { "width": "5%", "targets": 0 },
                { "width": "15%", "targets": 1 },
                { "width": "5%", "targets": 4 }
            ]
            });
        }
        
        $("#resourceList").on("click", '.resource-edit', function() {
            var currentRow = $(this).closest("tr");
            $('#resourceIDInput').val(currentRow.find("td:eq(0)").text());
            $('#resourceKeyInput').val(currentRow.find("td:eq(1)").text());
            $('#resourceEnglishInput').val(currentRow.find("td:eq(2)").text());
            $('#resourceArabicInput').val(currentRow.find("td:eq(3)").text());
            $('#editResourceModal').modal('show');
        });
        $(".btnAddResource").click(function () {
            $("#addResourceForm")[0].reset();
            $('#addResourceModal').modal('show');
        });
        $('#editResourceForm').bootstrapValidator({ 
            message: 'This value is not valid',
            fields: {
                resource_english: {
                    validators: {
                        notEmpty: {
                            message: 'Please add english resource'
                        }
                    }
                },
                resource_arabic: {
                    validators: {
                        notEmpty: {
                            message: 'Please add arabic resource'
                        }
                    }
                },
            }     
        }).on('success.form.bv', function (e) {
            e.preventDefault();
            var formdata = $('#editResourceForm').serialize();
            var btnresourceText = $('.btnsaveResource').text();
            $('.btnsaveResource').html('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>Please wait...');
            $.ajax({
                url: ajaxurl,
                data: {
                    action: 'edit_resource',
                    resourceData: formdata
                },
                type: "POST",
                success: function(response) {
                    $('.btnsaveResource').html(btnresourceText);
                    // var data = jQuery.parseJSON(response);
                    $('.editresponse').show();
                    $('.editresponse').html('<div class="alert alert-success font-weight-bold  text-white ">Resource Updated.</div>');
                    setTimeout(function(){
                        $('.editresponse').hide();
                    }, 2000);
                    location.reload();
                    
                }
            });
        });

        $('#addResourceForm').bootstrapValidator({ 
            message: 'This value is not valid',
            fields: {
                resource_key: {
                    validators: {
                        notEmpty: {
                            message: 'Please add Resource Key'
                        },
                        regexp: {
                            regexp: /^[a-z]+$/i,
                            message: 'Use only alphabetical characters and no space'
                        }
                    }
                },
                resource_english: {
                    validators: {
                        notEmpty: {
                            message: 'Please add english resource'
                        }
                    }
                },
                resource_arabic: {
                    validators: {
                        notEmpty: {
                            message: 'Please add arabic resource'
                        }
                    }
                },
            }     
        }).on('success.form.bv', function (e) {
            e.preventDefault();
            var formdata = $('#addResourceForm').serialize();
            var btnresourceText = $('.addResource').text();
            $('.addResource').html('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>Please wait...');
            $.ajax({
                url: ajaxurl,
                data: {
                    action: 'add_resource',
                    resourceData: formdata
                },
                type: "POST",
                success: function(response) {
                    $('.addResource').html(btnresourceText);
                    var data = jQuery.parseJSON(response);
                    $('.addresourceresponse').show();
                    if(data.status == "true"){
                        $('.addresourceresponse').html('<div class="alert alert-success font-weight-bold  text-white ">'+data.msg+'</div>');

                        setTimeout(function(){
                            $('#addResourceModal').modal('hide');
                        }, 4000);
                        location.reload();
                    } else {
                        $('.addresourceresponse').html('<div class="alert alert-danger font-weight-bold  text-white ">'+data.msg+'</div>');
                    }
                    setTimeout(function(){
                        $('.addresourceresponse').hide();
                    }, 2000);
                    
                }
            });
        });
    }
    var kycupdatedetailss = function(){
        $('.KycEditDetails').click(function(){
            $(".kyc-inactive, #btnCancel" ).prop( "disabled", false );
            if(MortgageLoanAccountNumber != ""){
              $("#lbleDistrictName, #lblNetWorth, #pepsyes, #pepsno, #lblFundSource, #lblUnitNo, #lblPosition" ).prop( "disabled", true );
            }
            $(".KycEditDetails").prop("disabled", true);
            $(".edit-input-details").show();
            $(".no-edit-input-details").hide();
        });
        $("#btnCancel").click(function(){
            $(".kyc-inactive, #btnUpdate" ).prop( "disabled", true);
            $(".KycEditDetails").prop("disabled", false);
            $(".edit-input-details").hide();
            $(".no-edit-input-details").show();
            // $('#kycupdatedetails').trigger("reset");

            setTimeout(function() {
                $('#errorupdateStatusModal').modal('show');
            }, 1000); 
          
            setTimeout(function() {
                location.reload(true)
            }, 2500); 
            
        });

        $(document).ready(function () {
            if($("#pepsyes").val() == "Yes") {
                $("#pepsyes").prop("checked", true);
                $(".peps-details").show();
            };
            if($("#pepsno").val() == "No") {
                $("#pepsno").prop("checked", true);
                $(".peps-details").hide();
            };
            $('#pepsyes').click(function(){
                $(".peps-details").show();
                $("#pepsno").prop("checked", false);
                $("#pepsyes").val('Yes');
            });
            $('#pepsno').click(function(){
                $(".peps-details").hide();
                $("#pepsyes").prop("checked", false);
                $("#pepsno").val('No');
            });
        });
    }
    var kycpepsstatus = function(){
        $(document).ready(function() {
        if($('#kycupdatedetails').length){ 
            $('#kycupdatedetails').bootstrapValidator({ 
                message: 'This value is not valid',
                fields: {
                    MobileNumber: {
                        container: '.MobileNumber',
                        validators: {
                            notEmpty: {
                                message: errorValidMobile
                            },
                            stringLength: {
                                max:10
                            },
                            regexp: {
                                regexp: /^[0-9]+$/i,
                            }
                        }
                    },
                    lblEmail: {
                        container: '.lblEmail',
                        validators: {
                            notEmpty: {
                                message: errorValidEmailEmpty
                            },
                            stringLength: {
                                max:30
                            },
                            regexp: {
                                regexp: /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-z]+\.[a-z]{2,3}$/i,
                                message: errorValidEmail
                            }
                        }
                    },
                    lblBuildingNo: {
                        container: '.lblBuildingNo',
                        validators: {
                            notEmpty: {
                                message: errorValidBuildingNo
                            },
                            stringLength: {
                                max:10
                            },
                            regexp: {
                                regexp: /^[0-9]+$/i,
                            }
                        }
                    },
                    lblStreetName: {
                        container: '.lblStreetName',
                        validators: {
                            notEmpty: {
                                message: errorKYCStreetName
                            },
                            stringLength: {
                                max:40
                            },
                        }
                    },
                    lbleDistrictName: {
                        container: '.lbleDistrictName',
                        validators: {
                            notEmpty: {
                                message: errorKYCDistrictName
                            },
                            stringLength: {
                                max:40
                            },
                        }
                    },
                    lblCityId: {
                        container: '.lblCityId',
                        validators: {
                            notEmpty: {
                                message: errorValidCity
                            },
                            stringLength: {
                                max:40
                            },
                        }
                    },
                    lblZipCode: {
                        container: '.lblZipCode',
                        validators: {
                            notEmpty: {
                                message: errorKYCZipCode
                            },
                            stringLength: {
                                max:6
                            },
                            regexp: {
                                regexp: /^[0-9]+$/i,
                            }
                        }
                    },
                    lblAdditionalNumber: {
                        container: '.lblAdditionalNumber',
                        validators: {
                             stringLength: {
                                max:10
                            },
                            regexp: {
                                regexp: /^[0-9]+$/i,
                            }
                        }
                    },
                    lblUnitNo: {
                        container: '.lblUnitNo',
                        validators: {
                             stringLength: {
                                max:10
                            },
                            regexp: {
                                regexp: /^[0-9]+$/i,
                            }
                        }
                    },
                    lblNIDExDate: {
                        container: '.lblNIDExDate',
                        validators: {
                            notEmpty: {
                                message: errorValidExpitryDate
                            }
                        }
                    },
                    lblMonthlyIncome: {
                        container: '.lblMonthlyIncome',
                        validators: {
                            notEmpty: {
                                message: errorValidMonthlyIncome
                            },
                            stringLength: {
                                max:20
                            },
                        }
                    },
                    lblNetWorth: {
                        container: '.lblNetWorth',
                        validators: {
                            notEmpty: {
                                message: errorValidNetWorth
                            },
                            stringLength: {
                                max:20
                            },
                        }
                    },
                    lblFundSource: {
                        container: '.lblFundSource',
                        validators: {
                            notEmpty: {
                                message: errorEmptyFields
                            }
                        }
                    },
                    pepsyes: {
                    },
                    pepsno: {
                    },
                    lblOccupation: {
                        container: '.lblOccupation',
                        validators: {
                            notEmpty: {
                                message: errorValidOccupation
                            }
                        }
                    },
                    lblPosition: {
                        container: '.lblPosition',
                        validators: {
                            notEmpty: {
                                message: errorValidPosition
                            }
                        }
                    }
                }   
             })
             .on('success.form.bv', function (e) {
                   e.preventDefault();
                   $("#btnUpdate" ).prop( "disabled", false );
                    var formdata = $('#kycupdatedetails').serialize();
                    var btnText = $('#btnUpdate').attr('data-btnname');
                    $('#btnUpdate').html('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>');
                      $.ajax({
                        url: ajaxurl,
                        data: {
                            action: 'kyc_post_user_account_info',
                            KYCPostdata: formdata,
                            accNumber:MortgageLoanAccountNumber
                        },
                        type: "POST",
                        success: function(response) {
                            var data = jQuery.parseJSON(response);
                            if( data.body == "true"){
                                $('#btnUpdate').html(btnText);
                                $('#updateStatusModal').modal('show');
                                location.reload()
                            }
                            else{
                                $('#errorupdateStatusModal').modal('show');
                                location.reload()
                            }
                        }
                });
            });        
        }

     });

    }

    return {
        init: function () {
            general_setting();
            verifyNationalID();
            verifyOTP();
            createTicket();
            resendOTP();
            resendKYCOTP();
            viewDetailsbyAccountNo();
            logout();
            userlogs();
            usersactivitylog();
            resourceTable();
            kycupdatedetailss();
            kycpepsstatus();
            smspaymentschedule();
        }
    };

}();