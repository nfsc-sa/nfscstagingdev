<?php
session_start();//session starts here 
$objAdmin = new Nfsc_Clarion_Admin('nfsc-clarion', '1.0.1');
$objAdmin->maintenace_mode();
$plugin_aseets_path = plugin_dir_url( dirname( __FILE__ ) ) ;

if(pll_current_language() == 'ar'){
  $html_lang  =  'lang="ar" dir="rtl"';
  $dashboard_url = home_url().'/account-ar';
  $OTPfieldDirect = 'style="flex-direction: row-reverse;"';
} else {
  $dashboard_url = home_url().'/en/account';
  $html_lang  = 'lang="en"';
  $OTPfieldDirect = "";
}

require_once plugin_dir_path( dirname( __FILE__ ) ) . '../admin/constant/lang.php'; 
?>

<!DOCTYPE html>
<html <?php echo $html_lang; ?> >

<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  
  <title>
    <?php wp_title(''); ?> | <?php echo get_bloginfo( 'name' ); ?>
    
  </title>
  <!--     Fonts and icons     -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" />
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
  <link href="<?php echo $plugin_aseets_path.'/template/assets/css/nucleo-svg.css'; ?>" rel="stylesheet" />
  <!-- Font Awesome Icons -->
  <script src="https://kit.fontawesome.com/42d5adcbca.js" crossorigin="anonymous"></script>
  <link href="<?php echo $plugin_aseets_path.'/template/assets/css/nucleo-svg.css';?>" rel="stylesheet" />
  <!-- CSS Files -->
  <link rel="stylesheet" href="<?php echo $plugin_aseets_path.'/template/assets/dist/css/bootstrapValidator.css'; ?>" />
  <script src="https://www.google.com/recaptcha/api.js?onload=loginCaptcha&render=explicit&hl=<?php echo pll_current_language(); ?>" async defer></script>
  <link id="pagestyle" href="<?php echo $plugin_aseets_path.'/template/assets/css/soft-ui-dashboard.css?v=1.0.3';?>" rel="stylesheet" />
  <link id="adminstyle" href="<?php echo $plugin_aseets_path.'../admin/css/nfsc-clarion-admin.css?v=1.0.3';?>" rel="stylesheet" />
  <script type="text/javascript" >
      var  ajaxurl = "<?php echo admin_url('admin-ajax.php'); ?>";
      var  errorNationalIDReq = "<?php echo trim($errorNationalIDReq); ?>";
      var  errorNationalIDTenDigit = "<?php echo trim($errorNationalIDTenDigit); ?>";
      var  errorNationalIDValid = "<?php echo trim($errorNationalIDValid); ?>";
      var  errorOTPValid = "<?php echo $errorOTPValid; ?>";
      var  erroOriginsationDate = "<?php echo $erroOriginsationDate; ?>";
      var  errorOTPEnter = "<?php echo $errorOTPEnter; ?>";
  </script>
</head>

<body class="login-page login-background">
  
<main class="main-content mt-0">
   <section>
      <div class="page-header min-vh-75">
         <div class="container">
            <div class="row">
               <div class="col-xl-4 col-lg-5 col-md-6 d-flex flex-column mx-auto">
                  <div class="card card-plain mt-6">
                     <div class="card-header pb-3 text-left bg-transparent">
                        <a class="navbar-brand m-0" href="<?php echo home_url(); ?>">
                        <img src="<?php echo $plugin_aseets_path.'template/assets/img/logo.png';?>" class="navbar-brand-img h-100" alt="main_logo" style="width:100%;">
                        </a>
                     </div>
                     <div class="card-body">
                        <form role="form" id="verifyNICForm" method="POST" aria-labelledby="verifyNICForm">
                          <div class="form-group" >
                            <label for="nationalID"><?php echo $lblNationalID; ?></label>
                            <input type="text" id="nationalID" name="nationalID" class="form-control" placeholder="<?php echo $lblNationalID; ?>" aria-label="NationalID" aria-describedby="nationalid-addon"  maxlength="10" onkeyup="onlyNumber(this)">
                          </div>
                          <div id="recaptchaLogin" class="col-12 text-center"></div>
                           <input type="hidden" name="login_captcha_reponse_key" id="login_captcha_reponse_key" data-validate="true">
                            <div class="text-center" id="captchError" >
                              <span class="help-block error" data-bv-validator="callback" data-bv-for="login_captcha_reponse_key" data-bv-result="INVALID" style=""><?php echo $errorCaptcha; ?></span>       
                            </div>  
                            <div class="col-12 text-center nationalIDVaildError mt-4" style="display:none;">
                              
                            </div>
                           <div class="col-12 text-center">
                              <button type="submit" id="btnVerifyme" class="btn bg-gradient-info w-100 mt-4 mb-0 verify-national-id" disabled><?php echo $btnVerifyme; ?></button>
                           </div>
                        </form>
                        <form role="form" id="OTPForm" class="digit-group" data-group-name="digits" data-autosubmit="true" autocomplete="on" style="display:none;" aria-labelledby="otp">
                           <div>
                              <p><?php echo $lblCodeGenerated; ?></p>
                           </div>
                           <div class="row">
                              <div class="col-md-12 mb-md-0 mb-4 mt-4  text-center">
                                <div class="row" <?php echo $OTPfieldDirect; ?> >
                                  <div class="col-md-2 col-2">
                                      <input type="text" id="digit-1" name="OTP[]" data-next="digit-2" class="form-control" inputmode="numeric" autocomplete="one-time-code" />
                                  </div>
                                  <div class="col-md-2 col-2">
                                      <input type="text" id="digit-2" name="OTP[]" data-next="digit-3" data-previous="digit-1" class="form-control" inputmode="numeric" autocomplete="one-time-code" />
                                  </div>
                                  <div class="col-md-2 col-2">
                                      <input type="text" id="digit-3" name="OTP[]" data-next="digit-4" data-previous="digit-2" class="form-control" inputmode="numeric" autocomplete="one-time-code"/>
                                  </div>
                                  <div class="col-md-2 col-2">
                                      <input type="text" id="digit-4" name="OTP[]" data-next="digit-5" data-previous="digit-3" class="form-control" inputmode="numeric" autocomplete="one-time-code"/>
                                  </div>
                                  <div class="col-md-2 col-2">
                                      <input type="text" id="digit-5" name="OTP[]" data-next="digit-6" data-previous="digit-4" class="form-control" inputmode="numeric" autocomplete="one-time-code"/>
                                  </div>
                                  <div class="col-md-2 col-2">
                                      <input type="text" id="digit-6" name="OTP[]" data-previous="digit-5" class="form-control" inputmode="numeric" autocomplete="one-time-code" />
                                  </div>
                                  
                                </div>
                              </div>
                           </div>
                           <div class="text-center mt-4">
                              <p><?php echo $lblOTP; ?> <span id="m_timer"></span></p>
                              <input type="hidden" value="" name="acc_no" class="hiddenAcc_no">
                              <input type="hidden" value="" name="hidenationalId" class="hiddenNationalID">
                              <input type="hidden" value="" name="hideoriginationDate" class="hiddenoriginationDate">
                              <input type="hidden" value="<?php echo $dashboard_url; ?>" name="dashboard_url" class="dashboard_url">
                           </div>
                           <div class="col-md-12 mt-3 text-center" id="OTPError">
                              <span class="error"><?php echo $errorOTPEnter; ?></span>
                          </div>
                           <div class="row">
                              <div class="col-md-6 col-6 col-sm-6">
                                 <button type="button" id="btnResendOtp" name="resend-otp" class="btn bg-gradient-info w-100 mt-4 mb-0" disabled><?php echo $btnResendOTP; ?></button>
                              </div>
                              <div class="col-md-6 col-6 col-sm-6">
                                 <button type="button" id="btnSignIn" name="sign-in" class="btn bg-gradient-info w-100 mt-4 mb-0" disabled data-btnName="<?php echo $btnSignIn; ?>"><?php echo $btnSignIn; ?></button>
                              </div>
                           </div>
                        </form>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </section>
</main>
 
  <!-- -------- START FOOTER 3 w/ COMPANY DESCRIPTION WITH LINKS & SOCIAL ICONS & COPYRIGHT ------- -->
  <footer class="footer py-5">
    <div class="container">
      
      <div class="row">
        <div class="col-8 mx-auto text-center mt-1">
          <p class="mb-0 text-secondary">
            <?php echo $lblCopyright ; ?>
          </p>
        </div>
      </div>
    </div>
  </footer>
  <!-- -------- END FOOTER 3 w/ COMPANY DESCRIPTION WITH LINKS & SOCIAL ICONS & COPYRIGHT ------- -->
  <!--   Core JS Files   -->
  <script src="<?php echo $plugin_aseets_path.'template/assets/js/core/popper.min.js';?>" ></script>
  <script src="<?php echo $plugin_aseets_path.'template/assets/js/core/bootstrap.min.js';?> " ></script>
  <script src="<?php echo $plugin_aseets_path.'template/assets/js/plugins/perfect-scrollbar.min.js';?>" ></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/flatpickr/4.6.9/flatpickr.min.js" ></script>
  <script src="<?php echo $plugin_aseets_path.'template/assets/js/jquery.countdownTimer.js'; ?> "></script>
  <script src="<?php echo $plugin_aseets_path.'template/assets/js/plugins/smooth-scrollbar.min.js'; ?> "></script>
  <script type="text/javascript" src="<?php echo $plugin_aseets_path.'/template/assets/dist/js/bootstrapValidator.min.js'; ?>"></script>
  <script type="text/javascript" src="<?php echo $plugin_aseets_path.'template/assets/js/nfsc-dashboard.js'; ?> "></script>
  
<script>
  // Restricts input for the given textbox to the given inputFilter.
function setInputFilter(textbox, inputFilter) {
  ["input", "keydown", "keyup", "mousedown", "mouseup", "select", "contextmenu", "drop"].forEach(function(event) {
    textbox.addEventListener(event, function() {
      if (inputFilter(this.value)) {
        this.oldValue = this.value;
        this.oldSelectionStart = this.selectionStart;
        this.oldSelectionEnd = this.selectionEnd;
       } else if (this.hasOwnProperty("oldValue")) {
        this.value = this.oldValue;
        this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
      } else {
        this.value = "";
      }
    });
  });
  ["input"].forEach(function(event) {
    textbox.addEventListener(event, function() {
      if (inputFilter(this.value)) {
        $('#btnSignIn').removeAttr('disabled', 'disabled');
      }
     
    });
  });
  ["input","select"].every(function(event) {
    textbox.addEventListener(event, function() {
     
      // if (inputFilter(this.value)) {
        if(this.value.length === 0){
          $('#btnSignIn').attr('disabled', 'disabled');
          var t = $('#digit-1').val();
      console.log(t);
        }
      // }
     
    });
  });
}
// Install input filters.
setInputFilter(document.getElementById("digit-1"), function(value) {
  return /^-?\d*$/.test(value); });
  setInputFilter(document.getElementById("digit-2"), function(value) {
  return /^-?\d*$/.test(value); });
  setInputFilter(document.getElementById("digit-3"), function(value) {
  return /^-?\d*$/.test(value); });
  setInputFilter(document.getElementById("digit-4"), function(value) {
  return /^-?\d*$/.test(value); });
  setInputFilter(document.getElementById("digit-5"), function(value) {
  return /^-?\d*$/.test(value); });
  setInputFilter(document.getElementById("digit-6"), function(value) {
  return /^-?\d*$/.test(value); });
  
 
  function loginCaptcha () {
    recaptchaLogin = grecaptcha.render('recaptchaLogin', {
        'sitekey': '6LfIjpEbAAAAAGmP5QpprlLPRWL_XP1xvSQ5Ev97',
        'callback': function (response) {
            jQuery('#login_captcha_reponse_key').val(response);
           // add if codition for check recaptcha
            // $('#btnVerifyme').removeAttr('disabled');
            var isValid = ValidateNICNumber($('#nationalID').val());
            if(isValid == true){
             $('#btnVerifyme').removeAttr('disabled');
            }
            $('#captchError').hide();

        }
    });
// add validate number variable function
    var ValidateNICNumber = function (number) {
        if (number.length != 10) {
            return false;
        } else {
            var nSum = 0;
            var lastDigit = "";
            var nDigit = 0;
            for (i = 0; i < 9; i++) {
                nDigit = parseInt(number[i]);
               // console.log(i);
                if (i % 2 == 0) // If Odd position digit double it
                {
                    if ((nDigit * 2) > 9) // If two digits number then add each digit
                        nSum = parseInt(nSum) + parseInt((nDigit * 2).toString().substring(0, 1)) + parseInt((nDigit * 2).toString().substring(1, 2));                  
                    else
                        nSum = parseInt(nSum) + (parseInt(nDigit) * 2);
                } else
                    nSum += nDigit;
            }
            console.log(nSum);
            var lastDigit = (nSum % 10).toString();
            console.log(lastDigit);
            if (lastDigit != "0") {
                lastDigit = (10 - (lastDigit)).toString();
            }
    
            if (lastDigit == number.substring(10, 9)){
                return true;
            } else {
                return false;
            }
        }
    }
  }
    
</script>
<script async defer src="https://buttons.github.io/buttons.js"></script>

</body>

</html>