<?php 
session_start();
if(!$_SESSION['NationalId']){  
  wp_redirect(home_url('/en/kyc-login')); 
}
$sNationalId = $_SESSION['NationalId'];
$objAdmin = new Nfsc_Clarion_Admin('nfsc-clarion', '1.0.1');
$language = pll_current_language();
$objAdmin->maintenace_mode();
$userData = $objAdmin->kyc_user_account_info($sNationalId);
$userData = $userData->data;
$FullName = $userData[0]->FullName;
$DropdownData = $objAdmin->get_dropdown_details();
$DropdownData = $DropdownData->data;
usort($DropdownData->ListCityMaster, function($a, $b) { 
  return $a->CityNameInEn > $b->CityNameInEn ? 1 : -1; 
});                                                                                             
usort($DropdownData->ListFundSourceMaster, function($a, $b) { 
  return $a->FundSourceNameInEn > $b->FundSourceNameInEn ? 1 : -1; 
}); 
usort($DropdownData->ListProfessionMaster, function($a, $b) { 
  return $a->ProfessionNameInEn > $b->ProfessionNameInEn ? 1 : -1; 
}); 
usort($DropdownData->ListPositionMaster, function($a, $b) { 
  return $a->PositionNameInEn > $b->PositionNameInEn ? 1 : -1; 
}); 
require_once plugin_dir_path( dirname( __FILE__ ) ) . '../admin/constant/lang.php'; 
require_once plugin_dir_path( dirname( __FILE__ ) ) . 'template/comman/header.php'; 
$btnLogOut = $objAdmin->get_resource('btnLogOut');
$LMSReferenceNo = $userData[0]->ReferenceNo;
$MortgageLoanAccountNumber = $userData[0]->MortgageLoanAccountNumber;
$NationalID = $userData[0]->NationalID;
$NIDExpiryDate = ($userData[0]->NIDExpiryDate) ? date('d-m-Y', strtotime($userData[0]->NIDExpiryDate)) : '';
//$OriginationDate = ($userData[0]->OriginationDate) ? date('d-m-Y', strtotime($userData[0]->OriginationDate)) : '';
$OriginatorNameInEn = $userData[0]->OriginatorName;
$ContractTypeInEn = $userData[0]->ContractTypeInEn;
$FullName = $userData[0]->FullName;
$LegalOwner = $userData[0]->LegalOwner;
$MobileNumber = $userData[0]->MobileNumber;
$Email = $userData[0]->Email;
$MonthlyIncomes = $userData[0]->MonthlyIncome;
$MonthlyIncome = number_format($MonthlyIncomes, 2, '.', ',');
$NetWorths = $userData[0]->NetWorth;
$NetWorth = number_format($NetWorths, 2, '.', ',');
$FundSourceID = $userData[0]->FundSourceID;
$FundSource = $userData[0]->FundSource;
$Occupation = $userData[0]->ProfessionNameInEn;
$OccupationID = $userData[0]->ProfessionID;
$Position = $userData[0]->Position;
$PositionID = $userData[0]->PositionID;
$Address = $userData[0]->Address;
$ISPEPS = $userData[0]->ISPEPS;
$BuildingNo = $userData[0]->BuildingNumber;
$StreetName = $userData[0]->StreetName;
$DistrictName = $userData[0]->DistrictName;
$CityID = $userData[0]->CityID;
$City = $userData[0]->CityNameInEn;
$ZipCode = $userData[0]->PostalCode;
$AdditionalNumber = $userData[0]->AdditionalNumbers;
$UnitNo = $userData[0]->UnitNo;
$list = array();
if($MortgageLoanAccountNumber != ""){
  $DistrictName = 'NA';
  $UnitNo = 'NA';
  $Position = 'NA';
  $PositionID = 'NA';
  $NetWorth = 'NA';
  $FundSource = 'NA';
  $FundSourceID = 'NA';
}
?>  
<script>
  var  errorKYCStreetName = "<?php echo $errorKYCStreetName ?>";
  var  errorKYCDistrictName = "<?php echo $errorKYCDistrictName ?>";
  var  errorKYCZipCode = "<?php echo $errorKYCZipCode ?>";
  var errorEmptyFields = "<?php echo $errorEmptyFields ?>";
  var errorValidEmail = "<?php echo $errorValidEmail ?>";
  var errorValidMobile = "<?php echo $errorValidMobile ?>";
  var errorValidCity = "<?php echo $errorValidCity ?>";
  var errorValidExpitryDate = "<?php echo $errorValidExpitryDate ?>";
  var errorValidMonthlyIncome = "<?php echo $errorValidMonthlyIncome ?>";
  var errorValidNetWorth = "<?php echo $errorValidNetWorth ?>";
  var errorValidBuildingNo = "<?php echo $errorValidBuildingNo ?>";
  var errorValidEmailEmpty = "<?php echo $errorValidEmailEmpty ?>";
  var errorValidOccupation = "<?php echo $errorValidOccupation ?>";
  var errorValidPosition = "<?php echo $errorValidPosition ?>";
  var MortgageLoanAccountNumber = "<?php echo $MortgageLoanAccountNumber ?>";
</script>
<div class="container-fluid py-4 container dashboard-conatiner">
  <section class="py-3">
      <div class="container">
        <div class="row align-middle text-center">
                 <h3 class="m-1 mb-0 badge text-lg badge-soft-info"><?php echo $lblKycUdateDetails; ?></h3>
        </div>
    </div>
  </section>
  <div class="tab-content tab-space">
   <!-- Start Tab 1 -->
   <form role="form" id="kycupdatedetails" method="POST" action="" role="form" aria-labelledby="kycupdatedetails">
   <div class="tab-pane active" id="profile-tabs-simple">
    <div class="row mt-4">
    <div class="col-lg-2">
            <button type="button" class="btn bg-gradient-info mb-0 kyc-btn KycEditDetails" data-btnName="<?php echo $lblEditKycDetails; ?>"><?php echo $lblEditKycDetails; ?></button>
    </div>
    <div class="col-lg-4">
        <div class="card">
          <div class="card-body p-3">
            <div class="d-flex flex-column justify-content-center text-center">
                <h6 class="mb-0 badge text-sm badge-soft-info"><?php echo $lblCustomerID; ?> - <?php echo $NationalID; ?>
                <input type="hidden" id="NationalID" class="form-control non-edit" name="NationalID" value="<?php echo $NationalID; ?>" >
                </h6>
              </div>
          </div>
        </div>
      </div>
      <?php if($MortgageLoanAccountNumber != ""){ ?>
      <div class="col-lg-4">
        <div class="card">
          <div class="card-body p-3">
            <div class="d-flex flex-column justify-content-center text-center">
                <h6 class="mb-0 badge text-sm badge-soft-info"><?php echo $lblAccountNo; ?> - <?php echo $MortgageLoanAccountNumber; ?></h6>
              </div>
          </div>
        </div>
      </div>
      <?php } ?>
    </div> 
  
    <div class="row mt-4">
      <div class="col-lg-6 ms-auto">
          <div class="card">
            <div class="card-body p-3">
                <div class="row">
                  <div class="col-12">
                      <div class="table-responsive">
                        <table class="table align-items-center mb-0"  aria-describedby="account" aria-hidden="true">
                            <tbody>
                              <tr>
                                  <td>
                                    <div class="d-flex px-2 py-0">
                                        <div class="d-flex flex-column justify-content-center">
                                          <h6 class="mb-0"><?php echo $lblFullName; ?></h6>
                                        </div>
                                    </div>
                                  </td>
                                  <td class="align-middle text-center">
                                     <input type="text" id="FullName" class="form-control non-edit" name="FullName" value="<?php echo $FullName; ?>" disabled >
                                  </td>
                              </tr>
                              <tr>
                                  <td>
                                    <div class="d-flex px-2 py-0">
                                        <div class="d-flex flex-column justify-content-center">
                                          <h6 class="mb-0"><?php echo $lblMobileNumber; ?><span class="kyc-star">*</span></h6>
                                        </div>
                                    </div>
                                  </td>
                                  <td class="align-middle text-center">
                                   <input type="text" id="MobileNumber" class="form-control kyc-inactive non-edit" name="MobileNumber" value="<?php echo $MobileNumber; ?>" disabled >
                                   <div class="MobileNumber" style="text-align: left;"></div>
                                  </td>
                              </tr>
                              <tr>
                                  <td>
                                    <div class="d-flex px-2 py-0">
                                        <div class="d-flex flex-column justify-content-center">
                                          <h6 class="mb-0 "><?php echo $lblEmail; ?><span class="kyc-star">*</span></h6>
                                        </div>
                                    </div>
                                  </td>
                                  <td class="align-middle text-center">
                                   <input type="text" id="lblEmail" class="form-control kyc-inactive non-edit" name="lblEmail" value="<?php echo $Email; ?>" disabled >
                                   <div class="lblEmail" style="text-align: left;"></div>
                                  </td>
                              </tr>
                              <tr class="edit-input-details">
                                  <td>
                                    <div class="d-flex px-2 py-0">
                                        <div class="d-flex flex-column justify-content-center">
                                          <h6 class="mb-0"><?php echo $lblBuildingNo; ?><span class="kyc-star">*</span></h6>
                                        </div>
                                    </div>
                                  </td>
                                  <td class="align-middle text-center">
                                     <input type="text" id="lblBuildingNo" class="form-control kyc-inactive non-edit" name="lblBuildingNo" value="<?php echo $BuildingNo; ?>" disabled >
                                     <div class="lblBuildingNo" style="text-align: left;"></div>
                                  </td>
                              </tr>
                              <tr class="edit-input-details">
                                <td>
                                  <div class="d-flex px-2 py-0">
                                      <div class="d-flex flex-column justify-content-center">
                                        <h6 class="mb-0"><?php echo $lblStreetName; ?><span class="kyc-star">*</span></h6>
                                      </div>
                                  </div>
                                </td>
                                <td class="align-middle text-center">
                                   <input type="text" id="lblStreetName" class="form-control kyc-inactive non-edit" name="lblStreetName" value="<?php echo $StreetName; ?>" disabled>
                                   <div class="lblStreetName" style="text-align: left;"></div>
                                </td>
                            </tr>
                            <tr class="edit-input-details">
                                <td>
                                  <div class="d-flex px-2 py-0">
                                      <div class="d-flex flex-column justify-content-center">
                                        <h6 class="mb-0"><?php echo $lbleDistrictName; ?><span class="kyc-star">*</span></h6>
                                      </div>
                                  </div>
                                </td>
                                <td class="align-middle text-center">
                                 <input type="text" id="lbleDistrictName" class=" form-control kyc-inactive non-edit" name="lbleDistrictName" value="<?php echo $DistrictName; ?>" disabled>
                                 <div class="lbleDistrictName" style="text-align: left;"></div>
                                </td>
                            </tr>
                            <tr class="edit-input-details">
                                <td>
                                  <div class="d-flex px-2 py-0">
                                      <div class="d-flex flex-column justify-content-center">
                                        <h6 class="mb-0"><?php echo $lblCityId; ?><span class="kyc-star">*</span></h6>
                                      </div>
                                  </div>
                                </td>
                                <td class="align-middle text-center">
                                 <select class="form-control" id="lblCityId" name="lblCityId">
                                  <option value="<?php echo $CityID; ?>"> <?php echo $City; ?> </option>
                                   <?php if($DropdownData->ListCityMaster){?>
                                      <?php foreach($DropdownData->ListCityMaster as $list) { ?>
                                           <option value="<?php echo $list->ID; ?>" data-id="<?php echo $list->ID; ?>" ><?php echo $list->CityNameInEn; ?></option>
                                      <?php } ?>
                                    <?php } ?>
                                  </select>
                                  <div class="lblCityId" style="text-align: left;"></div>
                                </td>
                            </tr>
                            <tr class="edit-input-details">
                                <td>
                                  <div class="d-flex px-2 py-0">
                                      <div class="d-flex flex-column justify-content-center">
                                        <h6 class="mb-0"><?php echo $lblZipCode; ?><span class="kyc-star">*</span></h6>
                                      </div>
                                  </div>
                                </td>
                                <td class="align-middle text-center">
                                 <input type="text" id="lblZipCode" class="form-control kyc-inactive non-edit" name="lblZipCode" value="<?php echo $ZipCode; ?>" disabled >
                                 <div class="lblZipCode" style="text-align: left;"></div>
                                </td>
                            </tr>
                            <tr class="edit-input-details">
                                <td>
                                  <div class="d-flex px-2 py-0">
                                      <div class="d-flex flex-column justify-content-center">
                                        <h6 class="mb-0"><?php echo $lblAdditionalNumber; ?></h6>
                                      </div>
                                  </div>
                                </td>
                                <td class="align-middle text-center">
                                  <input type="text" id="lblAdditionalNumber" class="form-control kyc-inactive non-edit" name="lblAdditionalNumber" value="<?php echo $AdditionalNumber; ?>" disabled >
                                  <div class="lblAdditionalNumber" style="text-align: left;"></div>
                                </td>
                            </tr>
                            <tr class="edit-input-details">
                                <td>
                                  <div class="d-flex px-2 py-0">
                                      <div class="d-flex flex-column justify-content-center">
                                        <h6 class="mb-0"><?php echo $lblUnitNo; ?></h6>
                                      </div>
                                  </div>
                                </td>
                                <td class="align-middle text-center">
                                <input type="text" id="lblUnitNo" class="form-control kyc-inactive non-edit" name="lblUnitNo" value="<?php echo $UnitNo; ?>" disabled >
                                <div class="lblUnitNo" style="text-align: left;"></div>
                                </td>
                            </tr>
                            <tr class="no-edit-input-details">
                                <td>
                                  <div class="d-flex px-2 py-0">
                                      <div class="d-flex flex-column justify-content-center">
                                        <h6 class="mb-0"><?php echo $lblAddress; ?></h6>
                                      </div>
                                  </div>
                                </td>
                                <td class="align-middle text-center">
                                 <textarea class="form-control kyc-inactive non-edit" id="lblAddress" rows="6" cols="5" name="lblAddress" disabled><?php echo $Address; ?></textarea>
                                </td>
                            </tr>
                         </tbody>
                        </table>
                      </div>
                  </div>
                </div>
            </div>
          </div>
      </div>
      <div class="col-lg-6 mt-lg-0 mt-4">
        <div class="card h-100">
          <div class="card-body p-3">
            <div class="row">
                  <div class="col-12">
                      <div class="table-responsive">
                        <table class="table align-items-center mb-0"  aria-describedby="account" aria-hidden="true">
                            <tbody>
                             <tr>
                                <td>
                                  <div class="d-flex px-2 py-0">
                                      <div class="d-flex flex-column justify-content-center">
                                        <h6 class="mb-0"><?php echo $lblReferenceNumber; ?></h6>
                                      </div>
                                  </div>
                                </td>
                                <td class="align-middle text-center">
                                 <input type="text" id="lblReferenceNumber" class="form-control non-edit" name="lblReferenceNumber" value="<?php echo $LMSReferenceNo; ?>" disabled >
                                </td>
                             </tr>
                              <tr>
                                <td>
                                  <div class="d-flex px-2 py-0">
                                      <div class="d-flex flex-column justify-content-center">
                                        <h6 class="mb-0"><?php echo $lblNIDExDate; ?><span class="kyc-star">*</span></h6>
                                      </div>
                                  </div>
                                </td>
                                <td class="align-middle text-center">
                                   <input type="text" id="lblNIDExDate" name="lblNIDExDate" class="form-control datepicker kyc-inactive non-edit" placeholder="dd-mm-yyyy" value="<?php echo $NIDExpiryDate; ?>" aria-label="origination-date" aria-describedby="origination-date-addon" disabled>
                                   <div class="lblNIDExDate" style="text-align: left;"></div>
                                </td>
                              </tr>
                              <tr>
                                  <td>
                                    <div class="d-flex px-2 py-0">
                                        <div class="d-flex flex-column justify-content-center">
                                          <h6 class="mb-0 "><?php echo $lblOroginationName; ?></h6>
                                        </div>
                                    </div>
                                  </td>
                                  <td class="align-middle text-center">
                                     <input type="text" id="lblOroginationName" class="form-control non-edit" name="lblOroginationName" value="<?php echo $OriginatorNameInEn; ?>" disabled >
                                  </td>
                              </tr>
                               <tr>
                                  <td>
                                    <div class="d-flex px-2 py-0">
                                        <div class="d-flex flex-column justify-content-center">
                                          <h6 class="mb-0"><?php echo $lblLegalOwner; ?></h6>
                                        </div>
                                    </div>
                                  </td>
                                  <td class="align-middle text-center">
                                      <input type="text" id="lblLegalOwner" class="form-control non-edit" name="lblLegalOwner" value="<?php echo $LegalOwner; ?>" disabled >
                                  </td>
                              </tr>
                              <tr>
                                <td>
                                  <div class="d-flex px-2 py-0">
                                      <div class="d-flex flex-column justify-content-center">
                                        <h6 class="mb-0"><?php echo $lblMonthlyIncome; ?><span class="kyc-star">*</span></h6>
                                      </div>
                                  </div>
                                </td>
                                <td class="align-middle text-center">
                                  <input type="text" id="lblMonthlyIncome" class="form-control kyc-inactive non-edit" name="lblMonthlyIncome" value="<?php echo $MonthlyIncome; ?>" disabled >
                                  <div class="lblMonthlyIncome" style="text-align: left;"></div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                  <div class="d-flex px-2 py-0">
                                      <div class="d-flex flex-column justify-content-center">
                                        <h6 class="mb-0"><?php echo $lblNetWorth; ?><span class="kyc-star">*</span></h6>
                                      </div>
                                  </div>
                                </td>
                                <td class="align-middle text-center">
                                 <input type="text" id="lblNetWorth" class="form-control kyc-inactive non-edit" name="lblNetWorth" value="<?php echo $NetWorth; ?>" disabled >
                                 <div class="lblNetWorth" style="text-align: left;"></div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                  <div class="d-flex px-2 py-0">
                                      <div class="d-flex flex-column justify-content-center">
                                        <h6 class="mb-0"><?php echo $lblFundSource; ?><span class="kyc-star">*</span></h6>
                                      </div>
                                  </div>
                                </td>
                                <td class="align-middle text-center">
                                   <select class="form-control kyc-inactive non-edit" id="lblFundSource" name="lblFundSource" disabled>
                                  <option value="<?php echo $FundSourceID; ?>"> <?php echo $FundSource; ?> </option>
                                   <?php if($DropdownData->ListFundSourceMaster){?>
                                      <?php foreach($DropdownData->ListFundSourceMaster as $list) { ?>
                                           <option value="<?php echo $list->ID; ?>" data-id="<?php echo $list->ID; ?>" ><?php echo $list->FundSourceNameInEn; ?></option>
                                      <?php } ?>
                                    <?php } ?>
                                  </select>
                                  <div class="lblFundSource" style="text-align: left;"></div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                  <div class="d-flex px-2 py-0">
                                      <div class="d-flex flex-column justify-content-center">
                                        <h6 class="mb-0"><?php echo $lblAreYouPEPS; ?><span class="kyc-star">*</span></h6>
                                      </div>
                                  </div>
                                </td>
                                <td class="align-middle" style="padding-left:18px;">
                                <?php if( $ISPEPS == "Yes" ) { ?>
                                  <input type="radio" class="kyc-inactive non-edit" id="pepsyes"  name="pepsyes" value="<?php echo $ISPEPS; ?>" checked disabled><label for="pepsyes">Yes</label>
                                <?php }else{ ?>
                                  <input type="radio" class="kyc-inactive non-edit" id="pepsyes"  name="pepsyes" value="<?php echo $ISPEPS; ?>" disabled><label for="pepsyes">Yes</label>
                                  <?php } ?>
                                  <?php if( $ISPEPS == "No" ) { ?>
                                  <input type="radio" class="kyc-inactive non-edit"  id="pepsno" name="pepsno" value="<?php echo $ISPEPS; ?>" checked disabled><label for="pepsno">No </label>
                                  <?php }else{ ?>
                                    <input type="radio" class="kyc-inactive non-edit"  id="pepsno" name="pepsno" value="<?php echo $ISPEPS; ?>" disabled><label for="pepsno">No </label>
                                  <?php } ?>
                                </td>
                            </tr>
                            <tr class="peps-details">
                                <td>
                                  <div class="d-flex px-2 py-0">
                                      <div class="d-flex flex-column justify-content-center">
                                        <h6 class="mb-0"><?php echo $lblOccupation; ?><span class="kyc-star">*</span></h6>
                                      </div>
                                  </div>
                                </td>
                                <td class="align-middle text-center">
                                
                                  <select class="form-control kyc-inactive non-edit" id="lblOccupation" name="lblOccupation" disabled>
                                  <?php if(!empty($OccupationID)){ ?>
                                  <option value="<?php echo $OccupationID; ?>"> <?php echo $Occupation; ?> </option>
                                  <?php }else{?> 
                                    <option value=""> <?php echo "Select Occupation"; ?> </option>
                                    <?php } ?>
                                   <?php if($DropdownData->ListProfessionMaster){?>
                                      <?php foreach($DropdownData->ListProfessionMaster as $list) { ?>
                                           <option value="<?php echo $list->ID; ?>" data-id="<?php echo $list->ID; ?>" ><?php echo $list->ProfessionNameInEn; ?></option>
                                      <?php } ?>
                                    <?php } ?>
                                  </select>
                                  <div class="lblOccupation" style="text-align: left;"></div>
                                </td>
                            </tr>
                            <tr class="peps-details">
                                <td>
                                  <div class="d-flex px-2 py-0">
                                      <div class="d-flex flex-column justify-content-center">
                                        <h6 class="mb-0"><?php echo $lblPosition; ?><span class="kyc-star">*</span></h6> 
                                      </div>
                                  </div>
                                </td>
                                <td class="align-middle text-center">
                                  <select class="form-control kyc-inactive non-edit" id="lblPosition" name="lblPosition" disabled>
                                    <?php if(!empty($PositionID)){ ?>
                                  <option value="<?php echo $PositionID; ?>"> <?php echo $Position; ?> </option>
                                  <?php }else{ ?>
                                    <option value=""> <?php echo "Select Position"; ?> </option>
                                    <?php } ?>
                                  <?php if($DropdownData->ListPositionMaster){?>
                                     <?php foreach($DropdownData->ListPositionMaster as $list) { ?>
                                           <option value="<?php echo $list->ID; ?>" data-id="<?php echo $list->ID; ?>" ><?php echo $list->PositionNameInEn; ?></option>
                                      <?php } ?>
                                    <?php } ?>
                                  </select>
                                  <div class="lblPosition" style="text-align: left;"></div>
                                </td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                  </div>
                </div>            
             </div>
        </div>
      </div>
    </div>
    <div class="row kyc-submit">
        <div class="col-md-2 col-6 col-sm-6">
            <button type="button" id="btnCancel" name="cancel" class="btn bg-gradient-info w-100 mt-4 mb-0" data-btnName="<?php echo $lblKycCancelBtn; ?>" disabled><?php echo $lblKycCancelBtn; ?></button>
        </div>
        <div class="col-md-2 col-6 col-sm-6">
            <button type="submit" id="btnUpdate" name="update" class="btn bg-gradient-info w-100 mt-4 mb-0" data-btnName="<?php echo $lblKycUpdateBtn; ?>" disabled><?php echo $lblKycUpdateBtn; ?></button>
        </div>
      </div>
    </div> 
   </form>
  </div>
   <!--Success Modal -->
    <div class="modal fade" id="updateStatusModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">             
                    <div class="row text-center">
                         <h6 class="mb-0"><?php echo $alertUpdateDetails; ?></h6>
                    </div>
               </div>
         </div>
      </div>
    </div>
    <div class="modal fade" id="errorupdateStatusModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                  <div class="row text-center">
                         <h6 class="mb-0"><?php echo $alerterrorUpdateDetails; ?></h6>
                  </div>
              </div>
         </div>
      </div>
    </div>
</div>
<?php require_once plugin_dir_path( dirname( __FILE__ ) ) . 'template/comman/footer.php'; ?>
<style>
  .account-img{
    display:none !important;
  }
</style>
</body>
</html>