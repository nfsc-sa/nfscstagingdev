<?php 
session_start();
if(!$_SESSION['NationalId']){  
  wp_redirect(home_url('/en/login')); 
}
$accountNumber = $_SESSION['AccountNumber'];
$nationalId = $_SESSION['NationalId'];
$objAdmin = new Nfsc_Clarion_Admin('nfsc-clarion', '1.0.1');
$objAdmin->maintenace_mode();
$userData = $objAdmin->user_account_info($accountNumber);
$userData = $userData->data;
$FullName = $userData->FullName;
$DelinquentBalance =  number_format($userData->DelinquentBalance, 2);
$contractDatas = $objAdmin->contracts_account_info($nationalId);
$contractData = $contractDatas->data;
$productcount = $contractDatas->ProductCount;
require_once plugin_dir_path( dirname( __FILE__ ) ) . '../admin/constant/lang.php'; 
require_once plugin_dir_path( dirname( __FILE__ ) ) . 'template/comman/header.php'; 
$language = pll_current_language();

?>
<div class="container-fluid py-4 container dashboard-conatiner min-vh-50">
    <div class="row mt-4">
        <div class="col-lg-8 col-sm-12 col-md-12 m-auto">
            <div class="card">
                <div class="card-body p-3">
                    <div class="row">
                        <div class="col-12">
                            <div class="d-flex align-items-center">
                                <?php if (pll_current_language() == 'ar') { ?>
                                    <h6 class="mb-0"><?php echo $lblSanctionedDisLoan; ?></h6> &nbsp;- &nbsp;<span class="text-white bg-secondary pt-1 pb-1 ps-2 pe-2 rounded-3"><?php echo $productcount; ?></span>
                                <?php } else { ?>
                                    <h6 class="mb-0"><?php echo $lblSanctionedDisLoan; ?> - <span class="text-white bg-secondary pt-1 pb-1 ps-2 pe-2 rounded-3"><?php echo $productcount; ?></span></h6>      
                                <?php } ?>
                            </div>
                        </div>
                    </div>    
                </div>
            </div>
        </div>
    </div>
    <?php 
    if($contractData) {  
        foreach($contractData as $contractDatas) { ?>
            <div class="row mt-4">
                <div class="col-lg-8 m-auto">
                    <div class="card">
                        <div class="contract-type card-header pb-0 p-3">
                            <div class="d-flex align-items-center">
                                <h6 class="mb-0 bg-primary p-3 rounded-1 pt-1 pb-1 ps-2 pe-2 text-white"><?php echo ($language == 'ar' ) ? $contractDatas->ContractTypeInAr : $contractDatas->ContractTypeInEn; ?></h6>
                            </div>
                        </div>
                        <div class="card-body p-3">
                            <div class="row">
                                <div class="col-md-4 col-sm-12 text-center p-1">
                                <span class="font-weight-bold text-sm"><?php echo $lblAccountNo; ?></span><br>
                                <span class="font-weight-bold text-lg text-primary"><?php echo $contractDatas->MortgageLoanAccountNumber; ?></span>
                                </div>
                                <div class="col-md-4 col-sm-12 text-center p-1">
                                <span class="font-weight-bold text-sm"><?php echo $lblPayableAmt; ?></span><br>
                                <span class="font-weight-bold text-lg"><?php echo number_format($contractDatas->TotalPayableAmount, 2); ?></span>
                                </div>
                                <div class="col-md-4 col-sm-12 text-center p-1">
                                    <button class="btn bg-gradient-dark mb-0 btnViewDetails" data-acc="<?php echo $contractDatas->MortgageLoanAccountNumber; ?>"><?php echo $lblViewDetails; ?></button>
                                </div>
                            </div>
                            <?php if ($contractDatas->TotalPayableAmount > 0) : ?>
                                <div class="row">
                                    <div class="col-12">
                                        <div class="text-center delinquent-bg-warning mt-3">
                                            <span><?php echo str_replace("{0}",$contractDatas->DelinquentBalance, $errorDeliquentBalanceOver); ?></span>
                                        </div>         
                                    </div>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
    <?php }
            } ?>
</div>
<?php
require_once plugin_dir_path( dirname( __FILE__ ) ) . 'template/comman/footer.php'; ?>

</body>
<style>
img.account-img {
    display:none;
}
</style>
</html>