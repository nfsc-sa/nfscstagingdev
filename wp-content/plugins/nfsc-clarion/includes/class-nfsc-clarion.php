<?php

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       https://profiles.wordpress.org/yogeshdalavi/
 * @since      1.0.0
 *
 * @package    Nfsc_Clarion
 * @subpackage Nfsc_Clarion/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    Nfsc_Clarion
 * @subpackage Nfsc_Clarion/includes
 * @author     Yogesh Dalavi <yogesh.dalavi@clariontechnologies.co.in>
 */
class Nfsc_Clarion {

	/**
	 * The loader that's responsible for maintaining and registering all hooks that power
	 * the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      Nfsc_Clarion_Loader    $loader    Maintains and registers all hooks for the plugin.
	 */
	protected $loader;

	/**
	 * The unique identifier of this plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $plugin_name    The string used to uniquely identify this plugin.
	 */
	protected $plugin_name;

	/**
	 * The current version of the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $version    The current version of the plugin.
	 */
	protected $version;

	/**
	 * Define the core functionality of the plugin.
	 *
	 * Set the plugin name and the plugin version that can be used throughout the plugin.
	 * Load the dependencies, define the locale, and set the hooks for the admin area and
	 * the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function __construct() {
		if ( defined( 'NFSC_CLARION_VERSION' ) ) {
			$this->version = NFSC_CLARION_VERSION;
		} else {
			$this->version = '1.0.0';
		}
		$this->plugin_name = 'nfsc-clarion';

		$this->load_dependencies();
		$this->set_locale();
		$this->define_admin_hooks();
		$this->define_public_hooks();
		$this->define_template_hooks();

	}

	/**
	 * Load the required dependencies for this plugin.
	 *
	 * Include the following files that make up the plugin:
	 *
	 * - Nfsc_Clarion_Loader. Orchestrates the hooks of the plugin.
	 * - Nfsc_Clarion_i18n. Defines internationalization functionality.
	 * - Nfsc_Clarion_Admin. Defines all hooks for the admin area.
	 * - Nfsc_Clarion_Public. Defines all hooks for the public side of the site.
	 *
	 * Create an instance of the loader which will be used to register the hooks
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function load_dependencies() {

		/**
		 * The class responsible for orchestrating the actions and filters of the
		 * core plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-nfsc-clarion-loader.php';

		/**
		 * The class responsible for defining internationalization functionality
		 * of the plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-nfsc-clarion-i18n.php';

		/**
		 * The class responsible for defining template for dashboard
		 * side of the site.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/template/class-nfsc-clarion-template.php';

		/**
		 * The class responsible for defining all actions that occur in the admin area.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-nfsc-clarion-admin.php';

		/**
		 * The class responsible for defining all actions that occur in the public-facing
		 * side of the site.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/class-nfsc-clarion-public.php';

		$this->loader = new Nfsc_Clarion_Loader();

	}

	/**
	 * Define the locale for this plugin for internationalization.
	 *
	 * Uses the Nfsc_Clarion_i18n class in order to set the domain and to register the hook
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function set_locale() {

		$plugin_i18n = new Nfsc_Clarion_i18n();

		$this->loader->add_action( 'plugins_loaded', $plugin_i18n, 'load_plugin_textdomain' );

	}

	/**
	 * Register all of the hooks related to the admin area functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_admin_hooks() {

		$plugin_admin = new Nfsc_Clarion_Admin( $this->get_plugin_name(), $this->get_version() );

		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_styles' );
		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_scripts' );
		$this->loader->add_action( 'wp_ajax_nopriv_validateNationalID',$plugin_admin, 'validateNationalID');
        $this->loader->add_action( 'wp_ajax_validateNationalID',$plugin_admin, 'validateNationalID');
		$this->loader->add_action( 'wp_ajax_nopriv_verifyOTP',$plugin_admin, 'verifyOTP');
        $this->loader->add_action( 'wp_ajax_verifyOTP',$plugin_admin, 'verifyOTP');
		$this->loader->add_action( 'wp_ajax_nopriv_KYCverifyOTP',$plugin_admin, 'KYCverifyOTP');
        $this->loader->add_action( 'wp_ajax_KYCverifyOTP',$plugin_admin, 'KYCverifyOTP');
		$this->loader->add_action( 'wp_ajax_nopriv_resendOTP',$plugin_admin, 'resendOTP');
        $this->loader->add_action( 'wp_ajax_resendOTP',$plugin_admin, 'resendOTP');
		$this->loader->add_action( 'wp_ajax_nopriv_resendKYCOTP',$plugin_admin, 'resendKYCOTP');
        $this->loader->add_action( 'wp_ajax_resendKYCOTP',$plugin_admin, 'resendKYCOTP');
		$this->loader->add_action( 'wp_ajax_nopriv_kyc_post_user_account_info',$plugin_admin, 'kyc_post_user_account_info');
        $this->loader->add_action( 'wp_ajax_kyc_post_user_account_info',$plugin_admin, 'kyc_post_user_account_info');
		$this->loader->add_action( 'wp_ajax_nopriv_validateKYCNationalID',$plugin_admin, 'validateKYCNationalID');
        $this->loader->add_action( 'wp_ajax_validateKYCNationalID',$plugin_admin, 'validateKYCNationalID');

		$this->loader->add_action( 'wp_ajax_nopriv_get_ticket_category',$plugin_admin, 'get_ticket_category');
        $this->loader->add_action( 'wp_ajax_get_ticket_category',$plugin_admin, 'get_ticket_category');

		$this->loader->add_action( 'wp_ajax_nopriv_get_ticket_subcategory',$plugin_admin, 'get_ticket_subcategory');
        $this->loader->add_action( 'wp_ajax_get_ticket_subcategory',$plugin_admin, 'get_ticket_subcategory');

		$this->loader->add_action( 'wp_ajax_nopriv_createTicket',$plugin_admin, 'createTicket');
        $this->loader->add_action( 'wp_ajax_createTicket',$plugin_admin, 'createTicket');

		$this->loader->add_action( 'wp_ajax_nopriv_viewDetailsAccountNumber',$plugin_admin, 'viewDetailsAccountNumber');
        $this->loader->add_action( 'wp_ajax_viewDetailsAccountNumber',$plugin_admin, 'viewDetailsAccountNumber');

		$this->loader->add_action( 'wp_ajax_nopriv_sms_payment_schedule',$plugin_admin, 'sms_payment_schedule');
        $this->loader->add_action( 'wp_ajax_sms_payment_schedule',$plugin_admin, 'sms_payment_schedule');

		$this->loader->add_action( 'wp_ajax_nopriv_add_resource',$plugin_admin, 'add_resource');
        $this->loader->add_action( 'wp_ajax_add_resource',$plugin_admin, 'add_resource');

		$this->loader->add_action( 'wp_ajax_nopriv_edit_resource',$plugin_admin, 'edit_resource');
        $this->loader->add_action( 'wp_ajax_edit_resource',$plugin_admin, 'edit_resource');

		$this->loader->add_action( 'wp_ajax_nopriv_logout',$plugin_admin, 'logout');
        $this->loader->add_action( 'wp_ajax_logout',$plugin_admin, 'logout');

		$this->loader->add_action( 'wp_ajax_nopriv_add_user_logs',$plugin_admin, 'add_user_logs');
        $this->loader->add_action( 'wp_ajax_add_user_logs',$plugin_admin, 'add_user_logs');

		$this->loader->add_action( 'wp_ajax_nopriv_users_activity_log',$plugin_admin, 'users_activity_log');
        $this->loader->add_action( 'wp_ajax_users_activity_log	',$plugin_admin, 'users_activity_log');

		$this->loader->add_action( 'admin_init',$plugin_admin, 'general_setting');

		$this->loader->add_action( 'admin_menu',$plugin_admin, 'dashboard_general_setting');

		$this->loader->add_action( 'wp_head', $plugin_admin, 'hide_online_service_menu'); 
		
    }


	/**
	 * Register all of the hooks related to the to create the template
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_template_hooks() {

		$plugin_template = new PageTemplater( $this->get_plugin_name(), $this->get_version() );

		$this->loader->add_action( 'plugins_loaded', $plugin_template, 'get_instance' );
		

	}

	/**
	 * Register all of the hooks related to the public-facing functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_public_hooks() {

		$plugin_public = new Nfsc_Clarion_Public( $this->get_plugin_name(), $this->get_version() );

		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_styles' );
		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_scripts' );

	}

	/**
	 * Run the loader to execute all of the hooks with WordPress.
	 *
	 * @since    1.0.0
	 */
	public function run() {
		$this->loader->run();
	}

	/**
	 * The name of the plugin used to uniquely identify it within the context of
	 * WordPress and to define internationalization functionality.
	 *
	 * @since     1.0.0
	 * @return    string    The name of the plugin.
	 */
	public function get_plugin_name() {
		return $this->plugin_name;
	}

	/**
	 * The reference to the class that orchestrates the hooks with the plugin.
	 *
	 * @since     1.0.0
	 * @return    Nfsc_Clarion_Loader    Orchestrates the hooks of the plugin.
	 */
	public function get_loader() {
		return $this->loader;
	}

	/**
	 * Retrieve the version number of the plugin.
	 *
	 * @since     1.0.0
	 * @return    string    The version number of the plugin.
	 */
	public function get_version() {
		return $this->version;
	}

}
